

app.controller('myCtrl',['$scope','SrvAgency', function($scope,SrvAgency){
	
	$scope.arrAgency = [];
	$scope.pageSize = 10;
	$scope.init = function(){
		
		$scope.listAgency();
	}
	$scope.listAgency = function(){
		SrvAgency.List().then(function(response){
			
			if(response.data.Respond.code == '200'){
				$scope.arrAgency = response.data.result.data;
				
				  
			}else{
				$scope.arrAgency = [];
			}
		})
	}
	$scope.init();
}])