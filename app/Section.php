<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Support\Facades\App;

class Section extends Model
{
    use SoftDeletes,Translatable;
    protected $translatable = ['name'];
    
    
    public static function listSection(){
         $lang = App::getlocale();
        $arrSection = Section::orderBy('name','ASC')->get();
        $arrSectionTrans = $arrSection->translate($lang,'en');
        $arrSectionTrans2 = translationHelper::translatedCollectionToArray($arrSectionTrans);
        
        return $arrSectionTrans2;
    }
}
