<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Support\Facades\App;

class Keyword extends Model
{
    use SoftDeletes,Translatable; 
    protected $translatable = ['name'];
    
    
    public static function listKeyword(){
         $lang = App::getlocale();
        $arrKeyword = Keyword::orderBy('name','ASC')->get();
        $arrKeywordTrans = $arrKeyword->translate($lang,'en');
        $arrKeywordTrans2 = translationHelper::translatedCollectionToArray($arrKeywordTrans);
        return $arrKeywordTrans2;
    }
    
    
}
