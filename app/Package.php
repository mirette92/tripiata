<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Support\Facades\Auth;

class Package extends Model
{
    use SoftDeletes,Translatable;
    protected $table = 'packages';
    protected $fillable = ['id','offer_id','room_id','agency_id','accomodation_id','curency_id','price','verified','updated_at','created_at','name'];
    protected $translatable = ['price'];
/*     public function getAllOffersAttachedToPackages($agency_id){

        $arrPackage = $this->where('agency_id',$agency_id);

        return $arrPackage;
    } */
    public function listLimit6($lang){
        $arrPackage = $this->limit(6)->get();
        //$arrPackageTrans = $arrPackage->translate($lang,'en');
        //$arrPackage2 =  translationHelper::translatedCollectionToArray($arrPackageTrans);
        foreach($arrPackage as $objPackage){
            $objReview = new Review();
            $objPackage->arrReview = $objReview->getReviewRateAttachedToOffer($objPackage->offer_id);
            $objPackage->totalRate = $objReview->getReviewRateAttachedToAgency($objPackage->offer_id);
        }
        return $arrPackage;
    }
    public function getPackageAttachedtoDestination($lang){
        $arrPackage = $this->limit(6)->get();
        $arrPackageTrans = $arrPackage->translate($lang,'en');
        $arrPackage2 =  translationHelper::translatedCollectionToArray($arrPackageTrans);
        return $arrPackage2;
    }

    public function getAllOffersAttachedToPackages($offer_id){

        $arrOffer = $this->where('offer_id',$offer_id)->with('Room')->with('Currency')->with('Acco')->get();
        //dd($arrOffer);
        return $arrOffer;
    }

    public function getAllOffersAttachedToPackages1($id){

        $pac = $this->where('id',$id)->pluck('offer_id');
        //dd($arrOffer);
        return $pac;
    }

    public function save(array $options = [])
    {
        $objAgency = new Agency();
        $agency = $objAgency->getAgency(Auth::user()->id);


        //dd($request);

        //$input = $request->all();
        $this->agency_id = $agency[0]['id'];
        parent::save();
    }
    public function Room(){
        return $this->belongsTo('App\Room','room_id','id');
    }
    public function Currency(){
        return $this->belongsTo('App\Currency','curency_id','id');
    }
    public function Acco(){
        return $this->belongsTo('App\Accommodation','accomodation_id','id');
    }
}
