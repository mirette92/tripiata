<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    //
    protected $table = 'likes';
    protected $fillable = ['id','user_id','comment_id','isLike'];
    
    public function getLikeAttachedToOffer($intCommentID){
        //print($intArticleID);
        $Like = $this->where('isLike',1)->where('comment_id',$intCommentID)->get();
        return count($Like);
    }
    public function getDisLikeAttachedToOffer($intCommentID){
        //print($intArticleID);
        $Like = $this->where('isLike',0)->where('comment_id',$intCommentID)->get();
        return count($Like);
    }
    public function isLikeByUser($intCommentID,$intUserID){
        $Like = $this->where('user_id',$intUserID)->where('isLike',1)->where('comment_id',$intCommentID)->first();
        if(!$Like){
            return false;
        }
        return true;
    }
    public function isUnLikeByUser($intCommentID,$intUserID){
        $Like = $this->where('user_id',$intUserID)->where('isLike',0)->where('comment_id',$intCommentID)->first();
        if(!$Like){
            return false;
        }
        return true;
    }
    
    public function removeLike($intCommentID,$intUserID){
        $Like = $this->where('user_id',$intUserID)->where('comment_id',$intCommentID)->delete();
        return $Like;
    }
    
    public function LikeByUser($intCommentID,$intUserID){
        $Like = $this->where('user_id',$intUserID)->where('comment_id',$intCommentID)->first();
        if(!$Like){
            return $Like;
        }
        return $Like;
    
    }
}