<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends \TCG\Voyager\Models\User
{
    use HasApiTokens,Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function checkFBID($fkID){
        $objUser = $this->where('fb_id',$fkID)->with('Phone')->first();
        if($objUser == null){
            return false;
        }else{
            return true;
        }
        
    }

    public static function getUserById($user_id){
        
        $objUser = User::where('id',$user_id)->first();
        
        return $objUser;
    }
    public function getUserByEmail($strEmail){
        $objUser = $this->where('email',$strEmail)->first();
        
        
        return $objUser;
    }
}
