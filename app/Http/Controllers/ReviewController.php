<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    //
    
    public function AddReview(Request $request){
        //dd($request);
        
        $objReview = new Review();
        $objReview->rate = $request->score;
        $objReview->review_text = $request->review_text;
        $objReview->offer_id = $request->offer_id;
       
        $objReview->user_id = Auth::user()->id;
        $objReview->save();
        
        return redirect()->back();
    }
    
    
}
