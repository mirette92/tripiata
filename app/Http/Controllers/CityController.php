<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\Helpers\translationHelper;

class CityController extends Controller
{
    //
    public function ListCityAttachedtoOffer(){
        $objCity = new City();
        $arrCity = $objCity->listCityAttachedtoHotel();
        $data = array ('arrCity'=>$arrCity);
        return view('hotels-destinations')->with($data);
        
    }
}
