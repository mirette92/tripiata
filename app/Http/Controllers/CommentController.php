<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
class CommentController extends Controller
{
    //
    
    public function AddComment(Request $request){
        
        $objComment = new Comment();
        $objComment->review_id = $request->review_id;
        $objComment->user_id = $request->user_id;
        $objComment->comment = $request->comment;
        $objComment->save();
        return redirect()->back();
    }
}
