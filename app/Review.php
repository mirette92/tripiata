<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Pagination\Paginator;
use App\Like;
use Illuminate\Support\Facades\Auth;

class Review extends Model
{
    use SoftDeletes,Translatable;
    
    public function getReviewRateAttachedToAgency($intAgencyID){
       
        $RateValuePerAgency = 0;
        $totalReviewRate = $this->where('agency_id',$intAgencyID)->sum('rate');
        
        if($totalReviewRate > 0){
            $RateValuePerAgency = $totalReviewRate / count($this->getReviewAttachedtoAgency($intAgencyID));
            //dd($RateValuePerAgency);
        }
        return ceil($RateValuePerAgency);
    }
    public function getReviewRateAttachedToOffer($intOfferID){
        $RateValuePerOffer = 0;
        $totalReviewRate= $this->where('offer_id',$intOfferID)->sum('rate');
        
        if($totalReviewRate > 0){
            $RateValuePerOffer = $totalReviewRate / count($this->listReviewAttachedtoOffer($intOfferID));
            //dd($RateValuePerPlace);
        }
        return $RateValuePerOffer;
    }
    
    public function listReviewAttachedtoOffer($offer_id){
       $arrReview = $this->where('offer_id',$offer_id)->with('user')->with('comment')->get();
       foreach($arrReview as $objReview){
           $objLike = new Like();
       if(Auth::user()){
           $intUserID = Auth::User()->id;
           $objReview->IsLikebyUser = $objLike->isLikeByUser($objReview->id,$intUserID);
           $objReview->IsUnLikebyUser = $objLike->isLikeByUser($objReview->id,$intUserID);
      
       }else{
           $objReview->IsLikebyUser = 0;
           $objReview->IsUnLikebyUser = 0;
       }
       $objReview->totalLike = $objLike->getLikeAttachedToOffer($objReview->id);
       $objReview->totalUnLike = $objLike->getDisLikeAttachedToOffer($objReview->id);
       }
       return $arrReview;
   }
   public function listReviewAttachedtoAgency($agency_id){
       $arrReview = $this->where('agency_id',$agency_id)->with('user')->with('comment')->paginate(10);
       return $arrReview;
   }
   
   public function getReviewAttachedtoAgency($agency_id){
       $arrReview = $this->where('agency_id',$agency_id)->get();
       return $arrReview;
   }
   public function comment(){
       return $this->hasMany('App\Comment','review_id','id');
   }
   public function user(){
       return $this->belongsTo('App\User','user_id','id');
   }
}
