<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use App\Review;
use Illuminate\Support\Facades\App;


class Agency extends Model
{
    use SoftDeletes,Translatable;
    protected $table = 'agencies';
    protected $fillable = ['id','name','phone','facebook','verified','biography','user_id','updated_at','created_at'];
    protected $translatable = ['name','biography'];
    public function getAgency($user_id){
        $objAgency = $this->where('user_id',$user_id)->get();
        if(count($objAgency)>0){
            return $objAgency;
        }
        return null;
    }
    
    public function getAgencyById($agency_id,$lang){
        $objAgency = $this->where('id',$agency_id)->get();
        $objAgencyTrans = $objAgency->translate($lang,'en');
        foreach($objAgencyTrans as $agency){
            $objReview = new Review();
            $objOffer = new Offer();
            $agency->arrReview = $objReview->listReviewAttachedtoAgency($agency->id);
            $agency->offers = count($objOffer->getOffer($agency['id']));
            $agency->TotalRate = $objReview->getReviewRateAttachedToAgency($agency->id);
        }
        $objAgency2 = translationHelper::translatedCollectionToArray($objAgencyTrans);
        return $objAgency2;
    }
    
    public function listAgency($lang,$search){
        //dd($lang);
        $lang1 = App::getlocale();
        // dd( $lang1);
        $arrAgency = $this->where('name','like', '%'.$search.'%')->paginate(9);
        $arrAgencyTrans = $arrAgency->translate($lang1,'en');
        foreach($arrAgencyTrans as $Agency){
            $Agency->img = MediaUrl::getUrl().$Agency->img;
            $objOffer = new Offer();
                $objReview = new Review();
                
                $Agency->arrReview = count($objReview->listReviewAttachedtoAgency($Agency['id']));
                $Agency->TotalRate = $objReview->getReviewRateAttachedToAgency($Agency['id']);
                $Agency->offers = count($objOffer->getOffer($Agency['id']));
           
        }
        $arrAgency2= translationHelper::translatedCollectionToArray($arrAgencyTrans);
        $arrAgency2= translationHelper::paginationForTranslator($arrAgency,$arrAgency2);
        //         dd($arrHotel2);
        //dd($arrAgency);
        return $arrAgency2;
    }
    
    public function getAgencyByIDforweb($agnecy_id,$lang){
         $lang1 = App::getlocale();
        $objAgency = $this->where('id',$agnecy_id)->get();
        $objAgencyTrans = $objAgency->translate($lang1,'en');
        $objOffer = new Offer();
        $objReview = new Review();
        $objAgencyTrans[0]->arrReview = $objReview->listReviewAttachedtoAgency($objAgency[0]->id);
        $objAgencyTrans[0]->offers = count($objOffer->getOffer($objAgency[0]->id));
        $objAgencyTrans[0]->TotalRate = $objReview->getReviewRateAttachedToAgency($objAgency[0]->id);
        $objAgencyTrans2 = translationHelper::translatedCollectionToArray($objAgencyTrans);
        //$objAgency2= translationHelper::translatedCollectionToArray($objAgencyTrans);
       //dd($objAgency);
        return $objAgencyTrans2;
    }
}
