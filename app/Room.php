<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class Room extends Model
{
    use SoftDeletes,Translatable;
    protected $table = 'rooms';
    protected $fillable = ['id','name'];
    protected $translatable = ['name'];


    public function getRoomById($room_id){
        $objRoom = $this->where('id',$room_id)->first();

        return $objRoom;
    }
    public static function getRoom(){
        $arrRoom = Room::get();

        return $arrRoom;
    }

}
