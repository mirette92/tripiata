<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Application extends Model
{
    use SoftDeletes;
    protected $table = 'applications';
    protected $fillable = ['id','offer_id','agency_id'];
   
    
}
