<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;
use App\Helpers\translationHelper;


class Currency extends Model
{
    use SoftDeletes,Translatable;
    protected $table = 'currencies';
    protected $fillable = ['id','name','symbol','updated_at','created_at'];
    protected $translatable = ['name','symbol'];

    public function getCurrencyById($currency_id,$lang){
        $objCurrency = $this->where('id',$currency_id)->get();
        $arrCurrency = $objCurrency->translate($lang,'en');
        $arrCurrency2 =  translationHelper::translatedCollectionToArray($arrCurrency); 

        return $arrCurrency2;
    }

    public static function getCurrency(){
        $arrCurrency = Currency::get();

        return $arrCurrency;
    }
}
