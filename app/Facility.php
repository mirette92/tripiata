<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class Facility extends Model
{
    use SoftDeletes,Translatable;
    protected $fillable = ['id','name'];
    protected $translatable = ['name'];
    
    
    public function listFacility($lang){
        $arrFacility = $this->get();
        $arrFacilityTrans = $arrFacility->translate($lang,'en');
        $arrFacility2 = translationHelper::translatedCollectionToArray($arrFacilityTrans);
        return $arrFacility2;
    }
    
    public function getFacilityAttachedtoHotel($arrFacilityIds){
        
        $arrFacility = $this->whereIn('id',$arrFacilityIds)->get();
        return $arrFacility;
    }
    public function getFacilityAttachedtoOffer($arrFacilityIds){
        
        $arrFacility = $this->whereIn('id',$arrFacilityIds)->get();
        return $arrFacility;
    }
}
