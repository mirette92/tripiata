<?php

return [
    'Offers' => 'عروض',
    'Lang'=>'العربيه',
    'arabic'=>'العربيه',
    'Serach Again'=>'ابحث',
    'Sign up/in'=>'تسجيل دخول',
    'Home'=>'الرئيسيه',
    'Agency'=>'شركات',    
    'Contact us'=>'تواصيل بينا',
    'Logout'=>'خروج',
    'About Us'=>'عنا',
    'Email Us'=>'البريد الاكتروني',
    'Tour with Tripiata'=>'لف العلم مع تربيتا',
    'Visit Europe, America, Asia, Africa or beyond!'=>'زور اروبا و امريكا و اسيا و افريقيا ',
    'Choose Your Keyword'=>'اختار كلمه',
    'Choose a Category'=>'اختار نوع',
    'Search'=>'ابحث',
    'Top Sections'=>'اقوى الوجهات',
    'review'=>'تعليق',
    'Day Use'=>'يوم',
    'Days'=>'ايام',
    'Nights'=>'ليالي',
    'Copyright 2018 Tripiata. All Rights Reserved'=>'جميع الحقوق محفظه لتربيتا 2018',
    'About Tripiata'=>'عن تربيتا',
    'Who we are'=>'من نحن',
    'Careers'=>'وظيف',
    'Company history'=>'تاريخ الشركه',
    'Legal'=>'Legal',
    'Partners'=>'شركاء',
    'Privacy notice'=>'حقوق',
    'Links'=>'روابط',
    'Trips'=>'رحالات',
    'Hotels'=>'فنادق',
    'Destinations'=>'Destinations',
    'Choose your destination'=>'اختار مقصدك',
    'Choose your Section'=>'Choose your Section'

];