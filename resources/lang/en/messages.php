<?php

return [
    'Offers' => 'Offers',
    'Lang'=>'English',
    'English'=>'English',
    'Serach Again'=>'Serach Again',
    'Sign up/in'=>'Sign up/in',
    'Home'=>'Home',
    'Agency'=>'Agency',    
    'Contact us'=>' Contact Us',
    'Logout'=>'Logout',
    'About Us'=>'About Us',
    'Email Us'=>'Email Us',
    'Tour with Tripiata'=>'Tour with Tripiata',
    'Visit Europe, America, Asia, Africa or beyond!'=>'Visit Europe, America, Asia, Africa or beyond!',
    'Choose Your Keyword'=>'Choose Your Keyword',
    'Choose a Category'=>'Choose a Category',
    'Search'=>'Search',
    'Top Sections'=>'Top Sections',
    'review'=>'review',
    'Day Use'=>'Day Use',
    'Days'=>'Days',
    'Nights'=>'Nights',
    'Copyright 2018 Tripiata. All Rights Reserved'=>'Copyright 2018 Tripiata. All Rights Reserved',
    'About Tripiata'=>'About Tripiata',
    'Who we are'=>'Who we are',
    'Careers'=>'Careers',
    'Company history'=>'Company history',
    'Legal'=>'Legal',
    'Partners'=>'Partners',
    'Privacy notice'=>'Privacy notice',
    'Links'=>'Links',
    'Trips'=>'Trips',
    'Hotels'=>'Hotels',
    'Destinations'=>'Destinations',
    'Choose your destination'=>'Choose your destination',
    'Choose your Section'=>'Choose your Section'

];