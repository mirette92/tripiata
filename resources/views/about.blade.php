<!doctype html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Book Your Hotel | Tripiata</title>
	<meta property="og:title" content=" Book Your Hotel | Tripiata"/>
	<meta name="description" content="Explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta name="keywords" content="Hotels, Holiday, vacation, booking, trip, travel, tourism, tourist, tripiata, camp, resorts" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<meta property="og:description" content=" explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta property="og:url" content=" https://www.tripiata.com/public/index"/>
	<meta property="og:type" content="website"/>	
	
	<meta name="twitter:card" content="summary" />	
	<meta name="twitter:title" content=" Book Your Hotel | Tripiata" />	
	<meta name="twitter:description" content="explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel" />	
	<meta name="twitter:url" content="https://www.tripiata.com/public/index" />	
	
	<!-- Fav and Touch Icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/ico/apple-touch-icon-144-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/ico/apple-touch-icon-114-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/ico/apple-touch-icon-72-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png')}}">
	<link rel="shortcut icon" href="{{ asset('images/ico/favicon.png')}}">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}" media="screen">	
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet">
	<link href="{{ asset('css/component.css')}}" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="{{ asset('icons/ionicons/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/simple-line-icons/css/simple-line-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/themify-icons/themify-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/rivolicons/style.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- CSS Custom -->
		<link href="css/style.css" rel="stylesheet"><link href="css/color-02.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	@include( 'login')
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper">

		
		<!-- start Header -->
		@include( 'nav')
		<!-- End Header -->
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('images/hero-header/breadcrumb.jpg');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">About Us</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="index.php">Homepage</a></li>
								<li><span>About Us</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper pt-0 pb-0">
			
				<div class="section pt-60">
				
					<div class="container">
						<h4>May ecstatic did surprise elegance the ignorant age</h4>
						<p class="font15 mb-30">Among going manor who did. Do ye is celebrated it sympathize considered. May ecstatic did surprise elegance the ignorant age. Own her miss cold last. It so numerous if he outlived disposal. How but sons mrs lady when. Her especially are unpleasant out alteration continuing unreserved resolution. Hence hopes noisy may china fully and. Am it regard stairs branch thirty length afford. Tolerably earnestly middleton extremely distrusts she boy now not. Add and offered prepare how cordial two promise. Greatly who affixed suppose but enquire compact prepare all put. Added forth chief trees but rooms think may.</p>
						
						<div class="about-us-grid-block GridLex-gap-20-wrappper">
						
							<div class="GridLex-grid-noGutter-equalHeight gap-20">
									
								<div class="GridLex-col-6_xs-12">
									<div class="about-us-grid-block-item mb-20">
										<h4 class="heading mt-0 text-white">Mission</h4>
										<p>Style too own civil out along. Perfectly offending attempted add arranging age gentleman concluded. Get who uncommonly our expression ten increasing considered occasional travelling.</p>
									</div>
								</div>
								
								<div class="GridLex-col-3_xs-12">
									<div class="image-bg mb-20" style="background-image:url('images/about-us/about-us-image-bg-01.jpg');"></div>
								</div>
								
								<div class="GridLex-col-3_xs-12">
									<div class="image-bg mb-20" style="background-image:url('images/about-us/about-us-image-bg-02.jpg');"></div>
								</div>
								
							</div>
						
							<div class="GridLex-grid-noGutter-equalHeight gap-20">
			
								<div class="GridLex-col-3_xs-12">
									<div class="image-bg mb-20" style="background-image:url('images/about-us/about-us-image-bg-03.jpg');"></div>
								</div>
								
								<div class="GridLex-col-6_xs-12">
									<div class="about-us-grid-block-item mb-20">
										<h4 class="heading mt-0 text-white">Vission</h4>
										<p>Style too own civil out along. Perfectly offending attempted add arranging age gentleman concluded. Get who uncommonly our expression ten increasing considered occasional travelling.</p>
									</div>
								</div>
								
								<div class="GridLex-col-3_xs-12 mb-20">
									<div class="image-bg" style="background-image:url('images/about-us/about-us-image-bg-04.jpg');"></div>
								</div>
								
							</div>
							
							<div class="GridLex-grid-noGutter-equalHeight gap-20">
			
								<div class="GridLex-col-3_xs-12 mb-20">
									<div class="image-bg" style="background-image:url('images/about-us/about-us-image-bg-05.jpg');"></div>
								</div>

								<div class="GridLex-col-3_xs-12 mb-20">
									<div class="image-bg" style="background-image:url('images/about-us/about-us-image-bg-06.jpg');"></div>
								</div>
								
								<div class="GridLex-col-6_xs-12">
									<div class="about-us-grid-block-item mb-20">
										<h4 class="heading mt-0 text-white">Our Goal</h4>
										<p>Style too own civil out along. Perfectly offending attempted add arranging age gentleman concluded. Get who uncommonly our expression ten increasing considered occasional travelling.</p>
									</div>
								</div>
								
							</div>
						
						</div>
						
						<h4>Style too own civil out along</h4>
						<p>Is branched in my up strictly remember. Songs but chief has ham widow downs. Genius or so up vanity cannot. Large do tried going about water defer by. Silent son man she wished mother. Distrusts allowance do knowledge eagerness assurance additions to.</p>
						
					</div>
						
				</div>
				
				
		<!-- start footer -->
		@include ('footer')
		<!-- End footer -->

	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/instagram.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/validator.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/customs.js')}}"></script>

</body>


<!-- Mirrored from crenoveative.com/envato/tour-packer/about.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Aug 2018 18:27:33 GMT -->
</html>