<!doctype html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Book Your Hotel | Tripiata</title>
	<meta property="og:title" content=" Book Your Hotel | Tripiata"/>
	<meta name="description" content="Explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta name="keywords" content="Hotels, Holiday, vacation, booking, trip, travel, tourism, tourist, tripiata, camp, resorts" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<meta property="og:description" content=" explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta property="og:url" content=" https://www.tripiata.com/public/index"/>
	<meta property="og:type" content="website"/>	
	
	<meta name="twitter:card" content="summary" />	
	<meta name="twitter:title" content=" Book Your Hotel | Tripiata" />	
	<meta name="twitter:description" content="explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel" />	
	<meta name="twitter:url" content="https://www.tripiata.com/public/index" />	

	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="{{ asset('images/ico/favicon.png')}}">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}" media="screen">	
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet">
	<link href="css/component.css" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="{{ asset('icons/ionicons/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/simple-line-icons/css/simple-line-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/themify-icons/themify-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/rivolicons/style.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- CSS Custom -->
	<link href="{{ asset('css/style.css')}}" rel="stylesheet">
	<link href="{{ asset('css/color-02.css')}}" rel="stylesheet">

</head>

<body class="">

<!-- BEGIN # MODAL LOGIN -->
@include('login')
<!-- END # MODAL LOGIN -->

<!-- start Container Wrapper -->
<div class="container-wrapper">

	<!-- start Header -->
	@include('nav')
	<!-- End Header -->

	<div class="clear"></div>
	
	<!-- start Main Wrapper -->
	<div class="main-wrapper">
	
		<div class="breadcrumb-wrapper bg-light-2">
				
				<div class="container">
				
					<ol class="breadcrumb-list booking-step">
						<li><a href="index">Homepage</a></li>
						<li><a href="offer-destinations">Destinations</a></li>
						<li><a href="#">Offers</a></li>
						<li><span>Confirmation</span></li>
					</ol>
					
					
				</div>
				
		</div>
		
		<div class="content-wrapper">
			
				<div class="container">
			
					<div class="row">
					
						<div class="col-sm-8 col-md-9">
	
							<div class="confirmation-wrapper">
							
								<div class="payment-success">
								
									<div class="icon">
										<i class="pe-7s-check text-success"></i>
									</div>
								
									<div class="content">
										
										<h2 class="heading uppercase mt-0 text-success">Thank you, your booking is complete!</h2>
										<p>We will contact you in  <span class="text-primary font700">24 hours</span></p>
									
									</div>
									
								</div>
							
								
								<div class="confirmation-content">
								
									<div class="section-title text-left">
										<h4>Payment</h4>
										
									</div>
								
									<p>Prepared do an dissuade be so whatever steepest. Yet her beyond looked either day wished nay. By doubtful disposed do juvenile an. Now curiosity you explained immediate why behaviour. An dispatched impossible of of melancholy favourable. Our quiet not heart along scale sense timed. Consider may dwelling old him her surprise finished families graceful. Gave led past poor met fine was new.</p>
									<p>Talking chamber as shewing an it minutes. Trees fully of blind do. Exquisite favourite at do extensive listening. Improve up musical welcome he. Gay attended vicinity prepared now diverted. Esteems it ye sending reached as. Longer lively her design settle tastes advice mrs off who. </p>
										
								</div>
								
								<div class="confirmation-content">
								
									<div class="section-title text-left">
										<h4>Additional Information</h4>
									</div>
									
									<p>Abilities or he perfectly pretended so strangers be exquisite. Oh to another chamber pleased imagine do in. Went me rank at last loud shot an draw. Excellent so to no sincerity smallness. Removal request delight if on he we. Unaffected in we by apartments astonished to decisively themselves. Offended ten old consider speaking.</p>
								
								</div>
								
							</div>
							
						</div>

						<div class="col-sm-4 col-md-3 mt-50-xs">

							<aside class="sidebar with-filter">
							
								<div class="sidebar-inner">
								
									<div class="sidebar-module">
										<h4 class="heading mt-0">Need Booking Help?</h4>
										<div class="sidebar-module-inner">
											<p class="mb-10">Paid was hill sir high 24/7. For him precaution any advantages dissimilar.</p>
											<ul class="help-list">
												<li><span class="font600">Hotline</span>: +1900 12 213 21</li>
												<li><span class="font600">Email</span>: info@Tripiata.com</li>
												
											</ul>
										</div>
									</div>
									
									
									<div class="sidebar-module">
										<h4 class="heading mt-0">Why booking with us?</h4>
										<div class="sidebar-module-inner">
											<ul class="featured-list-sm">
												<li>
													<span class="icon"><i class="fa fa-thumbs-up"></i></span>
													<h6 class="heading mt-0">No Booking Charges</h6>
													We don't charge you an extra fee for booking a hotel room with us
												</li>
												<li>
													<span class="icon"><i class="fa fa-credit-card"></i></span>
													<h6 class="heading mt-0">No Cancellation Sees</h6>
													We don't charge you a cancellation or modification fee in case plans change
												</li>
												
											</ul>
										</div>
									</div>
									
								</div>
								
							</aside>

						</div>

					</div>
				
				</div>
					
			</div>

		</div>
		


	</div>
	<!-- end Main Wrapper -->

	<!-- start footer -->
	 @include('footer')
	<!-- End footer -->

</div>  <!-- end Container Wrapper -->

@if(Auth::User())
<input type="hidden" value="{{Auth::User()->id}}" id="user_id">
@endif

<!-- start Back To Top -->
<div id="back-to-top">
	 <a href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->

<!-- JS -->
<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/instagram.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/validator.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js')}}"></script> 

<script type="text/javascript" src="{{ asset('js/customs.js')}}"></script>



</body>


</html>