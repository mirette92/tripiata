<!doctype html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Tripiata</title>
<meta name="description" content="HTML Responsive Template for Tour Agency or Company Based on Twitter Bootstrap 3.x.x" />
<meta name="keywords" content="tour package, holiday, hotel, vocation, booking, trip, travel, tourism, tourist" />
<meta name="author" content="crenoveative">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="{{ asset('images/ico/favicon.png')}}">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}" media="screen">	
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet">
	<link href="css/component.css" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="{{ asset('icons/ionicons/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/simple-line-icons/css/simple-line-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/themify-icons/themify-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/rivolicons/style.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- CSS Custom -->
	<link href="{{ asset('css/style.css')}}" rel="stylesheet">
	<link href="{{ asset('css/color-02.css')}}" rel="stylesheet">

</head>

<body class="">

<!-- BEGIN # MODAL LOGIN -->
@include('login')
<!-- END # MODAL LOGIN -->

<!-- start Container Wrapper -->
<div class="container-wrapper">

	<!-- start Header -->
	@include('nav')
	<!-- End Header -->

	<div class="clear"></div>
	
	<!-- start Main Wrapper -->
	<div class="main-wrapper scrollspy-container">
	
		<!-- start end Page title -->
		<div class="page-title detail-page-title" style="background-image:url('{{App\MediaUrl::getUrl().$Offer['img']}}');">
			
			<div class="container">
			
				<div class="flex-row">
					
					<div class="flex-column flex-md-8 flex-sm-12">
						
						<h1 class="hero-title">{{$Offer['name']}}</h1>
						<!--<p class="line18">He do subjects prepared bachelor juvenile ye oh. He feelings removing informed he as ignorant we prepared.</p>-->
						
						<ul class="list-col clearfix">
							<li class="rating-box">
								<div class="rating-wrapper">
									<div class="raty-wrapper">
										<div class="star-rating-white" data-rating-score="{{$Offer['totalReview']}}"></div> 
										<span style="display: block;"> / {{count($Offer['Review'])}} review</span>
									</div>
								</div>
							</li>
							
							<li class="duration-box">
								<div class="meta">
									<span class="block">{{$Offer['Differ']}}</span>
									Days
								</div>
								<div class="meta">
									&amp;
								</div>
								<div class="meta">
									<span class="block">{{$Offer['Differ'] - 1}}</span>
									Nights
								</div>
							</li>
							
							<li class="price-box">
								<div class="meta">
									<span class="block">{{$Offer['Currency']['symbol'].$Offer['price']}}</span>
									starting from
								</div>
							</li>
							
						</ul>
						
					</div>
					
					<div class="flex-column flex-md-4 flex-align-bottom flex-sm-12 mt-20-sm">
						<div class="text-right text-left-sm">
							<a href="#section-5" class="anchor btn btn-primary">See Reviews &amp; Get Discount</a>
						</div>
					</div>
				
				</div>

			</div>
			
		</div>
		<!-- end Page title -->
		
		<div class="breadcrumb-wrapper bg-light-2">
			
			<div class="container">
			
				<ol class="breadcrumb-list">
					<li><a href="index">Homepage</a></li>
					<li><a href="offer-destinations">Destinations</a></li>
					<li><a href="#">Offers</a></li>
					<li><span>{{$Offer['name']}}</span></li>
				</ol>
				
			</div>
			
		</div>
		
		<div class="content-wrapper">
		
			<div class="container">
		
				<div class="row">
				
					<div class="col-md-9" role="main">

						<div class="detail-content-wrapper">
							
								<div id="section-0" class="detail-content">
									
									<div class="section-title text-left">
										<h4>Itinerary</h4>
									</div>
									
									<div class="detail-item">
									
										<div class="row">
										
											<div class="col-sm-4 col-sm-3 mb-30">
											
												<ul class="list-info no-icon bb-dotted">
													<li><span class="font600">Duration: </span>@if($Offer-> Differ == 1) Day Use @elseif($Offer->Differ > 1){{$Offer->Differ}} Days {{$Offer->Differ  - 1}} Nights @endif</li>
													<li><span class="font600">Destination: </span> {{$Offer['Destination']['name']}}</li>
													<li><span class="font600">Valid From:</span> {{$Offer['from']}}</li>
													<li><span class="font600">Valid To:</span> {{$Offer['to']}}</li>
												</ul>
											
											</div>
											
											<div class="col-sm-8 col-md-9">
											
												<div class="itinerary-wrapper">
												
													<div class="itinerary-item intro-item">
														<h5>Introduction</h5>
														<div class="intro-item-body">
															<p>{{$Offer['desc']}}.</p>
															
														</div>
													</div>
													@if($Offer['Program'] != null)
													<div class="itinerary-day-label font600 uppercase"><span>Day</span></div>
													
													<div class="itinerary-item-wrapper">
													
														<div class="panel-group bootstarp-toggle">
														@foreach($Offer['Program'] as $index=>$objProgram)
															<div class="panel itinerary-item">
																<div class="panel-heading">
																	<h5 class="panel-title">
																		<a data-toggle="collapse" data-parent="#" href="#bootstarp-toggle-one"><span class="absolute-day-number">1</span> Visit: {{$objProgram['name']}}</a>
																	</h5>
																</div>
																<div id="bootstarp-toggle-one" class="panel-collapse collapse">
																	<div class="panel-body">
																	
																		<p>{{$objProgram['desc']}}.</p>
																		
																		<ul class="itinerary-meta clearfix">
																			<li><i class="fa fa-building-o"></i> stay at {{$objProgram['hotel']['name']}} Hotel</li>
																			<li><i class="fa fa-clock-o"></i> trip from: {{$objProgram['from']}}</li>
																		</ul>
																		
																	</div>
																</div>

															</div>
															@endforeach
															<!-- end of panel -->

															

														</div>
													
													</div>
													@endif
												</div>
												
											</div>
											
										</div>
									
									</div>

								</div>
								@if($Offer->multi_img != null)
								<div id="section-1" class="detail-content">
								
									<div class="section-title text-left">
										<h4>Gallery</h4>
									</div>
									
									<div class="slick-gallery-slideshow">
				
										<div class="slider gallery-slideshow">
										@foreach($Offer->multi_img as $objImg)
											<div><div class="image"><img src="{{App\MediaUrl::getUrl().$objImg}}" class="extra-height" alt="Image" /></div></div>
										@endforeach	
										</div>
										<div class="slider gallery-nav">
										@foreach($Offer->multi_img as $objImg)
											<div><div class="image"><img src="{{App\MediaUrl::getUrl().$objImg}}" alt="Image" /></div></div>
										@endforeach	
										</div>
									
									</div>

								</div>
								@endif
								
								@if($Offer['Program'] != null)
								<div id="section-2" class="detail-content">
								
									<div class="section-title text-left">
										<h4>Tour Accommodation</h4>
									</div>
									
									<div class="hotel-item-wrapper">
									
										<div class="row gap-1">
										@foreach($Offer['Program'] as $index=>$objProgram)
											<div class="col-sm-xss-12 col-xs-6 col-sm-4 col-md-4">
											
												<div class="hotel-item mb-1">
													<a href="hotel-detail?hotel_id={{$objProgram['hotel']['id']}}">
														<div class="image">
															<img src="{{$objProgram['hotel']['img']}}" alt="Hotel" />
														</div>
														<div class="content">
															<h6>{{$objProgram['hotel']['name']}} Hotel</h6>
														</div>
													</a>
												</div>
												
											</div>
											@endforeach
											
										
										</div>
									
									</div>
									
									<p>Unpacked now declared put you confined daughter improved. Celebrated imprudence few interested especially reasonable off one. Wonder bed elinor family secure met. It want gave west into high no in.</p>
								</div>
								@endif
									@if($Offer['facilities']  != null)
								<div id="section-3" class="detail-content">
								
									<div class="section-title text-left">
										<h4>What`s Excluded</h4>
									</div>
									
									<ul class="list-with-icon with-heading">
										@foreach($Offer['facilities'] as $objFacility)
										<li>
											<i class="fa fa-check-circle text-primary"></i>
											<h6 class="heading mt-0">{{$objFacility['name']}}</h6>
											<p>{{$objFacility['desc']}}</p>
										</li>
										@endforeach
										
										
									</ul>
									
									
								</div>
								@endif
								@if(count($Offer['packages'])>0)
								<div id="section-4" class="detail-content">

									<div class="section-title text-left">
										<h4>Availability</h4>
									</div>
									
									
									<div class="availabily-wrapper">
									
										<ul class="availabily-list">
											
											<li class="availabily-heading clearfix">
											
												<div class="date-from">
													Duration
												</div>
												
												<div class="date-to">
													Sleep
												</div>
												
												<div class="status">
													Accommodation
												</div>
												
												<div class="price">
													Price
												</div>
												
												<div class="action">
													&nbsp;
												</div>
											
											</li>
											@foreach($Offer['packages'] as $objPackage)
											<li class="availabily-content clearfix">
												
												<div class="date-from">
													<span class="availabily-heading-label">start:</span>
													<span>@if($objPackage->Differ == 1) Day Use @elseif($objPackage->Differ >1){{$objPackage->Differ - 1}} Days |  {{$objPackage->Differ}} Nights @endif</span>
												</div>
												
												<div class="date-to">
													<span class="availabily-heading-label">end:</span>
													<span>{{$objPackage['Room']['name']}}</span>
												</div>
												
												<div class="status">
													<span class="availabily-heading-label">status:</span>
													<span>{{$objPackage['Acco']['name']}}</span>
												</div>
												
												<div class="price">
													<span class="availabily-heading-label">price:</span>
													<span>{{$objPackage['CUrrency']['symbol'].$objPackage['Price']['name']}}</span>
												</div>
												
												
												<div class="action">
												
													<a @if(Session::has('user')) href="addApp?offer_id={{$Offer['id']}}" @else href="#loginModal" data-toggle="modal" @endif  class="btn btn-primary btn-sm btn-inverse">Book now</a>
    												
    											</div>
										
												
											</li>
											
											@endforeach
											
											
											
										</ul>
										
									</div>
									
									<ul class="list-with-icon with-heading">
									
										<li>
											<i class="fa fa-bell text-primary"></i>
											<h6 class="heading mt-0">Notes: </h6>
											<p>You will get extra <span class="text-primary font-weight-bold">{{$Offer['discount']}} discount</span> after selecting one of the above offers.</p>
										</li>
									</ul>

								</div>
								@endif
								
								<div id="section-5" class="detail-content">
								
									
									
									<div class="review-wrapper">
					
										<div class="review-header">
											<div class="row">
											
												<div class="col-sm-4 col-md-3">
												
													<div class="review-overall">
														<div class="section-title text-left">
															<h4>Reviews</h4>
														</div>
													
														<h6>Offer by <span class="text-primary">{{$Offer['agency']['name']}}</span></h6>
														<p class="review-overall-point"><span>{{$Offer['agency']['TotalRate']}}</span> / 5.0</p>
														<p class="review-overall-recommend">90% recommend this package</p>
														
													</div>
												
												</div>
												
												<div class="col-sm-8 col-md-9">
													
													<div class="review-overall-breakdown">
													<div class="row gap-20">
														<div class="col-sm-12 col-md-4">
															<h5 class="sidebar-title">{{$Offer['agency']['name']}}</h3>
															<div class="sidebar-module-inner">
																<ul class="sidebar-category">
																	<li><a href="#">Number of offers<span>{{$Offer['agency']['offers']}}</span></a></li>
																	<li><a href="#">Email<span>{{$Offer['agency']['email']}}</span></a></li>
																	<li><a href="#">Rating<span><b class="text-primary">{{$Offer['agency']['TotalRate']}} </b> / 5.0</span></a></li>
																	<li><a href="#">Reviews<span>{{count($Offer['agency']['arrReview'])}}</span></a></li>
																</ul>
															</div>
														</div>
														<div class="col-sm-12 col-md-8">

															<h5 class="sidebar-title">About</h3>
															<div class="sidebar-module-inner agency-details">
																<p>{{$Offer['agency']['biography']}}. </p>
															</div>
														</div>
													</div>	
												</div>	
												
												</div>
												
											</div>
										</div>
										
										@if(count($Offer['Review'])>0)
										<div class="review-content">
										
											<div class="row gap-20">
											
												<div class="col-sm-6">
													<h5>There are 2135 reviews</h5>
												</div>
												
												<div class="col-sm-6 text-right text-left-xs">
													<a href="#leave-comment" class="anchor btn btn-primary btn-inverse btn-sm">Leave comments</a>
												</div>
											
											</div>
											
											
											<ul class="review-list">
											@foreach($Offer['Review'] as $objReview)
											
												<li class="clearfix">
													<div class="image img-circle">
														<img class="img-circle" src="{{App\MediaUrl::getUrl().$objReview['user']['avatar']}}" alt="Man" />
													</div>
													<div class="content">
														<div class="row gap-20 mb-0">
															<div class="col-sm-9">
																<h6>{{$objReview['user']['name']}}</h6>
															</div>
															<div class="col-sm-3">
																<p class="review-date">{{$objReview['created_at']}}</p>
															</div>
														</div>
														
														<div class="rating-wrapper">
															<div class="raty-wrapper">
																<div class="star-rating-12px" data-rating-score="{{$objReview['rate']}}"></div>
															</div>
														</div>
														
														<div class="review-text">
														
															<p>{{$objReview['review_text']}}.</p>
															
															
														
														</div>
														
														<div class="review-other">
															
															<div class="row gap-20 mb-0">
																
																<div class="col-sm-6">
																
																	<ul class="social-share-sm">
																	
																		<li><span><i class="fa fa-share-square"></i> share</span></li>
																		<li class="the-label"><a href="#">Facebook</a></li>
																		<li class="the-label"><a href="#">Twitter</a></li>
																		<li class="the-label"><a href="#">Google Plus</a></li>
																		
																	</ul>
																
																</div>
																
																<div class="col-sm-6">
																
																	<ul class="social-share-sm for-useful">
																		<li><span>Was this review helpful? </span></li>
																		<li><a data-toggle="collapse" data-parent="#" href="#bootstarp-comments"><span><i class="fa fa-comments"></i> Reply</span></a></li>
																		<li class="the-label"><a href="#"><i class="fa fa-thumbs-up"></i></a> 2</li>
																		<li class="the-label"><a href="#"><i class="fa fa-thumbs-down"></i></a> 1</li>
																	</ul>
																
																</div>
															
															</div>
																<div class="clearfix"></div><br>
																
																<div id="bootstarp-comments" class="panel-collapse collapse">
																
																@foreach($objReview['Comment'] as $comment)
																	<div class="comments">
																		<div class="single-comment">
																			<div class="comment-image img-circle">
																				<img class="img-circle" src="{{App\MediaUrl::getUrl().App\User::getUserById($comment->user_id)['avatar']}} " alt="Man" />
																			</div>
																			<div class="content">
																				<div class="row gap-20 mb-0">
																					<div class="col-sm-9">
																						<h6>{{App\User::getUserById($comment->user_id)['name']}} </h6>
																					</div>
																					<div class="col-sm-3">
																						<p class="review-date">{{$comment->created_at}}</p>
																					</div>
																				</div>
																				
																				<div class="review-text">
																				
																					<p>{{$comment->comment}}.</p>
																					
																					
																				</div>
																			</div>
																		</div>
																		<div class="clearfix"></div><hr style="color:#E3E3E3">
																	
																		
																		
																	</div>
																	@endforeach
																	<div class="reply">
																			<div class="col-sm-12 col-md-12">
												
																				<div class="form-group">
																					<br>
																					<textarea class="form-control form-control-sm btn-right" rows="3"></textarea>
																				</div>
																			</div>
																			
																			<div class="clear"></div>
																			
																			<div class="col-sm-12 col-md-8 mt-10">
																				<a href="#" class="btn btn-primary">Reply</a>
																			</div>
																	</div>
																</div>
															
														</div>
														
													</div>
												</li>
												

												@endforeach
											</ul>
										
											<div class="bt text-center pt-30">
												<a href="#" class="btn btn-primary">Load More</a>
											</div>
											
										</div>
										@endif
									</div>

								</div>
								
								<div id="leave-comment" class="detail-content">
								
									<div class="section-title text-left">
										<h4>Leave Your Review</h4>
									</div>
									
									<div class="review-form">
										
										<form method="post" action="addReview" >
										@csrf
											<div class="row">
											
												
												
												<div class="clear"></div>
												
												<div class="col-sm-6 col-md-4">
												<input type="hidden" name="offer_id" value="{{$Offer['id']}}">
													<div class="form-group">
														<label>Your Rating: </label>
														<div class="rating-wrapper">
															<div class="raty-wrapper">
																<div class="star-rating" name="rate" data-rating-score="4.0"></div>
															</div>
														</div>
													</div>
												
												</div>
												
												<div class="clear"></div>
												
												<div class="col-sm-12 col-md-8">
												
													<div class="form-group">
														<label>Your Message: </label>
														<textarea class="form-control form-control-sm" name="review_text" rows="5"></textarea>
													</div>
												</div>
												
												<div class="clear"></div>
												
												<div class="col-sm-12 col-md-8 mt-10">
												@if(Session::has('user')) 
													<button  type="submit"  class="btn btn-primary">Submit</button>
													
												@else
													<a  href="#loginModal" data-toggle="modal"  class="btn btn-primary">Submit</a>
												@endif
												</div>
												
											</div>
										
										</form>
									
									</div>
									
								</div>	
								
								<div class="call-to-action">
								
									Question? <a href="contact" class="btn btn-primary btn-sm btn-inverse">Make an inquiry</a> 
								
								</div>
									
							</div>
						
					</div>

					<div class="col-sm-3 hidden-sm hidden-xs">
					
						<div class="scrollspy-sidebar sidebar-detail" role="complementary">
						
							<ul class="scrollspy-sidenav">
							
								<li>
									<ul class="nav">
										<li><a href="#section-0" class="anchor">Itinerary</a></li>
										<li><a href="#section-1" class="anchor">Gallery</a></li>
										<li><a href="#section-2" class="anchor">Tour Accommodation</a></li>
										<li><a href="#section-3" class="anchor">Facilities</a></li>
										<li><a href="#section-4" class="anchor">Availability</a></li>
										<li><a href="#section-5" class="anchor">Reviews</a></li>

									</ul>
								</li>

							</ul>
							
							<a href="#" class="btn btn-primary">Change Search</a>
							
							<div style="width: 100%; height: 20px;"></div>
							
						</div>

					</div>

				</div>
			
			</div>
				
		</div>

	</div>
	<!-- end Main Wrapper -->

	<!-- start footer -->
	 @include('footer')
	<!-- End footer -->

</div>  <!-- end Container Wrapper -->



<!-- start Back To Top -->
<div id="back-to-top">
	 <a href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/instagram.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/validator.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/customs.js')}}"></script>



<script>

/**
*  Sidebar Sticky
*/

!function ($) {

$(function(){

var $window = $(window)
var $body   = $(document.body)

var navHeight = $('.navbar').outerHeight(true) + 50

$body.scrollspy({
  target: '.scrollspy-sidebar',
  offset: navHeight
})

$window.on('load', function () {
  $body.scrollspy('refresh')
})

$('.scrollspy-container [href=#]').click(function (e) {
  e.preventDefault()
})

// back to top
setTimeout(function () {
  var $sideBar = $('.scrollspy-sidebar')

  $sideBar.affix({
	offset: {
	  top: function () {
		var offsetTop      = $sideBar.offset().top
		var sideBarMargin  = parseInt($sideBar.children(0).css('margin-top'), 10)
		var navOuterHeight = $('.scrollspy-nav').height()

		return (this.top = offsetTop - navOuterHeight - sideBarMargin)
	  }
	, bottom: function () {
		return (this.bottom = $('.scrollspy-footer').outerHeight(true))
	  }
	}
  })
}, 100)
	
})

}(window.jQuery)

</script>

</body>


</html>