<!doctype html>
<html lang="en">


<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Book Your Hotel | Tripiata</title>
	<meta property="og:title" content=" Book Your Hotel | Tripiata"/>
	<meta name="description" content="Explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta name="keywords" content="Hotels, Holiday, vacation, booking, trip, travel, tourism, tourist, tripiata, camp, resorts" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<meta property="og:description" content=" explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta property="og:url" content=" https://www.tripiata.com/public/index"/>
	<meta property="og:type" content="website"/>	
	
	<meta name="twitter:card" content="summary" />	
	<meta name="twitter:title" content=" Book Your Hotel | Tripiata" />	
	<meta name="twitter:description" content="explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel" />	
	<meta name="twitter:url" content="https://www.tripiata.com/public/index" />	
	
	<!-- Fav and Touch Icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/ico/apple-touch-icon-144-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/ico/apple-touch-icon-114-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/ico/apple-touch-icon-72-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png')}}">
	<link rel="shortcut icon" href="{{ asset('images/ico/favicon.png')}}">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}" media="screen">	
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet">
	<link href="{{ asset('css/component.css')}}" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="{{ asset('icons/ionicons/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/simple-line-icons/css/simple-line-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/themify-icons/themify-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/rivolicons/style.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- CSS Custom -->
		<link href="{{ asset('css/style.css')}}" rel="stylesheet"><link href="{{asset('css/color-02.css')}}" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	@include('login')
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper">

		<!-- start Header -->
		@include('nav')
		<!-- End Header -->
		
		<div class="clear"></div>
	
		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('images/hero-header/breadcrumb.jpg');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Agency</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="index">Homepage</a></li>
								<li><a href="agency">Agency</a></li>
								<li><span>Agency Details</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
			
				<div class="container">
							<div class="row">
								<div class="col-sm-4 col-md-3">												
									<div class="blog-media">
										<img src="{{App\MediaUrl::getUrl().$objAgency[0]['img']}}" style="height:195px" alt="" />
									</div>
								</div>
								<div class="col-sm-12 col-md-9 ">
									<div class="review-overall-breakdown">
										<div class="row gap-20">
											<div class="col-sm-12 col-md-4">
												<h3 class="sidebar-title">{{$objAgency[0]['name']}}</h3>
												<div class="sidebar-module-inner">
													<ul class="sidebar-category">
														<li><a href="#">Number of offers<span>{{$objAgency[0]['offers']}}</span></a></li>
														<li><a href="#">Email<span>{{$objAgency[0]['email']}}</span></a></li>
														<li><a href="#">Rating<span><b class="text-primary">{{$objAgency[0]['TotalRate']}} </b> / 5.0</span></a></li>
														<li><a href="#">Reviews<span>{{count($objAgency[0]['arrReview'])}}</span></a></li>
													</ul>
												</div>
											</div>
											<div class="col-sm-12 col-md-8">

												<h3 class="sidebar-title">About Us</h3>
												<div class="sidebar-module-inner">
													<p>{{$objAgency[0]['biography']}}. </p>
												</div>
											</div>
										</div>	
									</div>	
								</div>	
							</div>	

							<div class="blog-wrapper">

								<div class="blog-item blog-single">
								
									
									<h4 class="uppercase">Reviews</h4>
									
									<div id="comment-wrapper">
									
										<ul class="comment-item">
											@foreach($objAgency[0]['arrReview'] as $objReview)
											<li>
												<div class="comment-avatar">
													<img src="{{App\MediaUrl::getUrl().$objReview['user']['avatar']}}" alt="author image" />
												</div>
												<div class="comment-header">

													<h6 class="heading mt-0">{{$objReview['user']['name']}}</h6>
													<span class="comment-time">{{$objReview['created_at']}}</span>
												</div>
												<div class="comment-content">
													<p>{{$objReview['review_text']}}. </p>
												</div>
											</li>
											@endforeach
										</ul>
										
										<div class="clear"></div>
										@if(count($objAgency[0]['arrReview'])>2)
										<div class="text-center">
											<a href="#" class="btn btn-primary">Load More</a>
										</div>
										@endif
									</div><!-- End Comment -->
									
									<div class="section-title text-left">
											<h4>Agency Offers</h4>
									</div>
									<div class="GridLex-gap-20-wrappper package-grid-item-wrapper on-page-result-page alt-smaller">
						
										<div class="GridLex-grid-noGutter-equalHeight">
										
										
											@foreach($arrOffer as $objOffer)
										
											<div class="GridLex-col-4_sm-6_xs-12 mb-20">
												<div class="package-grid-item"> 
													<a href="{{env('APP_URL')}}/public/offer-details?offer_id={{$objOffer['id']}}">
														<div class="image">
															<img src="{{App\MediaUrl::getUrl().$objOffer['img']}}" alt="Tripiata" />
															<div class="absolute-in-image">
																<div class="duration"><span>@if($objOffer['Differ'] == 1) Day Use @elseif($objOffer['Differ'] >1){{$objOffer['Differ'] - 1}} days {{$objOffer['Differ']}} nights @endif</span></div>
															</div>
														</div>
														<div class="content clearfix">
															<h6>{{$objOffer['name']}}</h6>
															<div class="rating-wrapper">
																<div class="raty-wrapper">
																	<div class="star-rating-12px" data-rating-score="{{$objOffer['TotalRate']}}"></div> <span> / {{$objOffer['arrReview']}} review</span>
																</div>
															</div>
															<div class="absolute-in-content">
																<span class="btn"><i class="fa fa-angle-right"></i> </span>
																<div class="price">{{$objOffer['Currency'][0]['symbol'].$objOffer['price']}}</div>
															</div>
														</div>
													</a>
												</div>
											</div>
											@endforeach
										
										</div>
									</div>
									<div class="pager-wrappper mt-30 clearfix">
			
								<div class="pager-innner">
								
									<div class="flex-row flex-align-middle">
										
										<div class="flex-column flex-sm-12">
									
										
										</div>
									
									</div>
									
								</div>
								
							</div>

									<div class="clear"></div>
									
									
									</div>
								
							</div>
						
						</div>
						
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

		<!-- start footer -->
		@include ('footer')
		<!-- End footer -->

	</div>  <!-- end Container Wrapper -->
 

 
<!-- start Back To Top -->
<div id="back-to-top">
   <a href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/instagram.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/validator.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/customs.js')}}"></script>
<script>
    
    $(document).ready(function() { 
  $(document).on('click', '.prev, .next', function() { 
    $.ajax({
      // the route you're requesting should return view('page_details') with the required variables for that view
      url: '/route/as/defined/in/routes-file?from=' + $(this).attr('data-from'),
      type: 'get'
    }).done(response) { 
      $('div#results').html(response);
    }
  });
});
    
</script>
</body>

</html>