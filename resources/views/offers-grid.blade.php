<!doctype html>
<html lang="en">


<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tripiata</title>
	<meta name="description" content="HTML Responsive Template for Tour Agency or Company Based on Twitter Bootstrap 3.x.x" />
	<meta name="keywords" content="tour package, holiday, hotel, vocation, booking, trip, travel, tourism, tourist" />
	<meta name="author" content="crenoveative">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/ico/apple-touch-icon-144-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/ico/apple-touch-icon-114-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/ico/apple-touch-icon-72-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png')}}">
	<link rel="shortcut icon" href="{{ asset('images/ico/favicon.png')}}">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}" media="screen">	
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet">
	<link href="{{ asset('css/component.css')}}" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="{{ asset('icons/ionicons/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/simple-line-icons/css/simple-line-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/themify-icons/themify-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/rivolicons/style.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- CSS Custom -->
		<link href="{{ asset('css/style.css')}}" rel="stylesheet"><link href="css/color-02.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	@include('login')
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper">

		<!-- start Header -->
		@include('includes/nav.php')
		<!-- End Header -->
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url("{{ asset('images/hero-header/breadcrumb.jpg')");">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Packages List</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="index.php">Homepage</a></li>
								<li><span>Packages List</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
			
				<div class="container">
				
					<div class="row">
						
						<div class="col-sm-4 col-md-3">
							
							<aside class="sidebar with-filter">
				
								<div class="sidebar-search-wrapper bg-light-2">
								
									<div class="sidebar-search-header">
										<h4>Search Again</h4>
									</div>
									
									<div class="sidebar-search-content">

										<div class="form-group">
											
											<select name="destination" class="select2-multi form-control" data-placeholder="Choose a Destination" multiple>
												<option value="">Choose a Destination</option>
												<option value="0">Any Destination</option>
												<option value="Albania">Albania</option>
												<option value="Austria" selected>Austria</option>
												<option value="Belgium">Belgium</option>
												<option value="Bosnia">Bosnia</option>
												<option value="Croatia">Croatia</option>
												<option value="Czech Republic">Czech Republic</option>
												<option value="Denmark">Denmark</option>
												<option value="Egypt">Egypt</option>
												<option value="England">England</option>
												<option value="Estonia">Estonia</option>
												<option value="Finland">Finland</option>
												<option value="France" selected>France</option>
												<option value="Germany" selected>Germany</option>
												<option value="Greece">Greece</option>
												<option value="Herzegovina">Herzegovina</option>
												<option value="Hungary">Hungary</option>
												<option value="Ireland">Ireland</option>
												<option value="Italy">Italy</option>
												<option value="Latvia">Latvia</option>
												<option value="Liechtenstein">Liechtenstein</option>
												<option value="Lithuania">Lithuania</option>
												<option value="Luxembourg">Luxembourg</option>
												<option value="Monaco">Monaco</option>
												<option value="Montenegro">Montenegro</option>
												<option value="Netherlands">Netherlands</option>
												<option value="Northern Ireland">Northern Ireland</option>
												<option value="Poland">Poland</option>
												<option value="Portugal">Portugal</option>
												<option value="Russia">Russia</option>
												<option value="Scotland">Scotland</option>
												<option value="Serbia">Serbia</option>
												<option value="Slovakia">Slovakia</option>
												<option value="Slovenia">Slovenia</option>
												<option value="Spain">Spain</option>
												<option value="Sweden">Sweden</option>
												<option value="Switzerland">Switzerland</option>
												<option value="Turkey">Turkey</option>
												<option value="Vatican City">Vatican City</option>
												<option value="Wales">Wales</option>
											</select>

										</div>
									
										<div class="form-group">
														
											<select name="month" class="select2-multi form-control" data-placeholder="Choose a Departure Month" multiple>
												<option value="">Choose a Departure Month</option>
												<option value="0">Any Departure Month</option>
												<option value="1">January</option>
												<option value="2">February</option>
												<option value="3">March</option>
												<option value="4" selected>April</option>
												<option value="5">May</option>
												<option value="6">June</option>
												<option value="7">July</option>
												<option value="8">August</option>
												<option value="9">September</option>
												<option value="10">October</option>
												<option value="11">November</option>
												<option value="12">December</option>
											</select>
											
										</div>
										
										<div class="form-group">

											<select name="year" class="select2-multi form-control" data-placeholder="Choose a Departure Year" multiple>
												<option value="">Choose a Departure Year</option>
												<option value="0">Any Departure Year</option>
												<option value="2016" selected>2016</option>
												<option value="2017">2017</option>
											</select>
											
										</div>
									
										<a href="#" class="btn btn-primary btn-block">Search</a>
									
									</div>
									
								</div>
								
								<div class="sidebar-header clearfix">
									<h4>Filter Results</h4>
									<a href="#" class="sidebar-reset-filter"><i class="fa fa-times"></i> reset filter</a>
								</div>
								
								<div class="sidebar-inner">
								
									<div class="sidebar-module">
										<h6 class="sidebar-title">Name Contain</h6>
										<div class="sidebar-module-inner">
											<div class="sidebar-mini-search">
												<div class="input-group">
													<input type="text" class="form-control" placeholder="Search for...">
													<span class="input-group-btn">
														<button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
													</span>
												</div>
											</div>
										</div>
									</div>
									
									<div class="sidebar-module">
										<h6 class="sidebar-title">Price Range</h6>
										<div class="sidebar-module-inner">
											<input id="price_range" />
										</div>
									</div>
									
									<div class="clear"></div>
									
									<div class="sidebar-module">
										<h6 class="sidebar-title">Star Range</h6>
										<div class="sidebar-module-inner">
											<input id="star_range" />
										</div>
									</div>
									
									<div class="clear"></div>
									
									
									<div class="sidebar-module">
									
										<h6 class="sidebar-title">Destination</h6>
										<div class="sidebar-module-inner">
											<div class="checkbox-block">
												<input id="ending_point-1" name="ending_point" type="checkbox" class="checkbox"/>
												<label class="" for="ending_point-1">Berlin <span class="checkbox-count">(854)</span></label>
											</div>
											<div class="checkbox-block">
												<input id="ending_point-2" name="ending_point" type="checkbox" class="checkbox"/>
												<label class="" for="ending_point-2">Paris <span class="checkbox-count">(25)</span></label>
											</div>
											<div class="checkbox-block">
												<input id="ending_point-3" name="ending_point" type="checkbox" class="checkbox"/>
												<label class="" for="ending_point-3">Munich <span class="checkbox-count">(254)</span></label>
											</div>
											<div class="starting_point-block">
												<input id="ending_point-4" name="ending_point" type="checkbox" class="checkbox"/>
												<label class="" for="ending_point-4">Lyon<span class="checkbox-count">(22)</span></label>
											</div>
											<div class="starting_point-block">
												<input id="ending_point-5" name="ending_point" type="checkbox" class="checkbox"/>
												<label class="" for="ending_point-5">Vienna  <span class="checkbox-count">(9)</span></label>
											</div>
											
											<div class="more-less-wrapper">
												
												<div id="hotel_facilities_more_less" class="collapse"> 
													<div class="more-less-inner">
													
														<div class="checkbox-block">
															<input id="ending_point-6" name="ending_point" type="checkbox" class="checkbox"/>
															<label class="" for="ending_point-6">Toulouse <span class="checkbox-count">(3)</span></label>
														</div>
														<div class="checkbox-block">
															<input id="ending_point-7" name="ending_point" type="checkbox" class="checkbox"/>
															<label class="" for="ending_point-7">Graz  <span class="checkbox-count">(25)</span></label>
														</div>
														<div class="checkbox-block">
															<input id="ending_point-8" name="ending_point" type="checkbox" class="checkbox"/>
															<label class="" for="ending_point-8">Linz  <span class="checkbox-count">(2)</span></label>
														</div>
														
													</div>
												</div>
												<button class="btn btn-more-less collapsed" data-toggle="collapse" data-target="#hotel_facilities_more_less">Show more</button>
												
											</div>
											
										</div>
									
										
									</div>
									
									<div class="clear"></div>
								
									<div class="sidebar-module">
									
										<h6 class="sidebar-title">Filter Select</h6>
										<div class="sidebar-module-inner">
											<select class="select2-single form-control" data-placeholder="Select Placeholder">
												<option value="">Select Placeholder</option>
												<option value="0">Select One</option>
												<option value="1">Select Two</option>
												<option value="2">Select Three</option>
												<option value="3">Select Four</option>
												<option value="4">Select Five</option>
											</select>
										</div>
										
									</div>
									
									<div class="clear"></div>
									
									<div class="sidebar-module">
									
										<h6 class="sidebar-title">Filter Text Inside Sidebar Inner</h6>
										<div class="sidebar-module-inner">
											<p>Park fat she nor does play deal our. Procured sex material his offering humanity laughing moderate can.</p>
										</div>
										
									</div>
									
									<div class="clear"></div>
									
								</div>
								
								<div class="sidebar-box">
									<h4 class="sidebar-title">Sidebar Text</h4>
									<p>Park fat she nor does play deal our. Procured sex material his offering humanity laughing moderate can. Unreserved had she nay dissimilar admiration interested.</p>
								</div>
							
							</aside>
							
							
						</div>
						
						<div class="col-sm-8 col-md-9">
							
							<div class="sorting-wrappper">
			
								<div class="sorting-header">
									<h3 class="sorting-title uppercase">3 Countries: Germany, France, Austria</h3>
									<p class="sorting-lead">587 results found</p>
								</div>
								
								<div class="sorting-content">
								
									<div class="row">
									
										<div class="col-sm-12 col-md-8">
											<div class="sort-by-wrapper">
												<label class="sorting-label">Sort by: </label> 
												<div class="sorting-middle-holder">
													<ul class="sort-by">
														<li class="active up"><a href="#">Name <i class="fa fa-long-arrow-down"></i></a></li>
														<li><a href="#">Price</a></li>
														<li><a href="#">Location</a></li>
														<li><a href="#">Start Rating</a></li>
														<li><a href="#">User Rating</a></li>
													</ul>
												</div>
											</div>
										</div>
										
										<div class="col-sm-12 col-md-4">
											<div class="sort-by-wrapper mt pull-right pull-left-sm mt-10-sm">
												<label class="sorting-label">View as: </label> 
												<div class="sorting-middle-holder">
													<a href="offers-list.php" class="btn btn-sorting"><i class="fa fa-th-list"></i></a>
													<a href="offers-grid.php" class="btn btn-sorting active"><i class="fa fa-th-large"></i></a>
												</div>
											</div>
										</div>
										
									</div>
								
								</div>

							</div>
							
							<div class="GridLex-gap-20-wrappper package-grid-item-wrapper on-page-result-page alt-smaller">
						
								<div class="GridLex-grid-noGutter-equalHeight">
								
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/01.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Paris in Love</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.0"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/02.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Classic Europe</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="3.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/03.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Best of Egypt</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/07.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Scandinavia Attractions</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="5.0"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/08.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Monaco in Love</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.0"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/09.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Highlights of Italy</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/10.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Paris in Love</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.0"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/11.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Classic Europe</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="3.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/12.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Best of Egypt</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/13.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Adriatic Coastal Explorer</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="5.0"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/14.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Athens, Mykonos &amp; Santorini</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/15.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Honeymoon Time in Maldives</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/03.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Best of Egypt</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/11.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Classic Europe</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="3.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offer-details.php">
												<div class="image">
													<img src="images/tour-package/08.jpg" alt="Tour Package" />
													<div class="absolute-in-image">
														<div class="duration"><span>4 days 3 nights</span></div>
													</div>
												</div>
												<div class="content clearfix">
													<h6>Best of Egypt</h6>
													<div class="rating-wrapper">
														<div class="raty-wrapper">
															<div class="star-rating-12px" data-rating-score="4.5"></div> <span> / 7 review</span>
														</div>
													</div>
													<div class="absolute-in-content">
														<span class="btn"><i class="fa fa-angle-right"></i></span>
														<div class="price">$1422</div>
													</div>
												</div>
											</a>
										</div>
									</div>
									
								</div>
							
							</div>
							
							<div class="pager-wrappper mt-30 clearfix">
			
								<div class="pager-innner">
								
									<div class="flex-row flex-align-middle">
											
										<div class="flex-column flex-sm-12">
											Showing reslut 1 to 15 from 248 
										</div>
										
										<div class="flex-column flex-sm-12">
											<nav class="pager-right">
												<ul class="pagination">
													<li>
														<a href="#" aria-label="Previous">
															<span aria-hidden="true">&laquo;</span>
														</a>
													</li>
													<li class="active"><a href="#">1</a></li>
													<li><a href="#">2</a></li>
													<li><a href="#">3</a></li>
													<li><span>...</span></li>
													<li><a href="#">11</a></li>
													<li><a href="#">12</a></li>
													<li><a href="#">13</a></li>
													<li>
														<a href="#" aria-label="Next">
															<span aria-hidden="true">&raquo;</span>
														</a>
													</li>
												</ul>
											</nav>
										</div>
									
									</div>
									
								</div>
								
							</div>
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

		<!-- start footer -->
		<?php include 'includes/footer.php'; ?>
		<!-- End footer -->

	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


 
<!-- JS -->
<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/instagram.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/validator.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/customs.js')}}"></script>

</body>


<!-- Mirrored from crenoveative.com/envato/tour-packer/offers-grid.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Aug 2018 18:21:11 GMT -->
</html>