@foreach($arrOffer as $objOffer)

  <div class="package-list-item clearfix">
    <div class="image">
      <a href="{{env('APP_URL')}}/public/offer-details?offer_id={{$objOffer['id']}}">
        <img src="{{App\MediaUrl::getUrl().$objOffer->img}}" alt="Tripiata" />
        <div class="absolute-in-image">
          <div class="duration"><span>@if($objOffer->Differ == 1) Day Use @elseif($objOffer->Differ >1){{$objOffer->Differ}} Days {{$objOffer->Differ - 1}} Nights @endif</span></div>
        </div>
      </a>
    </div>

    <div class="content">
      <h5>{{$objOffer['name']}} <button class="btn"></button></h5>
      <div class="row gap-10">
        <div class="col-sm-12 col-md-9">

          <p class="line18">{!! \Illuminate\Support\Str::limit(strip_tags($objOffer['desc']), $limit = 50, $end = '...') !!}</p>

          <ul class="list-info">
            <li><span class="icon"><i class="fa fa-map-marker"></i></span> <span class="font600">Destination: </span> {{$objOffer->Destination['name']}}</li>
            <li><span class="icon"><i class="fa fa-calendar"></i></span> <span class="font600">Valid From:</span> {{$objOffer->from}}</li>
            <li><span class="icon"><i class="fa fa-calendar"></i></span> <span class="font600">Valid To:</span> {{$objOffer->to}}</li>
            <li><span class="icon"><i class="fa fa-users"></i></span> <span class="font600">Offer By:</span><a href="{{env('APP_URL')}}/public/agency-detail?agency_id={{$objOffer['agency']['id']}}"> {{$objOffer['agency']['name']}}</a></li>
            <li><span class="icon"><i class="fa fa-bell"></i></span> <span class="font600">Discount:</span> Extra <span class="text-primary font-weight-bold">{{$objOffer->discount}}%</span> Discount after booking</li>
          </ul>


        </div>
        <div class="col-sm-12 col-md-3 text-right text-left-sm">

          <div class="rating-wrapper">
            <div class="raty-wrapper">
              <div class="star-rating-12px" data-rating-score="{{$objOffer['totalReview']}}"></div> <span> {{count($objOffer['Review'])}} review</span>
            </div>
          </div>

          <div class="price">{{$objOffer['Currency']['symbol'].$objOffer['price']}}</div>

          <a href="{{env('APP_URL')}}/public/offer-details?offer_id={{$objOffer['id']}}" class="btn btn-primary btn-sm">view</a>

        </div>
      </div>
    </div>

  </div>
  @endforeach
