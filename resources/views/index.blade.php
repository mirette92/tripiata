<!doctype html>
<html lang="en">


<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Book Your Hotel | Tripiata</title>
	<meta property="og:title" content=" Book Your Hotel | Tripiata"/>
	<meta name="description" content="Explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta name="keywords" content="Hotels, Holiday, vacation, booking, trip, travel, tourism, tourist, tripiata, camp, resorts" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<meta property="og:description" content=" explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta property="og:url" content=" https://www.tripiata.com/public/index"/>
	<meta property="og:type" content="website"/>	
	
	<meta name="twitter:card" content="summary" />	
	<meta name="twitter:title" content=" Book Your Hotel | Tripiata" />	
	<meta name="twitter:description" content="explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel" />	
	<meta name="twitter:url" content="https://www.tripiata.com/public/index" />		


	<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="{{ asset('images/ico/favicon.png')}}">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}" media="screen">	
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet">
	<link href="css/component.css" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="{{ asset('icons/ionicons/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/simple-line-icons/css/simple-line-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/rivolicons/style.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- CSS Custom -->
	<link href="{{ asset('css/style.css')}}" rel="stylesheet">
	<link href="{{ asset('css/color-02.css')}}" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body>

	<div id="introLoader" class="introLoading"></div>
	
	<!-- BEGIN # MODAL LOGIN -->
	@include('login')
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper">

		<!-- start Header -->
		@include('nav')
		<!-- End Header -->
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start hero-header with windows height -->
			<div class="hero" style="background-image:url('images/hero-header/01.jpg');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
						
							<h1 class="hero-title">{{ trans('messages.Tour with Tripiata') }}</h1>
							<p class="lead">{{ trans('messages.Visit Europe, America, Asia, Africa or beyond!') }}</p>

						</div>
						
					</div>
					
					<div class="col-md-10 col-md-offset-1 full-width">
					<form action="offers-list" method="get">
						<div class="inner">
						
								<div class="col-md-6">
								
									<div class="form-group">

										<select  class="select2-multi form-control" data-placeholder="{{ trans('messages.Choose Your Keyword') }}" name="keyword_id" >
											<option value="">Any Keyword</option>
											
											@foreach(App\Keyword::listKeyword() as $objKeyword)
											<option value="{{$objKeyword['id']}}">{{$objKeyword['name']}}</option>
											@endforeach
										</select>
										
									</div>
								
								</div>

							
							<div class="col-md-3">
							
								<div class="form-group">
								
									<select  class="select2-multi form-control" data-placeholder="{{ trans('messages.Choose a Category') }}" name="section_id" >
										<option value="">Choose a Category</option>
											@foreach(App\Section::listSection() as $objSection)
												<option value="{{$objSection['id']}}">{{$objSection['name']}}</option>
											@endforeach
									</select>
									
								</div>
							
							</div>
							
							
							
							<div class="col-md-3 for-btn">
							
								<div class="form-group">
								
									<!-- <a href="#" class="btn btn-primary btn-block">Search</a> -->
									<button class="btn btn-primary btn-block" type="submit">{{ trans('messages.Search') }}</button>
									
								</div>
							
							</div>
							
						</div>
						</form>
					</div>
				
				</div>
				
			</div>


			<section>
			
				<div class="container">
				
					<div class="row">
						
						<div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
							
							<div class="section-title">
							
								<h3>{{ trans('messages.Top Sections') }}</h3>
								
							</div>
							
						</div>
					
					</div>
					
					<div class="grid destination-grid-wrapper">
			
						<div class="grid-item" data-colspan="10" data-rowspan="7">
							<a href="http://beta.tripiata.com/public/offers-list?keyword_id=&section_id={{App\Section::listSection()[0]['id']}}" class="top-destination-image-bg" style="background-image:url('{{App\MediaUrl::getUrl().App\Section::listSection()[0]['img']}}');">
								<div class="relative">
									<h4>{{App\Section::listSection()[0]['name']}}</h4>
									<!--  >span>Starting from $322</span-->
								</div>
							</a>
						</div>
						
						<div class="grid-item" data-colspan="10" data-rowspan="3">
							<a href="http://beta.tripiata.com/public/offers-list?keyword_id=&section_id={{App\Section::listSection()[1]['id']}}" class="top-destination-image-bg" style="background-image:url('{{App\MediaUrl::getUrl().App\Section::listSection()[1]['img']}}');">
								<div class="relative">
									<h4>{{App\Section::listSection()[1]['name']}}</h4>
									<!--  >span>Starting from $322</span-->
								</div>
							</a>
						</div>
						
						<div class="grid-item" data-colspan="5" data-rowspan="4">
							<a href="http://beta.tripiata.com/public/offers-list?keyword_id=&section_id={{App\Section::listSection()[2]['id']}}" class="top-destination-image-bg" style="background-image:url('{{App\MediaUrl::getUrl().App\Section::listSection()[2]['img']}}');">
								<div class="relative">
									<h4>{{App\Section::listSection()[2]['name']}}</h4>
									<!--  >span>Starting from $322</span-->
								</div>
							</a>
						</div>
						
						<div class="grid-item" data-colspan="5" data-rowspan="4">
							<a href="http://beta.tripiata.com/public/offers-list?keyword_id=&section_id={{App\Section::listSection()[3]['id']}}" class="top-destination-image-bg" style="background-image:url('{{App\MediaUrl::getUrl().App\Section::listSection()[3]['img']}}');">
								<div class="relative">
									<h4>{{App\Section::listSection()[3]['name']}}</h4>
									<!--  >span>Starting from $322</span-->
								</div>
							</a>
						</div>
						
					</div>
					
				</div>
				
			</section>
			
			<section class="bg-light">
			
				<div class="container">
				
					<div class="row">
						
						<div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
							
							<div class="section-title">
							
								<h3>{{ trans('messages.Offers') }}</h3>
								
								
							</div>
							
						</div>
					
					</div>
					
					<div class="GridLex-gap-30-wrappper package-grid-item-wrapper">
						
						<div class="GridLex-grid-noGutter-equalHeight">
						@foreach(App\Offer::listOfferSponsor() as $objOffer)
							<div class="GridLex-col-4_sm-6_xs-12 mb-30">
								<div class="package-grid-item"> 
									<a href="{{env('APP_URL')}}/public/offer-details?offer_id={{$objOffer['id']}}">
										<div class="image">
											<img src="{{App\MediaUrl::getUrl().$objOffer['img']}}" alt="Tour Package" />
											<div class="absolute-in-image">
												 <div class="duration"><span>@if($objOffer['Differ'] == 1){{ trans('messages.Day') }} @elseif($objOffer['Differ'] >1){{$objOffer['Differ']}}{{ trans('messages.Days') }}  {{$objOffer['Differ'] - 1}} {{ trans('messages.Nights') }}  @endif</span></div> 
											</div>
										</div>
										<div class="content clearfix">
											<h5>{{$objOffer['name']}}</h5>
											 <div class="rating-wrapper">
												<div class="raty-wrapper">
												   
													<div class="star-rating-read-only" data-rating-score="{{$objOffer['TotalRate']}}"></div> <span> /{{$objOffer['totalReview']}} {{ trans('messages.review') }}</span>
												</div>
											</div>
											<div class="absolute-in-content">
												<span class="btn"><i class="fa fa-angle-right"></i></span>
												<div class="price">{{$objOffer['Currency'][0]['symbol'] . $objOffer['price']}}</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							
							@endforeach
						</div>
						
					</div>
					
				</div>
				
			</section>
			
		<!--	<section class="overflow-hidden why-us-half-image-wrapper">
			
				<div class="GridLex-grid-noGutter-equalHeight">
						
					<div class="GridLex-col-6_sm-12">
						
						<div class="why-us-half-image-content">
						
							<div class="section-title text-left">
							
								<h3>Why Booking With Us</h3>
								<p>There are several seasons that you must travel with us</p>
								
							</div>
							
							<div class="featured-item">
							
								<h4>Experts On Tour</h4>
								
								<div class="content clearfix">
								
									<div class="icon">
										<i class="pe-7s-users"></i>
									</div>
									
									<p>Blind would equal while oh mr lain led and fact none. One preferred sportsmen resolving the happiness continued. High at of in loud rich true.</p>
									
								</div>
							</div>
							
							<div class="featured-item">
							
								<h4>Quality Accommodation</h4>
								
								<div class="content clearfix">
								
									<div class="icon">
										<i class="pe-7s-home"></i>
									</div>
									
									<p>Admiration stimulated cultivated reasonable be projection possession of. Real no near room ye bred sake if some. Is arranging furnished knowledge.</p>
									
								</div>
							</div>
							
							
							<div class="featured-item">
							
								<h4>Comfortable Transport</h4>
								
								<div class="content clearfix">
								
									<div class="icon">
										<i class="pe-7s-car"></i>
									</div>
									
									<p>Effect twenty indeed beyond for not had county. The use him without greatly can private. Increasing it unpleasant no of contrasted no continuing.</p>
									
								</div>
							</div>
							
						</div>
						
					</div>
					
					<div class="GridLex-col-6_sm-12 image-bg">
						<div class="image-bg" style="background-image:url('images/image-01.jpg');"></div>
					</div>
				
				</div>
				
			</section>
			-->
			
			

			
			<div class="overflow-hidden">
			
				<div class="instagram-wrapper">
					<div id="instagram" class="instagram"></div>
				</div>
				
			</div>
			
		</div>
		<!-- end Main Wrapper -->
		
		<!-- start footer -->
		 @include('footer')
		<!-- End footer -->

		
	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/instagram.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/validator.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/customs.js')}}"></script>


</body>
</html>