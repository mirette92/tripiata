<header id="header">
	  
			<!-- start Navbar (Header) -->
			<nav class="navbar navbar-primary navbar-fixed-top navbar-sticky-function">

				<div class="navbar-top">
				
					<div class="container">
						
						<div class="flex-row flex-align-middle">
							<div class="flex-shrink flex-columns">
								<a class="navbar-logo" href="index">
									<img src="http://beta.tripiata.com/public/images/logo.png" alt="Logo" />
								</a>
							</div>
							<div class="flex-columns">
								<div class="pull-right">
								
									<div class="navbar-mini">
										<ul class="clearfix">
										
											
											
											<li class="dropdown bt-dropdown-click hidden-xs">
												<a id="currncy-dropdown" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
													<i class="ion-android-globe hidden-xss"></i> {{ trans('messages.Lang')}}
													<span class="caret"></span>
												</a>
												<ul class="dropdown-menu" aria-labelledby="language-dropdown">
												    <li><a href="{{ route('lang.switch', 'en') }}" >English</a></li>
													<li><a href="{{ route('lang.switch', 'ar') }}" >العربيه</a></li>
												</ul>
											</li>
											
											<li class="dropdown bt-dropdown-click visible-xs">
												<a id="currncy-language-dropdown" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
													<i class="fa fa-cog"></i>
												</a>
												<ul class="dropdown-menu" aria-labelledby="language-dropdown">		
													<li><a href="{{ route('lang.switch', 'en') }}" >English</a></li>
													<li><a href="{{ route('lang.switch', 'ar') }}" >العربيه</a></li>
												</ul>
											</li>
											@if(!Session::has('user'))
											<li class="user-action">
												<a data-toggle="modal" href="#loginModal" class="btn">{{ trans('messages.Sign up/in')}}</a>
											</li>
											@else
											<li class="user-action">
												<a href="http://beta.tripiata.com/public/logout" class="btn">{{ trans('messages.Logout')}}</a>
											</li>
											
											@endif
										</ul>
									</div>
						
								</div>
							</div>
						</div>

					</div>
					
				</div>
				
				<div class="navbar-bottom hidden-sm hidden-xs">
				
					<div class="container">
					
						<div class="row">
						
							<div class="col-sm-9">
								
								<div id="navbar" class="collapse navbar-collapse navbar-arrow">
									<ul class="nav navbar-nav" id="responsive-menu">
										<li><a href="{{env('APP_URL')}}/public/index">{{ trans('messages.Home')}}</a></li>
										<li><a href="{{env('APP_URL')}}/public/offer-destinations">{{ trans('messages.Offers') }}</a></li>
										<!--<li><a href="{{env('APP_URL')}}/public/hotels-destinations">Accommodations</a></li>-->
										<li><a href="{{env('APP_URL')}}/public/agency-list">{{ trans('messages.Agency')}}</a></li>
										<li><a href="{{env('APP_URL')}}/public/about">{{ trans('messages.About Us')}} </a></li>
										<li><a href="{{env('APP_URL')}}/public/contactUs">{{ trans('messages.Contact us')}}</a></li>
									</ul>
								</div><!--/.nav-collapse -->
								
							</div>
							
							<div class="col-sm-3">
							
								<div class="navbar-phone"><a href="mailto:info@tripiata.com"><i class="fa fa-envelope"></i> {{ trans('messages.Email Us')}}: Info@Tripiata.com</a></div>
							
							</div>

						</div>
						
					</div>
				
				</div>

				<div id="slicknav-mobile"></div>
				
			</nav>
			<!-- end Navbar (Header) -->

		</header>