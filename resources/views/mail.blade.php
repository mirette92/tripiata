<!DOCTYPE html>
<html>
<head>

<title>sportoya </title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 
<style>
.fa {
     padding: 6px;
    font-size: 14px;
    width: 28px;
    height: 28px;
    text-align: center;
    text-decoration: none;
    margin: 3px 14px;
    border-radius: 50%

}

.fa:hover {
    
    text-decoration: none;
    background: #e5e5e5;
    color:#ff835d;
    border-color: #ff835d;
    border-width: 1px;
    border-style: solid;


}

.fa-facebook {
     border-color: #a6a8ab;
    border-width: 1px;
    border-style: solid;
    background: #e5e5e5;
    color: #a6a8ab;
}

.fa-instagram {
   border-color: #a6a8ab;
    border-width: 1px;
    border-style: solid;
    background: #e5e5e5;
    color: #a6a8ab;
}
.fa-envelope{
	border-color: #a6a8ab;
    border-width: 1px;
    border-style: solid;
    background: #e5e5e5;
    color: #a6a8ab;
}




body{
	    line-height: 1.1;
	    overflow:-webkit-paged-y;
}


  	.header{
  		
  		/*background: linear-gradient(to right, #ff9a59,  #ff5b61) ;*/
  		background-image: url("http://localhost/tamp/images/pattern.png"),linear-gradient(to right, #ff9a59,  #ff5b61);
  		width: 100%;
  		height: 72px;

  	}


  	.left {
    float: left;
    
	padding-top: 60px;
    padding-left: 13px;
   
}
.clear {
	clear:both;
}

.container-fluid {
    padding-right: 15px;
    
    margin-right: auto;
    margin-left: auto;
}

div{
	padding-left: 47px;
}

p {
    font-size: 16px;
    margin: -5px 2px 6px;
    color: #282828;
}

.border{
	border-radius: 10px;
	padding: 10px;
	border-width:1px;  
    border-style:outset;
    border-color: #ff9a59;
    border-bottom-width: 5px;
}


#example1 {
        border-style: solid;
    border-color: #282828;
    border-width: 2px;
    border-bottom-width: 19px;
    border-radius: 5px;
    height: 126px;
    width: 124px;

    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;

}

.im{


    
   
    background: url("http://localhost/tamp/images/emails2.jpg");
    background-repeat: no-repeat;
    background-size: auto;

	margin-left: 45%

	width: 110px;
  
    border-radius: 5px 5px 5px 5px;
    /* background-color: #ff835d; */
    background: linear-gradient(#ff9a59, #ff835d, #ff5b61);
}

.footer {
   position: fixed;
   padding-top: 26px;
   left: 0;
   bottom: 0;
   width: 100%;
   height: 87px;
   background-color:#e5e5e5;
   color:#a6a8ab;
   text-align: center;
}

footer a {
	

}
footer a:hover {
	opacity: 0.7;
}



@media screen and (min-width: 701px) {
 div  h3{
    font-size: 24px;
  }
}

@media screen and (max-width: 700px) {
 div   h3{
    font-size: 20px;
  }
}



@media screen and (min-width: 701px) {
 div  h4{
    font-size: 18px;
  }
}

@media screen and (max-width: 700px) {
 div   h4{
    font-size: 15px;
  }
}


@media screen and (min-width: 701px) {
 div  p{
    font-size: 16px;
  }
}

@media screen and (max-width: 700px) {
 div   p{
    font-size: 12px;
  }
}

@media screen and (min-width: 701px) {
.footer{
    height: 87px;
    padding-top: 26px;
  }
}

@media screen and (max-width: 700px) {
 .footer{
     height: 65px;
    padding-top: 14px;
  }
}



</style>
</head>
<body>
	<div class="header">

		<div class="left">
		   <img src="http://api.sportoya.com/images/logo2.png" width="190px" height="55px" >

	    </div>

	</div>


<div class="clear"></div>
	<div class="container-fluid ">
		<div class="row">
		 	
		 <div class="col-sm-6">
			<h4><strong>Hi {{$userName}},</strong> </h4> 
				
			
		    
		    
			  
			 

			 
			 
			 	<p>Application sent<p>
		
			<div class="col-sm-12">
	
				<img src="http://api.sportoya.com/images/signature.png"  alt=""  width="140" height="auto">
        	</div>
    
        	<div class="clear"></div>
         
            
		</div>
	</div>
<div class="clear"></div>

	<div class="clear"></div>

	 
	
</body>
</html>