<footer class="footer">
			
			<div class="container">
			
				<div class="main-footer">
				
					<div class="row">
				
						<div class="col-xs-12 col-sm-5 col-md-3">
						
							<div class="footer-logo">
								<img src="images/logo-white.png" alt="Logo" />
							</div>
							
							<p class="footer-address">324 Yarang Road, T.Chabangtigo, Muanng Pattani 9400 <br/>

							<i class="fa fa-envelope-o"></i> <a href="mailto:info@tripiata.com">info@tripiata.com</a></p>
							
							<div class="footer-social">
							
								<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-instagram"></i></a>
							<!--	<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>-->
							
							</div>
							
							<p class="copy-right">&#169; {{ trans('messages.Copyright 2018 Tripiata. All Rights Reserved') }}</p>
							
						</div>
						
						<div class="col-xs-12 col-sm-7 col-md-9">

							<div class="row gap-10">
							
								<div class="col-xs-12 col-sm-4 col-md-6 col-md-offset-3 mt-30-xs">
								
									<h5 class="footer-title">{{ trans('messages.About Tripiata') }}</h5>
									
									<ul class="footer-menu">
									
										<li><a href="static-page.html">{{ trans('messages.Who we are') }}</a></li>
										<li><a href="static-page.html">{{ trans('messages.Careers') }}</a></li>
										<li><a href="static-page.html">{{ trans('messages.Company history') }}</a></li>
										<li><a href="static-page.html">{{ trans('messages.Legal') }}</a></li>
										<li><a href="static-page.html">{{ trans('messages.Partners') }}</a></li>
										<li><a href="static-page.html">{{ trans('messages.Privacy notice') }}</a></li>
										
									</ul>
									
								</div>
								
								
								
								<div class="col-xs-12 col-sm-4 col-md-3 mt-30-xs">

									<h5 class="footer-title">{{ trans('messages.Links') }}</h5>
									
									<ul class="footer-menu">
									
										<li><a href="index.php">{{ trans('messages.Home') }}</a></li>
										<li><a href="offers-list.php">{{ trans('messages.Trips') }}</a></li>
										<li><a href="hotels.php">{{ trans('messages.Hotels') }}</a></li>
										<li><a href="agency.php">{{ trans('messages.Agency') }}</a></li>
										<li><a href="about.php">{{ trans('messages.About Us') }}</a></li>
										<li><a href="contact.php">{{ trans('messages.Contact us') }}</a></li>
										
									</ul>
									
								</div>
								
							</div>

						</div>
						
					</div>

				</div>
				
			</div>
			
		</footer>