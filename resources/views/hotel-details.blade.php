<!doctype html>
<html lang="en">


<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Book Your Hotel | Tripiata</title>
	<meta property="og:title" content=" Book Your Hotel | Tripiata"/>
	<meta name="description" content="Explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta name="keywords" content="Hotels, Holiday, vacation, booking, trip, travel, tourism, tourist, tripiata, camp, resorts" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<meta property="og:description" content=" explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta property="og:url" content=" https://www.tripiata.com/public/index"/>
	<meta property="og:type" content="website"/>	
	
	<meta name="twitter:card" content="summary" />	
	<meta name="twitter:title" content=" Book Your Hotel | Tripiata" />	
	<meta name="twitter:description" content="explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel" />	
	<meta name="twitter:url" content="https://www.tripiata.com/public/index" />	
	
	<!-- Fav and Touch Icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png')}}">
	<link rel="shortcut icon" href="images/ico/favicon.png">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}" media="screen">	
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet">
	<link href="{{ asset('css/component.css')}}" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="{{ asset('icons/ionicons/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/simple-line-icons/css/simple-line-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/themify-icons/themify-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/rivolicons/style.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- CSS Custom -->
		<link href="{{ asset('css/style.css')}}" rel="stylesheet"><link href="{{ asset('css/color-02.css')}}" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	@include ('login')
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper">

		<!-- start Header -->
		@include('nav')
		<!-- End Header -->
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('{{env('APP_URL')}}/public/images/hero-header/hotels-image.jpg');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Accommodation</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="index">Homepage</a></li>
								<li><a href="hotels-destinations">Destinations</a></li>
								<li><a href="#">Accommodations</a></li>
								<li><span>Accommodation Details</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
			
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-8 col-md-9">
						
							<div class="blog-wrapper">

								<div class="blog-item blog-single">
								
									<div class="blog-media">
										<img src="{{$objHotel[0]['img']}}" class="extra-height" alt="" />
									</div>
											
									<div class="clear"></div><hr>
									
									<div class="blog-content">
										<h4>About Accommodation</h4>
										
										<div class="blog-entry">  
											
											<p>{{$objHotel[0]['desc']}} </p>
									
											
										</div>
									</div>
									
									<div class="clear"></div><hr>
									@if(!empty($objHotel[0]['Offer']))
									<div class="section-title text-left">
											<h4>Hotel Offers</h4>
									</div>
									<div class="GridLex-gap-20-wrappper package-grid-item-wrapper on-page-result-page alt-smaller">
					
										<div class="GridLex-grid-noGutter-equalHeight">
										@foreach($objHotel[0]['Offer'] as $objOffer)
											<div class="col-sm-6 col-md-6 col-lg-6 mb-20">
												<div class="package-grid-item"> 
													<a href="{{env('APP_URL')}}/public/offer-details?offer_id={{$objOffer['id']}}">
														<div class="image">
															<img src="{{App\MediaUrl::getUrl().$objOffer['img']}}" alt="" />
															<div class="absolute-in-image">
																<div class="duration"><span>@if($objOffer->Differ == 0) Day Use @elseif($objOffer->Differ >0){{$objOffer->Differ + 1}} days {{$objOffer->Differ}} nights @endif</span></div>
															</div>
														</div>
														<div class="content clearfix">
															<h6>{{$objOffer['name']}}</h6>
															<div class="rating-wrapper">
																<div class="raty-wrapper">
																<!--	<div class="star-rating-12px" data-rating-score="{{$objOffer['TotalRate']}}"></div> <span> / {{$objOffer['arrReview']}} review</span>-->
																	<div class="star-rating-12px" data-rating-score="{{$objOffer['TotalRate']}}"></div> 
																	By <a href="{{env('APP_URL')}}/public/agency-detail?agency_id={{$objOffer['agency']['id']}}">{{$objOffer['agency']['name']}}</a>
														</span>
																</div>
															</div>
															<div class="absolute-in-content">
																<span class="btn"><i class="fa fa-angle-right"></i></span>
																
																<div class="price">{{$objOffer['Currency']['symbol'].$objOffer['price']}}</div>
															</div>
															</div>
													</a>
												</div>
											</div>
											@endforeach
										
											
										
											
											
											
											
											
										</div>
									</div>
									@endif
									<div class="pager-wrappper mt-30 clearfix">
			
								<div class="pager-innner">
								
									<div class="flex-row flex-align-middle">
											
										
										<div class="flex-column flex-sm-12">
											<nav class="pager-right">
											
												{!! $arrOffer->appends([
                                                'search' => '',
                                                
                                            ])->links() !!} 
												<ul class="pagination">
												
												</ul>
											</nav>
										</div>
									
									</div>
									
								</div>
								
							</div>

									<div class="clear"></div>
									
									
									</div>
								
							</div>
						
						</div>
						
						<div class="col-sm-4 col-md-3 mt-50-xs">
						
							<aside class="sidebar">
						
								<div class="sidebar-inner no-border for-blog">

									<div class="sidebar-module">
										<h3 class="sidebar-title">{{$objHotel[0]['name']}}</h3>
										<div class="sidebar-module-inner">
											<ul class="sidebar-category">
												<li><a href="#">Number of offers<span>{{$objHotel[0]['OfferCount']}}</span></a></li>
												<li><a href="#">Email<span>{{$objHotel[0]['email']}}</span></a></li>
												<!-- <li><a href="#">Rating<span>{{$objHotel[0]['TotalRate']}}</span></a></li>
												<li><a href="#">Reviews<span>{{count($objHotel[0]['arrReview'])}}</span></a></li> -->
												
												<li><a href="#">Star<span>{{$objHotel[0]['star']}}</span></a></li>
											</ul>
										</div>
										
										
										<div class="clear"></div><br>
										
										@if(!empty($objHotel[0]['facilities']))
										<div class="detail-content">
								
									<div class="section-title text-left">
										<h4>Facilities</h4>
									</div>
									
									<ul class="list-with-icon with-heading">
										@foreach($objHotel[0]['facilities'] as $obj)
										<li>
											<i class="fa fa-check-circle text-primary"></i>
											<h6 class="heading mt-0">{{$obj['name']}}</h6>
										</li>
										@endforeach
									
										
									
										
									</ul>
									
									
								</div>
									@endif
										
									</div>
									

									<div class="clear"></div>

								</div>
							
							</aside>
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

		<!-- start footer -->
		@include ('footer')
		<!-- End footer -->

	</div>  <!-- end Container Wrapper -->
 

 
<!-- start Back To Top -->
<div id="back-to-top">
   <a href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->



<!-- JS -->
<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/instagram.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/validator.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/customs.js')}}"></script>

</body>

</html>