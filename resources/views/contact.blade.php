<!doctype html>
<html lang="en">


<!-- Mirrored from crenoveative.com/envato/tour-packer/contact.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Aug 2018 18:28:14 GMT -->
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Book Your Hotel | Tripiata</title>
	<meta property="og:title" content=" Book Your Hotel | Tripiata"/>
	<meta name="description" content="Explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta name="keywords" content="Hotels, Holiday, vacation, booking, trip, travel, tourism, tourist, tripiata, camp, resorts" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<meta property="og:description" content=" explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta property="og:url" content=" https://www.tripiata.com/public/index"/>
	<meta property="og:type" content="website"/>	
	
	<meta name="twitter:card" content="summary" />	
	<meta name="twitter:title" content=" Book Your Hotel | Tripiata" />	
	<meta name="twitter:description" content="explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel" />	
	<meta name="twitter:url" content="https://www.tripiata.com/public/index" />	
	
	<!-- Fav and Touch Icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/ico/apple-touch-icon-144-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/ico/apple-touch-icon-114-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/ico/apple-touch-icon-72-precomposed.png')}}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png')}}">
	<link rel="shortcut icon" href="{{ asset('images/ico/favicon.png')}}">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}" media="screen">	
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet">
	<link href="{{ asset('css/component.css')}}" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="{{ asset('icons/ionicons/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/simple-line-icons/css/simple-line-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/themify-icons/themify-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/rivolicons/style.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- CSS Custom -->
		<link href="{{ asset('css/style.css')}}" rel="stylesheet"><link href="css/color-02.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	@include('login')
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper">

		<!-- start Header -->
		@include('nav')
		<!-- End Header -->
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('images/hero-header/breadcrumb.jpg');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">Contact Us</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="index.php">Homepage</a></li>
								<li><span>Contact Us</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
	
				
				<div class="section pt-0 pb-0">
				
					<div class="container">
					
						<div class="row">

							<div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
								
								<div class="section-title">
								
									<h3>Keep in touch</h3>
									
								</div>
								
							</div>
						
						</div>
						
						<form class="contact-form-wrapper" data-toggle="validator">
						
							<div class="row">
							
								<div class="col-sm-4">

									<h5 class="heading mt-5">Let's Social</h5>
									<p>May indulgence difficulty ham can put especially. Bringing remember for supplied her why was confined. Middleton principle did she procuring extensive believing add. Weather adapted prepare oh is calling.</p>
									
									<div class="boxed-social mb-30-xs clearfix">
										
										<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
										<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
										<a href="#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
										<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>
									
									</div>
								
								</div>
								
								<div class="col-sm-8">
								
									<div class="row">
									
										<div class="col-sm-6">
										
											<div class="form-group">
												<label for="inputName">Your Name <span class="font10 text-danger">(required)</span></label>
												<input id="inputName" type="text" class="form-control" data-error="Your name is required" required>
												<div class="help-block with-errors"></div>
											</div>
											
											<div class="form-group">
												<label for="inputEmail">Your Email <span class="font10 text-danger">(required)</span></label>
												<input id="inputEmail" type="email" class="form-control" data-error="Your email is required and must be a valid email address" required>
												<div class="help-block with-errors"></div>
											</div>
											
											<div class="form-group">
												<label>Subject</label>
												<input type="text" class="form-control" />
											</div>
										
										</div>
										
										<div class="col-sm-6">
										
											<div class="form-group">
												<label for="inputMessage">Message <span class="font10 text-danger">(required)</span></label>
												<textarea id="inputMessage" class="form-control" rows="9" data-minlength="50" data-error="Your message is required and must not less than 50 characters" required></textarea>
												<div class="help-block with-errors"></div>
											</div>
										
										</div>
										
										<div class="col-sm-12 text-right text-left-sm">
											<button type="submit" class="btn btn-primary mt-5">Send Message</button>
										</div>
										
									</div>

								</div>
							
							</div>
						
						</form>
						
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

		<!-- start footer -->
		@include('footer')
		<!-- End footer -->

	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


 
<!-- JS -->
<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/instagram.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/validator.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/customs.js')}}"></script>

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{{ asset('js/MarkerClusterer.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/infobox.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.mosne.map-for-category.js')}}"></script>
<script>
$(function () {

	var mycolor = "#ff0066";
	var mycolor2 = "#966E7E";
	var mybg_color = "#F7F7F7";

	var cluster_styles = [{
			url: 'images/m3.png',
			height: 30,
			width: 30,
			opt_textSize: 14,
			anchor: [3, 0],
			textColor: '#222222'
	}, {
			url: 'images/m4.png',
			height: 40,
			width: 40,
			opt_textSize: 17,
			opt_anchor: [6, 0],
			opt_textColor: '#222222'
	}, {
			url: 'images/m5.png',
			width: 50,
			height: 50,
			opt_textSize: 21,
			opt_anchor: [8, 0],
			opt_textColor: '#222222'
	}, {
			url: 'images/m5.png',
			width: 50,
			height: 50,
			opt_textSize: 21,
			opt_anchor: [8, 0],
			opt_textColor: '#222222'
	}];

	var my_cat_style ={
		place1: { icon: 'images/map-marker/01-marker.png'},
		place2: { icon: 'images/map-marker/02-marker.png'},
		place3: { icon: 'images/map-marker/03-marker.png'},
	};
		 
	var mapOptions = {
			zoom: 2,
			center: new google.maps.LatLng(25.2048, 55.2708),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			panControl: false,
			rotateControl: false,
			streetViewControl: false,
			scrollwheel: false,
	};
	
	$("#map_5").mosne_map({
			elements: '#map_5_list .maplocation',
			cluster_styles: {
					zoomOnClick: true,
					maxZoom: 5,
					styles: cluster_styles,
			},
			cat_style: my_cat_style ,
			zoom: 2,
			clickedzoom: 2, 	
			infowindows: false,  	
			infobox: true,   			
			map_opt: mapOptions,								
		
	});

});
</script>

</body>


<!-- Mirrored from crenoveative.com/envato/tour-packer/contact.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Aug 2018 18:28:28 GMT -->
</html>