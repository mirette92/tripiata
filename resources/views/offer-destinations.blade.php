<!doctype html>
<html lang="en">


<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Book Your Hotel | Tripiata</title>
	<meta property="og:title" content=" Book Your Hotel | Tripiata"/>
	<meta name="description" content="Explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta name="keywords" content="Hotels, Holiday, vacation, booking, trip, travel, tourism, tourist, tripiata, camp, resorts" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<meta property="og:description" content=" explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel"/>
	<meta property="og:url" content=" https://www.tripiata.com/public/index"/>
	<meta property="og:type" content="website"/>	
	
	<meta name="twitter:card" content="summary" />	
	<meta name="twitter:title" content=" Book Your Hotel | Tripiata" />	
	<meta name="twitter:description" content="explore and compare between different hotels , enjoy booking your best hotels , resorts , camps with different prices categories and facilities with great offers and deals, Tripiata book your hotel" />	
	<meta name="twitter:url" content="https://www.tripiata.com/public/index" />	
	
		<!-- Fav and Touch Icons -->
	<link rel="shortcut icon" href="{{ asset('images/ico/favicon.png')}}">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css')}}" media="screen">	
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet">
	<link href="{{ asset('css/main.css')}}" rel="stylesheet">
	<link href="css/component.css" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="{{ asset('icons/ionicons/css/ionicons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/simple-line-icons/css/simple-line-icons.css')}}">
	<link rel="stylesheet" href="{{ asset('icons/rivolicons/style.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,300italic,300,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

	<!-- CSS Custom -->
	<link href="{{ asset('css/style.css')}}" rel="stylesheet">
	<link href="{{ asset('css/color-02.css')}}" rel="stylesheet">
	
</head>

<body class="">

	<!-- BEGIN # MODAL LOGIN -->
	@include('login')
	<!-- END # MODAL LOGIN -->
	
	<!-- start Container Wrapper -->
	<div class="container-wrapper">

		<!-- start Header -->
		@include('nav')
		<!-- End Header -->
		
		<div class="clear"></div>
		
		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- start end Page title -->
			<div class="page-title" style="background-image:url('images/hero-header/destinations-image.jpg');">
				
				<div class="container">
				
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
						
							<h1 class="hero-title">{{ trans('messages.Destinations') }}</h1>
							
							<ol class="breadcrumb-list">
								<li><a href="index">{{ trans('messages.Home') }}</a></li>
								<li><span>{{ trans('messages.Destinations') }}</span></li>
							</ol>
							
						</div>
						
					</div>

				</div>
				
			</div>
			<!-- end Page title -->
			
			<div class="content-wrapper">
			
				<div class="container">
				
					<div class="row">
						
						<div class="col-sm-4 col-md-3">
							
							<aside class="sidebar with-filter">
				
								<div class="sidebar-search-wrapper bg-light-2">
								
									<div class="sidebar-search-header">
										<h4>{{ trans('messages.Search') }}</h4>
									</div>
									<form action="offers-list" method="get">
									<div class="sidebar-search-content">

										<div class="form-group">
											
											<select name="keyword_id" class="select2-multi form-control" data-placeholder="{{ trans('messages.Choose Your Keyword') }}" >
												<option value="">{{ trans('messages.Choose Your Keyword') }}</option>
													
    											@foreach(App\Keyword::listKeyword() as $objKeyword)
    											<option value="{{$objKeyword['id']}}">{{$objKeyword['name']}}</option>
    											@endforeach
											</select>

										</div>
									
										<div class="form-group">
														
											<select name="section_id" class="select2-multi form-control" data-placeholder="{{ trans('messages.Choose your Section') }}" >
												<option value="">{{ trans('messages.Choose your Section') }}</option>
												@foreach(App\Section::listSection() as $objSection)
												<option value="{{$objSection['id']}}">{{$objSection['name']}}</option>
											@endforeach
											</select>
											
										</div>
										
										
									
										<!-- <a href="#" class="btn btn-primary btn-block">Search</a> -->
										<button class="btn btn-primary btn-block" type="submit">{{ trans('messages.Search') }}</button>
									</div>
									</form>
								</div>
								
								
							
							</aside>
							
							
						</div>
						
						<div class="col-sm-8 col-md-9">
							
							<div class="sorting-wrappper">
			
								<div class="sorting-header">
									<h3 class="sorting-title uppercase">{{ trans('messages.Choose your destination') }}</h3>
									<p class="sorting-lead"></p>
								</div>
								
								

							</div>
							
							<div class="GridLex-gap-20-wrappper package-grid-item-wrapper on-page-result-page alt-smaller">
						
								<div class="GridLex-grid-noGutter-equalHeight">
								
									@foreach($arrDestination as $objDestination)	
													
									<div class="GridLex-col-4_sm-6_xs-12 mb-20">
										<div class="package-grid-item"> 
											<a href="offers-list?destination_id={{$objDestination['id']}}">
												<div class="image">
													<img src="{{App\MediaUrl::getUrl().$objDestination['img']}}" alt="Tripiata" />
													<div class="absolute-in-image">
														<div class="frame text-center"><span>{{$objDestination['name']}}</span></div>
													</div>
												</div>
												
											</a>
										</div>
									</div>
									
									@endforeach
									
								</div>
							
							</div>
							
						
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			

		</div>
		<!-- end Main Wrapper -->

		<!-- start footer -->
		 @include('footer')
		<!-- End footer -->

	</div>  <!-- end Container Wrapper -->
 

 
	<!-- start Back To Top -->
	<div id="back-to-top">
		 <a href="#"><i class="fa fa-angle-up"></i></a>
	</div>
	<!-- end Back To Top -->


 
<!-- JS -->
<script type="text/javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/instagram.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/ion.rangeSlider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/validator.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.raty.js')}}"></script> 
<script type="text/javascript" src="{{ asset('js/customs.js')}}"></script>



</body>

</html>