<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;
use App\Helpers\translationHelper;


class Currency extends Model
{
    use SoftDeletes,Translatable;
    protected $table = 'currencies';
    protected $fillable = ['id','name','symbol','updated_at','created_at'];
    protected $translatable = ['name','symbol'];

    public function getCurrencyById($currency_id){
        $objCurrency = $this->where('id',$currency_id)->first();

        return $objCurrency;
    }

    public static function getCurrency(){
        $arrCurrency = Currency::get();

        return $arrCurrency;
    }
}
