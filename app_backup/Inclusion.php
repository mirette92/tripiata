<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Inclusion extends Model
{
 	 protected $table = 'inclusions';
    protected $fillable = ['id','name','type'];
	public function getexclude($offer_id){
	$arrIds = DB::table('offer_inclusions')->where('offer_id',$offer_id)->pluck('inclusion_id');
	//dd($arrIds);
	$arr = $this->whereIn('id',$arrIds)->where('type','Exclude')->get();
	//dd($arr);
return $arr;
	}
	public function getinclude($offer_id){
	$arrIds = DB::table('offer_inclusions')->where('offer_id',$offer_id)->pluck('inclusion_id');
	$arr =  $this->whereIn('id',$arrIds)->where('type','Include')->get();
	return $arr;
	}   
}
