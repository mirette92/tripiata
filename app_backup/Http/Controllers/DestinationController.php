<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Destination;
use App\City;

class DestinationController extends Controller
{
    //
    
    public function ListDestinationAttachedtoOffer(){
        $objDestination = new Destination();
        $arrDestination = $objDestination->listDestinationAttachedtoOffer();
        $data = array ('arrDestination'=>$arrDestination);
        return view('offer-destinations')->with($data);
        
    }
    
    
    public function ListDestinationAttachedtoHotel(){
        $objDestination = new Destination();
        $objCity = new city();
        
         $arrCityIds= $objCity->listCityAttachedtoOffer();
         $arrDestination= $objDestination->listDestinationAttachedtoHotel($arrCityIds);
        dd($arrDestination);
        $data = array ('arrDestination'=>$arrDestination);
        return view('Hotel-destinations')->with($data);
        
    }
    
   
}
