<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use App\Respond;
use App\Offer;


class HotelController extends Controller
{
    //
    public function ListHotel(Request $request){
        $lang = 'en';
        $arr = array();
        $objHotel = new Hotel();
        $arrHotel = $objHotel->listHotel($lang,$request->search,$request->city_id);
        //dd($arrHotel);
        $data = array ('arrHotel'=>$arrHotel,'search'=>$request->search,'city_id'=>$request->city_id);
        return view('hotels')->with($data);
        return view('hotels',$data);
        $arr['result'] = $arrHotel;
        $arr = Respond::mergeRespond($arr,200);
        //dd($arr);
        return $arr;
    }
    public function GetHotelById(Request $request){
        $lang = 'en';
        $arr = array();
        $objHotel = new Hotel();
        $objHotel = $objHotel->getHotelByIdforweb($lang, $request->hotel_id);
        $objOffer = new Offer();
        
        $arrOffer = $objOffer->getOfferAttachedtoHotel($objHotel[0]->id);
        //dd($arrOffer);
        $data = array ('objHotel'=>$objHotel,'arrOffer'=>$arrOffer);
        return view('hotel-details')->with($data);
        return  $objHotel;
    }
}
