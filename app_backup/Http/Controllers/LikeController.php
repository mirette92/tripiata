<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Like;
use App\Respond;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    //

    public function AddLike(Request $request){
        //dd($request->user_id);
        $arr = array();
        // $objUser = new UserController();
        //print(Auth::User());die;
        $intUserID = $request->user_id;
        $objLike = new Like();
        $Like = $objLike->LikeByUser($request->comment_id,$intUserID);
        if($Like!=null){
            if($Like->isLike == $request->like){
                $result = $objLike->removeLike($request->comment_id,$intUserID);
                
                if($result){
                    $arr = Respond::mergeStatus($arr,200);
                    return $arr;
                }else{
                    $arr = Respond::mergeStatus($arr,4012);
                    return $arr;
                }
                
            }else if($Like->isLike != $request->like){
                $result = $objLike->removeLike($request->comment_id,$intUserID);
                
                if(!$result){
                    $arr = Respond::mergeStatus($arr,4012);
                    return $arr;
                }
            }
        }
        
        $objLike->user_id = $intUserID;
        $objLike->comment_id = $request->comment_id;
        $objLike->isLike = $request->like;
        $result = $objLike->save();
        if($result){
            $arr = Respond::mergeStatus($arr,200);
        }else{
            $arr = Respond::mergeStatus($arr,4012);
        }
        return $arr;
    }
    public function RemoveLike(Request $request){
        $arr = array();
        
        $objUser = new UserController();
        $intUserID = $objUser->GetUserID();
        $objLike = new Like();
        $result = $objLike->removeLike($request->comment_id,$intUserID);
        if($result){
            $arr = Respond::mergeStatus($arr,200);
        }else{
            $arr = Respond::mergeStatus($arr,4012);
        }
        return $arr;
    }
}
