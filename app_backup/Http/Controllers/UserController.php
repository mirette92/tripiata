<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
class UserController extends Controller
{
    //
    public function GetUserID(){
        $user = Auth::user();
        //dd(Auth);
        if($user == null){
            return null;
        }
        
        foreach($user->tokens as $token) {
            $id = $token->user_id;
            return $id;
        }
    }
    
    
    public function LoginWeb(Request $request){
        //dd('hiii');
        //dd($request);
        $arr = array();
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            
            $user = Auth::user();
            session()->put('user',$user);
            return redirect()->back();
           
        }else{
            return redirect()->back()->with('fail', 'failed to login,please check username and password');
            
       
        }
        
    }
    public function RegisterWeb(Request $request){
        $arr = array();
        $input = $request->all();
        $objuser = new User();
        $res = $objuser->getUserByEmail($input['email']);
        if($res != null || $res != ""){
            //dd('hi');
            return redirect()->back()->with('fail', 'Looks like you already have an account. Please log in instead.');
        }
        $input['password'] = bcrypt($input['password']);
        $input['role_id'] = 4;
        $user = User::create($input);
        $this->loginWeb($request);
        return redirect()->back();
        
    }
    
    public function logout(){
        session()->forget('user');
        return redirect()->back();
    }
}
