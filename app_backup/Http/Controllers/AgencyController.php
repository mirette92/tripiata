<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Application;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;

use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use App\Agency;
use App\Offer;
use App\User;
use App\Status;
use TCG\Voyager\Http\Controllers\Controller;
use TCG\Voyager\Events\BreadDataAdded;

use TCG\Voyager\Events\BreadDataDeleted;

use TCG\Voyager\Events\BreadDataUpdated;

use TCG\Voyager\Events\BreadImagesDeleted;
use App\Respond;

class AgencyController extends Controller
{
    use BreadRelationshipParser;
    
    
    public function ListAgency(Request $request){
        $lang = 'en';
        $arr = array();
        $objAgency = new Agency();
        $arrAgency = $objAgency->listAgency($lang,$request->search);
        
        $data = array ('arrAgency'=>$arrAgency,'search'=>$request->search);
        return view('agency')->with($data);
        
        $arr['result'] = $arrAgency;
        $arr = Respond::mergeRespond($arr,200);
        //dd($arr);
        return $arr;
    }
    
    public function GetAgencyById(Request $request){
        //dd($request->agency_id);
        $lang = 'en';
        $objAgency = new Agency();
        $objAgency = $objAgency->getAgencyByIDforweb($request->agency_id, $lang);
        
        $objOffer = new Offer();
        
        $arrOffer = $objOffer->getOffer($request->agency_id);
       // dd($arrOffer);
        //$data = array ('objHotel'=>$objHotel,'arrOffer'=>$arrOffer);
        $data = array ('objAgency'=>$objAgency,'arrOffer'=>$arrOffer);
        return view('agency-details')->with($data);
        return $objAgency;
    }
    
    public function create(Request $request)
    
    {
        
        
        $slug = $this->getSlug($request);
        //dd($slug);
        
        
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        
        
        // Check permission
        
        $this->authorize('add', app($dataType->model_name));
        
        
        
        $dataTypeContent = (strlen($dataType->model_name) != 0)
        
        ? new $dataType->model_name()
        
        : false;
        
        
        
        foreach ($dataType->addRows as $key => $row) {
            
            $details = json_decode($row->details);
            
            $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
            
        }
        
        
        
        // If a column has a relationship associated with it, we do not want to show that field
        
        $this->removeRelationshipField($dataType, 'add');
        
        
        
        // Check if BREAD is Translatable
        
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        
        
        
        $view = 'voyager::bread.edit-add';
        
        
        
        if (view()->exists("voyager::$slug.edit-add")) {
            
            $view = "voyager::$slug.edit-add";
            
        }
        
        
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
        
    }
    
    
    
    public function store(Request $request)
    {
        $objAgency = new Agency();
        $agency = $objAgency->getAgency(Auth::user()->id);
        $slug = $this->getSlug($request);
        
        //dd($request);
        
        $input = $request->all();
        $input['agency_id'] = $agency[0]['id'];
        $objOffer = new Offer;
        
        $result =  Offer::create($input);;
        
        return back()->with([
            'message'    => __('add successfully'),
            'alert-type' => 'success',
        ]);
        
    }
    
    public function getSlug(Request $request)
    
    {
        
        if (isset($this->slug)) {
            
            $slug = $this->slug;
            
        } else {
            
            $slug = explode('.', $request->route()->getName())[1];
            
        }
        
        
        
        return $slug;
        
    }
    
    public function index(Request $request)
    
    {
        //dd(Auth::user());
        
        $objAgency = new Agency();
        $agency = $objAgency->getAgency(Auth::user()->id);
        //dd($agency );
        // Check permission
        
        // $this->authorize('browse', model(PromoCodeGroupController::class));
        
        $isServerSide = false;
        
        
        
        $dataType = Voyager::model('DataType')->first();
        
        
        
        
        
        
        
        
        $slug = $this->getSlug($request);
        
        //dd($slug);
        
        // GET THE DataType based on the slug
        
        
        
        $dataType = Voyager::model('DataType')::where('slug', '=', $slug)->first();
        
        //dd($dataType);
        
        // Check permission
        
        Voyager::can('browse_'.$dataType->name);
        
        
        
        // Next Get the actual content from the MODEL that corresponds to the slug DataType
        
        /*     $dataTypeContent = (strlen($dataType->model_name) != 0)
        
        ? app($dataType->model_name)->latest()->get()
        
        : DB::table($dataType->name)->get(); // If Model doest exist, get data from table name
        
        
        */
        $orderObj = new Agency();
        //dd($agency[0]['id']);
        //dd(Auth::user()->role_id);
        if(Auth::user()->role_id == 3 ){
            //dd('hiii');
            $dataTypeContent = $orderObj->where('user_id',Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        }else{
            $dataTypeContent = $orderObj->orderBy('created_at', 'DESC')->get();
        }
        //dd($dataTypeContent);
        
        $view = 'agencies.browse';
        
        
        //return view($view, compact('dataType', 'dataTypeContent'));
        return Voyager::view('voyager::agencies.browse')->with(compact('dataType', 'dataTypeContent'));
        
        
        if (view()->exists("voyager::$slug.browse")) {
            
            $view = "voyager::$slug.browse";
            
        }
        
        // dd($view);
        
        return view($view, compact('data', 'dataTypeContent'));
        
        
        
        //return Voyager::view('voyager:places.browse')->with(compact('dataType', 'data'));
        
    }
    
    
    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        
        $relationships = $this->getRelationships($dataType);
        
        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? app($dataType->model_name)->with($relationships)->findOrFail($id)
        : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name
        
        foreach ($dataType->editRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');
        
        // Check permission
        $this->authorize('edit', $dataTypeContent);
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        
        $view = 'voyager::bread.edit-add';
        
        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        
        
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        
        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        
        // Check permission
        $this->authorize('edit', $data);
        
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);
        
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }
        
        if (!$request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
            
            event(new BreadDataUpdated($dataType, $data));
            
            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                'alert-type' => 'success',
                ]);
        }
    }
    
    
    public function delete($id)
    {
        // Check permission
        //$this->authorize('delete', Voyager::model('Setting'));
        
        //Voyager::model('PromoCode')->destroy($id);
        // dd('delete');
        return back()->with([
            'message'    => __('voyager.settings.successfully_deleted'),
            'alert-type' => 'success',
        ]);
    }
    
    
    
    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        // Check permission
        $this->authorize('delete', app($dataType->model_name));
        
        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL or Model Binding
            $ids[] = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            $this->cleanup($dataType, $data);
        }
        
        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;
        
        $res = $data->destroy($ids);
        $data = $res
        ? [
            'message'    => __('deleted successfully')." {$displayName}",
            'alert-type' => 'success',
            ]
            : [
                'message'    => __('error while deleting')." {$displayName}",
                'alert-type' => 'error',
                ];
            
            if ($res) {
                event(new BreadDataDeleted($dataType, $data));
            }
            
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }
    
    public function deleteBreadImages($data, $rows)
    {
        foreach ($rows as $row) {
            if ($data->{$row->field} != config('voyager.user.default_avatar')) {
                $this->deleteFileIfExists($data->{$row->field});
            }
            
            $options = json_decode($row->details);
            
            if (isset($options->thumbnails)) {
                foreach ($options->thumbnails as $thumbnail) {
                    $ext = explode('.', $data->{$row->field});
                    $extension = '.'.$ext[count($ext) - 1];
                    
                    $path = str_replace($extension, '', $data->{$row->field});
                    
                    $thumb_name = $thumbnail->name;
                    
                    $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                }
            }
        }
        
        if ($rows->count() > 0) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        
        $relationships = $this->getRelationships($dataType);
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model->with($relationships), 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }
        
        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');
        
        // Check permission
        $this->authorize('read', $dataTypeContent);
        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        
        $view = 'voyager::bread.read';
        
        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }
        
        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }
        
        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->where('type', 'image'));
        
        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            $files = json_decode($data->{$row->field});
            if ($files) {
                foreach ($files as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }
    }
    
}
