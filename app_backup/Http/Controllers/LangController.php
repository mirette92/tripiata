<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LangController extends Controller
{
    //
    public function switchLang(Request $request)
    {
     
       
            //dd('mm');
            Session::put('locale', $request->lang);
   
        return Redirect::back();
    }
}
