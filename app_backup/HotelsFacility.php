<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class HotelsFacility extends Model
{
    protected $table = 'hotels_facilities';
    
    public function getFacilitiesIds($hotel_id){
        $arrFacilityIds = $this->where('hotel_id',$hotel_id)->pluck('facility_id');
        return $arrFacilityIds;
    }
    
}
