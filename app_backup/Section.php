<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class Section extends Model
{
    use SoftDeletes,Translatable;
    protected $translatable = ['name'];
    
    
    public static function listSection(){
        $arrSection = Section::orderBy('name','ASC')->get();
        return $arrSection;
    }
}
