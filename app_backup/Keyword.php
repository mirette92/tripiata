<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class Keyword extends Model
{
    use SoftDeletes,Translatable; 
    protected $translatable = ['name'];
    
    
    public static function listKeyword(){
        
        $arrKeyword = Keyword::orderBy('name','ASC')->get();
        //$arrDestinationTrans = $arrDestination->translate($lang,'en');
        //$arrDestination2 = translationHelper::translatedCollectionToArray($arrDestinationTrans);
        return $arrKeyword;
    }
    
    
}
