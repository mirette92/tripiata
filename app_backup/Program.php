<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Support\Facades\Auth;
use App\Currency;

class Program extends Model
{
    use SoftDeletes,Translatable;
    protected $translatable = ['name','desc'];

    protected $fillable = ['id','name','desc','hotel_id','price','offer_id','from','to','agency_id','updated_at','created_at','currency_id'];
    public function getOfferIDs($hotel_id){
        $arrOfferIDs = $this->where('hotel_id',$hotel_id)->pluck('offer_id');

        return $arrOfferIDs;
    }

    public function getProgramAttachedtoOffer($offer_id){
        $arrProgram = $this->where('offer_id',$offer_id)->get();
        foreach($arrProgram as $objProgram){
            $objCurrency = new Currency();
            $objProgram->Currency = $objCurrency->getCurrencyById($objProgram->currency_id);
            $objHotel = new Hotel();
            $objProgram->hotel = $objHotel->getHotelById($objProgram->hotel_id);
        }
        if(count($arrProgram)>0){
            return $arrProgram;
        }
        return null;
    }
    public function save(array $options = [])
    {
        $objAgency = new Agency();
        $agency = $objAgency->getAgency(Auth::user()->id);


        //dd($request);

        //$input = $request->all();
        $this->agency_id = $agency[0]['id'];
        parent::save();
    }
}
