<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class Country extends Model
{
    use SoftDeletes,Translatable;
    protected $fillable = ['id','name'];
    protected $translatable = ['name'];
    
}
