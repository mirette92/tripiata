<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use App\Review;


class Agency extends Model
{
    use SoftDeletes,Translatable;
    protected $table = 'agencies';
    protected $fillable = ['id','name','phone','facebook','verified','biography','user_id','updated_at','created_at'];
    protected $translatable = ['name','biography'];
    public function getAgency($user_id){
        $objAgency = $this->where('user_id',$user_id)->get();
        if(count($objAgency)>0){
            return $objAgency;
        }
        return null;
    }
    
    public function getAgencyById($agency_id){
        $objAgency = $this->where('id',$agency_id)->first();
        $objReview = new Review();
        $objOffer = new Offer();
        $objAgency->arrReview = $objReview->listReviewAttachedtoAgency($objAgency->id);
        $objAgency->offers = count($objOffer->getOffer($objAgency['id']));
        $objAgency->TotalRate = $objReview->getReviewRateAttachedToAgency($objAgency->id);
        return $objAgency;
    }
    
    public function listAgency($lang,$search){
        //dd($lang);
        $arrAgency = $this->where('name','like', '%'.$search.'%')->paginate(9);
        $arrAgencyTrans = $arrAgency->translate($lang,'en');
        foreach($arrAgency as $Agency){
            $Agency->img = MediaUrl::getUrl().$Agency->img;
            $objOffer = new Offer();
            if(!empty($arrAgency)){
               
                
                $objReview = new Review();
                
                $Agency->arrReview = count($objReview->listReviewAttachedtoAgency($Agency['id']));
                $Agency->TotalRate = $objReview->getReviewRateAttachedToAgency($Agency['id']);
                $Agency->offers = count($objOffer->getOffer($Agency['id']));
                
            }else{
                $arrHotel->arrReview = [];
                $Hotel->TotalRate = 0;
                return $arrHotel;
            }
        }
        $arrAgency2= translationHelper::translatedCollectionToArray($arrAgencyTrans);
        $arrAgency2= translationHelper::paginationForTranslator($arrAgency,$arrAgency2);
        //         dd($arrHotel2);
        //dd($arrAgency);
        return $arrAgency;
    }
    
    public function getAgencyByIDforweb($agnecy_id,$lang){
        $objAgency = $this->where('id',$agnecy_id)->get();
        //$objAgencyTrans = $objAgency->translate($lang,'en');
        $objOffer = new Offer();
        $objReview = new Review();
        $objAgency[0]->arrReview = $objReview->listReviewAttachedtoAgency($objAgency[0]->id);
        $objAgency[0]->offers = count($objOffer->getOffer($objAgency[0]->id));
        $objAgency[0]->TotalRate = $objReview->getReviewRateAttachedToAgency($objAgency[0]->id);
        //$objAgency2= translationHelper::translatedCollectionToArray($objAgencyTrans);
       //dd($objAgency);
        return $objAgency;
    }
}
