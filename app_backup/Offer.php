<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Support\Facades\Auth;
use App\Program;
use App\FacilityOffer;
use App\Facility;
use App\Package;
use App\Currency;
use Illuminate\Support\Facades\DB;

class Offer extends Model
{
    use SoftDeletes,Translatable;
    protected $table = 'offers';
    protected $fillable = ['id','name','desc','from','to','price','img','discount','currency_id','agency_id','verified','multi_img','updated_at','created_at','destination_id','section_id'];

    protected $translatable = ['name','desc'];

    protected $casts = [
        'multi_img' => 'array'
    ];

    public function filter($arrPrice,$dest_id){
      $mydate = date('Y-m-d');
      $orig_timezone = new \DateTimeZone('UTC');
      $schedule_date = new \DateTime($mydate,$orig_timezone);
      $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
      $currentDate = $schedule_date->format('Y-m-d');
      $arrOffer = Offer::whereBetween('price',$arrPrice)->where('destination_id',$dest_id)->where('to','>=',$currentDate)->get();
      foreach($arrOffer as $objOffer){
          $objAgency = new Agency();
          $objReview = new Review();
          $objProgram = new Program();
          $objCurrency = new Currency();
          $objDestination = new Destination();

          $objOffer->agency = $objAgency->getAgencyById($objOffer->agency_id);
          $objOffer->totalReview = $objReview->getReviewRateAttachedToOffer($objOffer->id);
          $objOffer->TotalRate = $objReview->getReviewRateAttachedToAgency($objOffer['id']);
          $objOffer->Review = $objReview->listReviewAttachedtoOffer($objOffer->id);
        
          $objOffer->Currency = $objCurrency->getCurrencyById($objOffer->currency_id);
          $objOffer->Destination = $objDestination->getDestinationById($objOffer->destination_id);
        $date1=date_create($objOffer->from);
          $date2=date_create($objOffer->to);
          $diff=date_diff($date1,$date2);
          $objOffer->Differ =  $objOffer->duration;
      }
      //dd($arrOffer);
      return $arrOffer;
    }

    public function getOfferAttachedtoHotel($hotel_id){
        //dd($hotel_id);
        $mydate = date('Y-m-d');
        $orig_timezone = new \DateTimeZone('UTC');
        $schedule_date = new \DateTime($mydate,$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
        $currentDate = $schedule_date->format('Y-m-d');
        $arrOfferIDs = Program::where('hotel_id',$hotel_id)->where('to','>=',$currentDate)->groupBy('offer_id')->pluck('offer_id');

        $arrOffer = Offer::whereIn('id',$arrOfferIDs)->paginate(9);
        foreach($arrOffer as $objOffer){
            $objReview = new Review();
            $objCurrency = new Currency();
            $objAgency = new Agency();
            $objOffer->agency = $objAgency->getAgencyById($objOffer->agency_id);
      	    $objOffer->Currency = $objCurrency->getCurrencyById($objOffer->currency_id);
            $objOffer->arrReview = count($objReview->listReviewAttachedtoAgency($objOffer['id']));
            $objOffer->TotalRate = $objReview->getReviewRateAttachedToAgency($objOffer['id']);
        }
        //dd($arrOffer);
        return $arrOffer;
    }
    public static function listOfferSponsor(){
        $mydate = date('Y-m-d');
        $orig_timezone = new \DateTimeZone('UTC');
        $schedule_date = new \DateTime($mydate,$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
        $currentDate = $schedule_date->format('Y-m-d');
        $arrOffer= Offer::where('verified','!=','0')->where('to','>=',$currentDate)->orWhere('is_sponser',1)->orderBy('created_at','DESC')->limit(6)->get();

        foreach($arrOffer as $objOffer){
            $objAgency = new Agency();
            $objReview = new Review();
            $objProgram = new Program();
            $objCurrency = new Currency();
            $objDestination = new Destination();
            $objOffer->multi_img = json_decode($objOffer->multi_img);
            $objOffer->agency = $objAgency->getAgencyById($objOffer->agency_id);
            $objOffer->totalReview = $objReview->getReviewRateAttachedToOffer($objOffer->id);
            $objOffer->Review = $objReview->listReviewAttachedtoOffer($objOffer->id);
            $objOffer->TotalRate = $objReview->getReviewRateAttachedToAgency($objOffer['id']);
            $objOffer->Program = $objProgram->getProgramAttachedtoOffer($objOffer->id);
            $objOffer->Currency = $objCurrency->getCurrencyById($objOffer->currency_id);
            $objOffer->Destination = $objDestination->getDestinationById($objOffer->destination_id);
            $date1=date_create($objOffer->from);
            $date2=date_create($objOffer->to);
            $diff=date_diff($date1,$date2);
            $objOffer->Differ =  $diff->days;
            $objOffer->Differ =  $objOffer->duration;
        }

        //$arrOfferTrans = $arrOffer->translate($input['lang'],'en');


        //$arrOffer2 =  translationHelper::translatedCollectionToArray($arrOfferTrans);
        return $arrOffer;
    }


    public function listOffer($input){
        $mydate = date('Y-m-d');
        $orig_timezone = new \DateTimeZone('UTC');
        $schedule_date = new \DateTime($mydate,$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
        $currentDate = $schedule_date->format('Y-m-d');
        $offer = $this->newQuery();

        if((array_key_exists('name',$input)) && ($input['name']) !== null){
            $offer->Where('name', 'like', '%' . $input['name'] . '%');
        }
        if((array_key_exists('keyword_id',$input)) && ($input['keyword_id']) !== null){
            $objKeyword = new Keyword();
            $arrDestinationID = DB::table('destination_keywords')->where('keyword_id',$input['keyword_id'])->pluck('destination_id');
            $offer->WhereIn('destination_id',  $arrDestinationID);
        }
        if((array_key_exists('destination_id',$input)) && ($input['destination_id']) !== null){

            $offer->Where('destination_id',  $input['destination_id']);
        }
        if((array_key_exists('section_id',$input)) && ($input['section_id']) !== null){
            $offer->Where('section_id',$input['section_id']);
        }
        $arrOffer= $offer->where('to','>=',$currentDate)->where('verified','!=','0')->paginate(10);

        foreach($arrOffer as $objOffer){
            $objAgency = new Agency();
            $objReview = new Review();
            $objProgram = new Program();
            $objCurrency = new Currency();
            $objDestination = new Destination();
            $objOffer->multi_img = json_decode($objOffer->multi_img);
            $objOffer->agency = $objAgency->getAgencyById($objOffer->agency_id);
            $objOffer->totalReview = $objReview->getReviewRateAttachedToOffer($objOffer->id);
            $objOffer->TotalRate = $objReview->getReviewRateAttachedToAgency($objOffer['id']);
            $objOffer->Review = $objReview->listReviewAttachedtoOffer($objOffer->id);
            $objOffer->Program = $objProgram->getProgramAttachedtoOffer($objOffer->id);
            $objOffer->Currency = $objCurrency->getCurrencyById($objOffer->currency_id);
            $objOffer->Destination = $objDestination->getDestinationById($objOffer->destination_id);
 	        $date1=date_create($objOffer->from);
            $date2=date_create($objOffer->to);
            $diff=date_diff($date1,$date2);
            $objOffer->Differ =  $objOffer->duration;
        }

        //$arrOfferTrans = $arrOffer->translate($input['lang'],'en');


        //$arrOffer2 =  translationHelper::translatedCollectionToArray($arrOfferTrans);
        return $arrOffer;
    }
    /* cpanel*/

    public function getOffer($agency_id){
        $mydate = date('Y-m-d');
        $orig_timezone = new \DateTimeZone('UTC');
        $schedule_date = new \DateTime($mydate,$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
        $currentDate = $schedule_date->format('Y-m-d');
        //$arrOffer = $this->where('to','>=',$currentDate)->where('agency_id',$agency_id)->paginate(9);
        $arrOffer = $this->where('to','>=',$currentDate)->where('agency_id',$agency_id)->get();
        foreach($arrOffer as $objOffer){
            $objReview = new Review();
            $objCurrency = new Currency();
            $objOffer->Currency = $objCurrency->getCurrencyById($objOffer->currency_id);
            $objOffer->arrReview = count($objReview->listReviewAttachedtoAgency($objOffer['id']));
            $objOffer->TotalRate = $objReview->getReviewRateAttachedToAgency($objOffer['id']);
        }
        return $arrOffer;
    }


    public function getOfferById($offer_id){
        //dd($offer_id);
        $objOffer = $this->where('id',$offer_id)->first();

        $objAgency = new Agency();
        $objReview = new Review();
        $objProgram= new Program();
        $objFacilityOffer = new FacilityOffer();
        $objFacility = new Facility();
        $objPackage = new Package();
        $objCurrency = new Currency();
        $objDestination = new Destination();
        $objInclusion = new Inclusion();
        $objOffer->agency = $objAgency->getAgencyById($objOffer->agency_id);
        $objOffer->totalReview = $objReview->getReviewRateAttachedToOffer($objOffer->id);
        $objOffer->Review = $objReview->listReviewAttachedtoOffer($objOffer->id);
        $objOffer->Program = $objProgram->getProgramAttachedtoOffer($objOffer->id);
        $objOffer->packages = $objPackage->getAllOffersAttachedToPackages($objOffer->id);
        $objOffer->exclude = $objInclusion->getexclude($objOffer->id);
//dd($objOffer->exclude);
        $objOffer->include = $objInclusion->getInclude($objOffer->id);
        //dd($objOffer->packages);
        $arrFacilityIds = $objFacilityOffer->getFacilitiesIds($objOffer->id);
        $objOffer->facilities = $objFacility->getFacilityAttachedtoOffer($arrFacilityIds);
        $objOffer->Currency = $objCurrency->getCurrencyById($objOffer->currency_id);
        $objOffer->Destination = $objDestination->getDestinationById($objOffer->destination_id);
        $objOffer->multi_img = json_decode($objOffer->multi_img);
        $objLike = new Like();


        //dd($objOffer->multi_img);
	    $date1=date_create($objOffer->from);
        $date2=date_create($objOffer->to);
        $diff=date_diff($date1,$date2);
        $objOffer->Differ =  $objOffer->duration;
        return $objOffer;
    }

    public static function getOfferAttachedToUser($user_id){
        $mydate = date('Y-m-d');
        $orig_timezone = new \DateTimeZone('UTC');
        $schedule_date = new \DateTime($mydate,$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
        $currentDate = $schedule_date->format('Y-m-d');
        //dd($user_id);
        $Agency=array();
        $objAgency = new Agency();
        $Agency = $objAgency->getAgency($user_id);
        //dd($Agency);
        if(!empty($Agency)&& $Agency != null){
            //dd('hii');
            $arrOffer = Offer::where('agency_id',$Agency[0]['id'])->where('to','>=',$currentDate)->get();
            return $arrOffer;
        }
        $arrOffer = Offer::get();
        return $arrOffer;
    }



}
