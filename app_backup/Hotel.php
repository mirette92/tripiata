<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Spatial;
use Illuminate\Database\Eloquent\SoftDeletes;

use TCG\Voyager\Traits\Translatable;

use TCG\Voyager\Translator;
use App\Helpers\translationHelper;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Program;


class Hotel extends Model
{
    //
	 use Spatial;
	 use SoftDeletes ,Translatable;
    
    protected $table = 'hotels';
    protected $spatial = ['coordinates'];
 
    protected $fillable = ['id','name'];
    protected $translatable = ['name'];
     
    public function getHotelById($hotel_id){
        $objHotel = $this->where('id',$hotel_id)->first();
       
            
        $objHotel->img = MediaUrl::getUrl().json_decode($objHotel->img)[0];
         
        return $objHotel;
    }
    public static function getHotel(){
			$arrHotel = Hotel::get();
		 	return $arrHotel;
		}
    public function listHotel($lang,$search,$city_id){
        
        $arrHotel = $this->where('city_id',$city_id)->where('name','like', '%'.$search.'%')->paginate(9);
        foreach($arrHotel as $Hotel){
            
            $Hotel->img = MediaUrl::getUrl().json_decode($Hotel->img)[0];
            $objProgram = new Program();
            $arrOfferIDs = $objProgram->getOfferIDs($Hotel->id);
            $Hotel->OfferCount = count(Program::where('hotel_id',$Hotel->id)->get());
            //dd($arrOfferIDs);
            if(count($arrOfferIDs) != 0){
                $objOffer = new Offer();
                $Offer = $objOffer->getOfferById($arrOfferIDs[0]);
                
                $objReview = new Review();
                
                $Hotel->arrReview = count($objReview->listReviewAttachedtoAgency($Offer['agency_id']));
                $Hotel->TotalRate = $objReview->getReviewRateAttachedToAgency($Offer['agency_id']);
               
            }else{
                $arrHotel->arrReview = [];
                $Hotel->TotalRate = 0;
                
            }
            
        }
        
        return $arrHotel;
         //dd($arrHotel);
       // $data = $arrHotel->items();
        //$arrHotelTrans =  $arrHotel->translate('ar','en');
          //$v = $arrHotelTrans->paginate(2);
       // $arrHotel2= translationHelper::translatedCollectionToArray($arrHotelTrans);
        //$arrHotel2 = translationHelper::paginationForTranslator($arrHotel,$arrHotel2);  */
        //dd($arrHotelTrans);
        
    }
    public function getHotelByIdforweb($lang,$hotel_id){
        $lang='en';
        
        $objHotel = $this->where('id',$hotel_id)->get();
        //$objHotelTrans = $objHotel->translate($lang,'en');
        $objHotel[0]->img = MediaUrl::getUrl().json_decode($objHotel[0]->img)[0];
        
       
        $objHotel[0]->OfferCount = count(Program::where('hotel_id',$objHotel[0]->id)->get());
        
        if($objHotel[0]->OfferCount > 0){
             $objReview = new Review();
            $objProgram = new Program();
            $objFacility = new Facility();
            $objFacilityHotel = new HotelsFacility();
            $arrOfferIDs = $objProgram->getOfferIDs($objHotel[0]->id);
       	     $arrFacilityIds = $objFacilityHotel->getFacilitiesIds($objHotel[0]->id);
            $objHotel[0]->facilities = $objFacility->getFacilityAttachedtoHotel($arrFacilityIds);
            $objOffer = new Offer();
            $Offer = $objOffer->getOfferAttachedtoHotel($objHotel[0]->id); 
            $objHotel[0]->Offer = $Offer;
            $objHotel[0]->arrReview = $objReview->listReviewAttachedtoAgency($Offer['agency_id']);
            $objHotel[0]->TotalRate = $objReview->getReviewRateAttachedToAgency($Offer['agency_id']);
        }else{
            $objHotel[0]->arrReview = [];
            $objHotel[0]->TotalRate = 0;
            $objHotel[0]->Offer = [];
            $objHotel[0]->facilities = [];
            
        }
     
        //$objHotel2= translationHelper::translatedCollectionToArray($objHotelTrans);
        //dd($objHotel2);
        return $objHotel;
    }
    
    
}
