<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Support\Facades\DB;
class City extends Model
{
    use SoftDeletes,Translatable;
    protected $fillable = ['id','name','country_id','updated_at','created_at'];
    protected $translatable = ['name'];
    
    
    public function listCity($lang){
        
        $arrCity = $this->get();
        $arrCityTrans = $arrCity->translate($lang,'en');
        $arrCity2 =   translationHelper::translatedCollectionToArray($arrCityTrans);
        return $arrCity2;
    }
    
    public function listCityAttachedToCountry($lang,$country_id){
        $arrCity = $this->where('country_id',$country_id)->get();
        $arrCityTrans = $arrCity->translate($lang,'en');
        $arrCity2 = translationHelper::translatedCollectionToArray($arrCityTrans);
        return $arrCity2;
    }
    
    public function listCityAttachedtoHotel(){
        $results = DB::table('cities')
        ->select('cities.id', 'cities.name','cities.img', DB::raw("COUNT(*) as count_hotel"))
        ->join('hotels', 'cities.id', '=', 'hotels.city_id')
        ->groupBy('city_id')
        ->orderBy('count_hotel','DESC')
        ->get();
        //dd($results);
        return $results;
    }
    /* public static function listCityAttachedtoOffer(){
        $arrCityIds = DB::table('cities')
        ->select( 'cities.id',DB::raw("COUNT(*) as count_hotel"))
        ->join('hotels', 'cities.id', '=', 'hotels.city_id')
        ->groupBy('city_id')
        ->orderBy('count_hotel','DESC')
        ->pluck('cities.id');
        //dd($results);
        return $arrCityIds;
    } */
}
