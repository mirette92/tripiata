<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Support\Facades\DB;

class Destination extends Model
{
    use SoftDeletes,Translatable;
    protected $fillable = ['id','name','city_id','updated_at','created_at'];
    protected $translatable = ['name'];
    
    public function listDestinationAttachedtoOffer(){
        $mydate = date('Y-m-d');
        $orig_timezone = new \DateTimeZone('UTC');
        $schedule_date = new \DateTime($mydate,$orig_timezone);
        $schedule_date->setTimeZone(new \DateTimeZone('Africa/Cairo'));
        $currentDate = $schedule_date->format('Y-m-d');
        $results = DB::table('destinations')
        ->select('destinations.id', 'destinations.name','destinations.img', DB::raw("COUNT(*) as count_offer"))
        ->join('offers', 'destinations.id', '=', 'offers.destination_id')
        ->where('from','>=',$currentDate)->where('verified','!=','0')
        ->groupBy('destination_id')
        ->orderBy('count_offer','DESC')
        ->get();
        //dd($results);
        return $results;
    }
    
    
    public function listDestinationAttachedtoHotel($arrIds){
     $arrDestination = $this->whereIn('city_id',$arrIds)->get();
     return $arrDestination;
    }
    public function getDestinationById($intDestinationID){
        $objDestination = $this->where('id',$intDestinationID)->first();
       /*  $arrDestinationTrans = $arrDestination->translate($lang,'en');
        $arrDestination2 =  translationHelper::translatedCollectionToArray($arrDestinationTrans); */
        return $objDestination;
    }
    public static function listDestination(){
        
        $arrDestination = Destination::orderBy('name','ASC')->get();
        //$arrDestinationTrans = $arrDestination->translate($lang,'en');
        //$arrDestination2 = translationHelper::translatedCollectionToArray($arrDestinationTrans);
        return $arrDestination;
    }
    public function listDestinationAttachedtoCity($lang,$city_id){
        $arrDestination = $this->where('city_id',$city_id)->get();
        $arrDestinationTrans = $arrDestination->translate($lang,'en');
        $arrDestination2 =  translationHelper::translatedCollectionToArray($arrDestinationTrans);
        return $arrDestination2;
    }
    
    public function listLimit4($lang){
        $arrDestination = $this->limit(4)->get();
        $arrDestinationTrans = $arrDestination->translate($lang,'en');
    
        $arrDestination2 =  translationHelper::translatedCollectionToArray($arrDestinationTrans);
        return $arrDestination2;
    }
}
