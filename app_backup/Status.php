<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\translationHelper;
use TCG\Voyager\Traits\Translatable;

class Status extends Model
{
    use SoftDeletes,Translatable;
    protected $translatable = ['name'];
    
    public function getStatusById($status_id){
        $objStatus = $this->where('id',$status_id)->first();
        return $objStatus;
    }
}
