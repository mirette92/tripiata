<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacilityOffer extends Model
{
    use SoftDeletes;
    
    public function getFacilitiesIds($offer_id){
        $arrFacilityIds = $this->where('offer_id',$offer_id)->pluck('facility_id');
        return $arrFacilityIds;
    }
}
