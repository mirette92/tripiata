<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Route::get('/offers-list', function () {
    return view('offers-list');
}); */
Route::post('filterOffer','OfferController@FilterOffer');
Route::get('hotels-destinations','CityController@ListCityAttachedtoOffer');

    Route::get('clear-cache', function() {
        $exitCode = Artisan::call('cache:clear');
        $exitCode = Artisan::call('route:clear');
        $exitCode = Artisan::call('config:clear');
        $exitCode = Artisan::call('view:clear');
        print('Cache Cleared');
        // return what you want
    });
Route::get('offer-destinations','DestinationController@ListDestinationAttachedtoOffer');

Route::get('offers-list','OfferController@ListOffer');

Route::get('offer-details','OfferController@GetOfferById');

Route::get('test','DestinationController@ListDestinationAttachedtoHotel');

Route::get('index','PackageController@List');

Route::post('addPackage','PackageController@AddPackage');
Route::post('addProgram','ProgramController@AddProgram');

Route::get('/about', function () {
    return view('about');
});

Route::get('/contactUs', function () {
    return view('contact');
});

Route::get('/confirmation', function () {
    return view('confirmation');
});

/* Route::get('offer-list','HotelController@ListHotel'); */

Route::post('register','UserController@RegisterWeb');
Route::post('login','UserController@LoginWeb');

Route::get('hotel-list','HotelController@ListHotel');
Route::get('hotel-detail','HotelController@GetHotelById');

Route::get('agency-list','AgencyController@ListAgency');
Route::get('agency-detail','AgencyController@GetAgencyById');

Route::post('addReview','ReviewController@AddReview');
Route::get('addApp','ApplicationController@AddApplication');

Route::post('addComment','CommentController@AddComment');

Route::get('logout','UserController@logout');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LangController@switchLang']);
