<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('filter','OfferController@Filter');
Route::post('hotel/list','HotelController@ListHotel');

Route::post('agency/list','AgencyController@ListAgency');

Route::post('like','LikeController@AddLike');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
