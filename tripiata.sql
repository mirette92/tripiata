/*
Navicat MySQL Data Transfer

Source Server         : db
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : tripiata

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-09-18 22:30:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accommodations
-- ----------------------------
DROP TABLE IF EXISTS `accommodations`;
CREATE TABLE `accommodations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of accommodations
-- ----------------------------

-- ----------------------------
-- Table structure for agencies
-- ----------------------------
DROP TABLE IF EXISTS `agencies`;
CREATE TABLE `agencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `facebook` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` int(11) DEFAULT NULL,
  `biography` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of agencies
-- ----------------------------
INSERT INTO `agencies` VALUES ('1', 'test', null, null, null, null, null, null, null, '2');
INSERT INTO `agencies` VALUES ('2', 'Agency trip', null, null, null, null, null, null, null, '3');

-- ----------------------------
-- Table structure for applications
-- ----------------------------
DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `offer_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of applications
-- ----------------------------
INSERT INTO `applications` VALUES ('1', '1', '1', '1', null, null, null, '2');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', null, '1', 'Category 1', 'category-1', '2018-09-01 10:39:18', '2018-09-01 10:39:18');
INSERT INTO `categories` VALUES ('2', null, '1', 'Category 2', 'category-2', '2018-09-01 10:39:18', '2018-09-01 10:39:18');

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of cities
-- ----------------------------
INSERT INTO `cities` VALUES ('1', 'hurghada', '2018-09-13 06:30:35', '2018-09-15 02:22:28', null, '2');
INSERT INTO `cities` VALUES ('3', 'jukjk', '2018-09-15 02:14:00', '2018-09-15 02:21:46', null, '1');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `review_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of comments
-- ----------------------------

-- ----------------------------
-- Table structure for countries
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'egypt', '2018-09-13 06:30:22', '2018-09-13 06:30:22', null);
INSERT INTO `countries` VALUES ('2', 'test', '2018-09-15 02:13:59', '2018-09-15 02:13:59', null);

-- ----------------------------
-- Table structure for currencies
-- ----------------------------
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `symbol` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of currencies
-- ----------------------------
INSERT INTO `currencies` VALUES ('1', 'Pound', 'EGP', null, null, null);

-- ----------------------------
-- Table structure for data_rows
-- ----------------------------
DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of data_rows
-- ----------------------------
INSERT INTO `data_rows` VALUES ('1', '1', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `data_rows` VALUES ('2', '1', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', '', '2');
INSERT INTO `data_rows` VALUES ('3', '1', 'email', 'text', 'Email', '1', '1', '1', '1', '1', '1', '', '3');
INSERT INTO `data_rows` VALUES ('4', '1', 'password', 'password', 'Password', '1', '0', '0', '1', '1', '0', '', '4');
INSERT INTO `data_rows` VALUES ('5', '1', 'remember_token', 'text', 'Remember Token', '0', '0', '0', '0', '0', '0', '', '5');
INSERT INTO `data_rows` VALUES ('6', '1', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '0', '0', '0', '', '6');
INSERT INTO `data_rows` VALUES ('7', '1', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '', '7');
INSERT INTO `data_rows` VALUES ('8', '1', 'avatar', 'image', 'Avatar', '0', '1', '1', '1', '1', '1', '', '8');
INSERT INTO `data_rows` VALUES ('9', '1', 'user_belongsto_role_relationship', 'relationship', 'Role', '0', '1', '1', '1', '1', '0', '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', '10');
INSERT INTO `data_rows` VALUES ('10', '1', 'user_belongstomany_role_relationship', 'relationship', 'Roles', '0', '1', '1', '1', '1', '0', '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', '11');
INSERT INTO `data_rows` VALUES ('11', '1', 'locale', 'text', 'Locale', '0', '1', '1', '1', '1', '0', '', '12');
INSERT INTO `data_rows` VALUES ('12', '1', 'settings', 'hidden', 'Settings', '0', '0', '0', '0', '0', '0', '', '12');
INSERT INTO `data_rows` VALUES ('13', '2', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `data_rows` VALUES ('14', '2', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', '', '2');
INSERT INTO `data_rows` VALUES ('15', '2', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', '', '3');
INSERT INTO `data_rows` VALUES ('16', '2', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '', '4');
INSERT INTO `data_rows` VALUES ('17', '3', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `data_rows` VALUES ('18', '3', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', '', '2');
INSERT INTO `data_rows` VALUES ('19', '3', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', '', '3');
INSERT INTO `data_rows` VALUES ('20', '3', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '', '4');
INSERT INTO `data_rows` VALUES ('21', '3', 'display_name', 'text', 'Display Name', '1', '1', '1', '1', '1', '1', '', '5');
INSERT INTO `data_rows` VALUES ('22', '1', 'role_id', 'text', 'Role', '1', '1', '1', '1', '1', '1', '', '9');
INSERT INTO `data_rows` VALUES ('23', '4', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `data_rows` VALUES ('24', '4', 'parent_id', 'select_dropdown', 'Parent', '0', '0', '1', '1', '1', '1', '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', '2');
INSERT INTO `data_rows` VALUES ('25', '4', 'order', 'text', 'Order', '1', '1', '1', '1', '1', '1', '{\"default\":1}', '3');
INSERT INTO `data_rows` VALUES ('26', '4', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', '', '4');
INSERT INTO `data_rows` VALUES ('27', '4', 'slug', 'text', 'Slug', '1', '1', '1', '1', '1', '1', '{\"slugify\":{\"origin\":\"name\"}}', '5');
INSERT INTO `data_rows` VALUES ('28', '4', 'created_at', 'timestamp', 'Created At', '0', '0', '1', '0', '0', '0', '', '6');
INSERT INTO `data_rows` VALUES ('29', '4', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '', '7');
INSERT INTO `data_rows` VALUES ('30', '5', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `data_rows` VALUES ('31', '5', 'author_id', 'text', 'Author', '1', '0', '1', '1', '0', '1', '', '2');
INSERT INTO `data_rows` VALUES ('32', '5', 'category_id', 'text', 'Category', '1', '0', '1', '1', '1', '0', '', '3');
INSERT INTO `data_rows` VALUES ('33', '5', 'title', 'text', 'Title', '1', '1', '1', '1', '1', '1', '', '4');
INSERT INTO `data_rows` VALUES ('34', '5', 'excerpt', 'text_area', 'Excerpt', '1', '0', '1', '1', '1', '1', '', '5');
INSERT INTO `data_rows` VALUES ('35', '5', 'body', 'rich_text_box', 'Body', '1', '0', '1', '1', '1', '1', '', '6');
INSERT INTO `data_rows` VALUES ('36', '5', 'image', 'image', 'Post Image', '0', '1', '1', '1', '1', '1', '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', '7');
INSERT INTO `data_rows` VALUES ('37', '5', 'slug', 'text', 'Slug', '1', '0', '1', '1', '1', '1', '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', '8');
INSERT INTO `data_rows` VALUES ('38', '5', 'meta_description', 'text_area', 'Meta Description', '1', '0', '1', '1', '1', '1', '', '9');
INSERT INTO `data_rows` VALUES ('39', '5', 'meta_keywords', 'text_area', 'Meta Keywords', '1', '0', '1', '1', '1', '1', '', '10');
INSERT INTO `data_rows` VALUES ('40', '5', 'status', 'select_dropdown', 'Status', '1', '1', '1', '1', '1', '1', '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', '11');
INSERT INTO `data_rows` VALUES ('41', '5', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '0', '0', '0', '', '12');
INSERT INTO `data_rows` VALUES ('42', '5', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '', '13');
INSERT INTO `data_rows` VALUES ('43', '5', 'seo_title', 'text', 'SEO Title', '0', '1', '1', '1', '1', '1', '', '14');
INSERT INTO `data_rows` VALUES ('44', '5', 'featured', 'checkbox', 'Featured', '1', '1', '1', '1', '1', '1', '', '15');
INSERT INTO `data_rows` VALUES ('45', '6', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', '', '1');
INSERT INTO `data_rows` VALUES ('46', '6', 'author_id', 'text', 'Author', '1', '0', '0', '0', '0', '0', '', '2');
INSERT INTO `data_rows` VALUES ('47', '6', 'title', 'text', 'Title', '1', '1', '1', '1', '1', '1', '', '3');
INSERT INTO `data_rows` VALUES ('48', '6', 'excerpt', 'text_area', 'Excerpt', '1', '0', '1', '1', '1', '1', '', '4');
INSERT INTO `data_rows` VALUES ('49', '6', 'body', 'rich_text_box', 'Body', '1', '0', '1', '1', '1', '1', '', '5');
INSERT INTO `data_rows` VALUES ('50', '6', 'slug', 'text', 'Slug', '1', '0', '1', '1', '1', '1', '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', '6');
INSERT INTO `data_rows` VALUES ('51', '6', 'meta_description', 'text', 'Meta Description', '1', '0', '1', '1', '1', '1', '', '7');
INSERT INTO `data_rows` VALUES ('52', '6', 'meta_keywords', 'text', 'Meta Keywords', '1', '0', '1', '1', '1', '1', '', '8');
INSERT INTO `data_rows` VALUES ('53', '6', 'status', 'select_dropdown', 'Status', '1', '1', '1', '1', '1', '1', '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', '9');
INSERT INTO `data_rows` VALUES ('54', '6', 'created_at', 'timestamp', 'Created At', '1', '1', '1', '0', '0', '0', '', '10');
INSERT INTO `data_rows` VALUES ('55', '6', 'updated_at', 'timestamp', 'Updated At', '1', '0', '0', '0', '0', '0', '', '11');
INSERT INTO `data_rows` VALUES ('56', '6', 'image', 'image', 'Page Image', '0', '1', '1', '1', '1', '1', '', '12');
INSERT INTO `data_rows` VALUES ('57', '7', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('58', '7', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('59', '7', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('60', '7', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('61', '7', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('62', '8', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('63', '8', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('64', '8', 'phone', 'number', 'Phone', '0', '1', '1', '1', '1', '1', null, '3');
INSERT INTO `data_rows` VALUES ('65', '8', 'facebook', 'text', 'Facebook', '0', '1', '1', '1', '1', '1', null, '4');
INSERT INTO `data_rows` VALUES ('66', '8', 'verified', 'checkbox', 'Verified', '0', '1', '1', '1', '1', '1', null, '5');
INSERT INTO `data_rows` VALUES ('67', '8', 'biography', 'markdown_editor', 'Biography', '0', '1', '1', '1', '1', '1', null, '6');
INSERT INTO `data_rows` VALUES ('68', '8', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '7');
INSERT INTO `data_rows` VALUES ('69', '8', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '8');
INSERT INTO `data_rows` VALUES ('70', '8', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '9');
INSERT INTO `data_rows` VALUES ('71', '9', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('72', '9', 'offer_id', 'text', 'Offer Id', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('73', '9', 'package_id', 'text', 'Package Id', '0', '1', '1', '1', '1', '1', null, '3');
INSERT INTO `data_rows` VALUES ('74', '9', 'user_id', 'text', 'User Id', '0', '1', '1', '1', '1', '1', null, '4');
INSERT INTO `data_rows` VALUES ('75', '9', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '1', '0', '1', null, '5');
INSERT INTO `data_rows` VALUES ('76', '9', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '6');
INSERT INTO `data_rows` VALUES ('77', '9', 'deleted_at', 'timestamp', 'Deleted At', '0', '1', '1', '1', '1', '1', null, '7');
INSERT INTO `data_rows` VALUES ('78', '10', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('79', '10', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('80', '10', 'symbol', 'text', 'Symbol', '0', '1', '1', '1', '1', '1', null, '3');
INSERT INTO `data_rows` VALUES ('81', '10', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('82', '10', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('83', '10', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '6');
INSERT INTO `data_rows` VALUES ('84', '11', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('85', '11', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('86', '11', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '1', null, '3');
INSERT INTO `data_rows` VALUES ('87', '11', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('88', '11', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('89', '12', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('90', '12', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('91', '12', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('92', '12', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('93', '12', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('119', '15', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('120', '15', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('121', '15', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('122', '15', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('123', '15', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('130', '16', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('131', '16', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('132', '16', 'desc', 'markdown_editor', 'Desc', '0', '1', '1', '1', '1', '1', null, '3');
INSERT INTO `data_rows` VALUES ('133', '16', 'hotel_id', 'text', 'Hotel Id', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('134', '16', 'price', 'number', 'Price', '0', '1', '1', '1', '1', '1', null, '6');
INSERT INTO `data_rows` VALUES ('135', '16', 'offer_id', 'text', 'Offer Id', '0', '0', '0', '0', '0', '0', null, '7');
INSERT INTO `data_rows` VALUES ('136', '16', 'from', 'date', 'From', '0', '1', '1', '1', '1', '1', null, '9');
INSERT INTO `data_rows` VALUES ('137', '16', 'to', 'date', 'To', '0', '1', '1', '1', '1', '1', null, '10');
INSERT INTO `data_rows` VALUES ('138', '16', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '11');
INSERT INTO `data_rows` VALUES ('139', '16', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '12');
INSERT INTO `data_rows` VALUES ('140', '16', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '13');
INSERT INTO `data_rows` VALUES ('141', '16', 'program_belongsto_hotel_relationship', 'relationship', 'hotel', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Hotel\",\"table\":\"hotels\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '5');
INSERT INTO `data_rows` VALUES ('142', '16', 'program_belongsto_offer_relationship', 'relationship', 'Offer', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Offer\",\"table\":\"offers\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '8');
INSERT INTO `data_rows` VALUES ('146', '17', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('147', '17', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('148', '17', 'phone', 'text', 'Phone', '0', '1', '1', '1', '1', '1', null, '3');
INSERT INTO `data_rows` VALUES ('149', '17', 'star', 'text', 'Star', '0', '1', '1', '1', '1', '1', null, '4');
INSERT INTO `data_rows` VALUES ('150', '17', 'address', 'text', 'Address', '0', '1', '1', '1', '1', '1', null, '5');
INSERT INTO `data_rows` VALUES ('151', '17', 'coordinates', 'coordinates', 'Coordinates', '0', '1', '1', '1', '1', '1', null, '6');
INSERT INTO `data_rows` VALUES ('152', '17', 'img', 'multiple_images', 'Images', '0', '1', '1', '1', '1', '1', null, '7');
INSERT INTO `data_rows` VALUES ('153', '17', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '8');
INSERT INTO `data_rows` VALUES ('154', '17', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '9');
INSERT INTO `data_rows` VALUES ('155', '17', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '10');
INSERT INTO `data_rows` VALUES ('156', '17', 'facebook', 'text', 'Facebook', '0', '1', '1', '1', '1', '1', null, '11');
INSERT INTO `data_rows` VALUES ('158', '11', 'city_id', 'text', 'City Id', '0', '0', '0', '0', '0', '0', null, '6');
INSERT INTO `data_rows` VALUES ('166', '19', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('167', '19', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('168', '19', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('169', '19', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('170', '19', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('171', '11', 'destination_belongsto_city_relationship', 'relationship', 'cities', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\City\",\"table\":\"cities\",\"type\":\"belongsTo\",\"column\":\"city_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '7');
INSERT INTO `data_rows` VALUES ('172', '20', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('173', '20', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('175', '20', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('176', '20', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '6');
INSERT INTO `data_rows` VALUES ('177', '20', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '7');
INSERT INTO `data_rows` VALUES ('179', '20', 'keyword_belongstomany_destination_relationship', 'relationship', 'destinations', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Destination\",\"table\":\"destinations\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"destination_keywords\",\"pivot\":\"1\",\"taggable\":\"0\"}', '8');
INSERT INTO `data_rows` VALUES ('180', '21', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('181', '21', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('182', '21', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('183', '21', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('184', '21', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('185', '22', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('186', '22', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('187', '22', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('188', '22', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('189', '22', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('191', '9', 'agency_id', 'text', 'Agency Id', '0', '1', '1', '1', '1', '1', null, '8');
INSERT INTO `data_rows` VALUES ('193', '23', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('194', '23', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('195', '23', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('196', '23', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('197', '23', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('198', '23', 'country_id', 'select_dropdown', 'Country Id', '0', '1', '1', '1', '1', '1', null, '7');
INSERT INTO `data_rows` VALUES ('199', '23', 'city_belongsto_country_relationship', 'relationship', 'countries', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Country\",\"table\":\"countries\",\"type\":\"belongsTo\",\"column\":\"country_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '6');
INSERT INTO `data_rows` VALUES ('202', '24', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('203', '24', 'name', 'text', 'Name', '0', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('204', '24', 'desc', 'markdown_editor', 'Desc', '0', '1', '1', '1', '1', '1', null, '3');
INSERT INTO `data_rows` VALUES ('205', '24', 'from', 'date', 'From', '0', '1', '1', '1', '1', '1', null, '4');
INSERT INTO `data_rows` VALUES ('206', '24', 'to', 'date', 'To', '0', '1', '1', '1', '1', '1', null, '5');
INSERT INTO `data_rows` VALUES ('207', '24', 'price', 'number', 'Price', '0', '1', '1', '1', '1', '1', null, '6');
INSERT INTO `data_rows` VALUES ('208', '24', 'img', 'image', 'Img', '0', '1', '1', '1', '1', '1', null, '7');
INSERT INTO `data_rows` VALUES ('209', '24', 'discount', 'number', 'Discount', '0', '1', '1', '1', '1', '1', null, '8');
INSERT INTO `data_rows` VALUES ('210', '24', 'currency_id', 'select_dropdown', 'Currency Id', '0', '1', '1', '1', '1', '1', null, '10');
INSERT INTO `data_rows` VALUES ('211', '24', 'agency_id', 'select_dropdown', 'Agency Id', '0', '1', '1', '1', '1', '1', null, '12');
INSERT INTO `data_rows` VALUES ('212', '24', 'verified', 'checkbox', 'Verified', '0', '1', '1', '1', '1', '1', null, '13');
INSERT INTO `data_rows` VALUES ('213', '24', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '14');
INSERT INTO `data_rows` VALUES ('214', '24', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '15');
INSERT INTO `data_rows` VALUES ('215', '24', 'deleted_at', 'timestamp', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '16');
INSERT INTO `data_rows` VALUES ('216', '24', 'multi_img', 'multiple_images', 'Multi Img', '0', '1', '1', '1', '1', '1', null, '17');
INSERT INTO `data_rows` VALUES ('217', '24', 'offer_belongsto_currency_relationship', 'relationship', 'currencies', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Currency\",\"table\":\"currencies\",\"type\":\"belongsTo\",\"column\":\"currency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '9');
INSERT INTO `data_rows` VALUES ('218', '24', 'offer_belongsto_agency_relationship', 'relationship', 'agencies', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Agency\",\"table\":\"agencies\",\"type\":\"belongsTo\",\"column\":\"agency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '11');
INSERT INTO `data_rows` VALUES ('220', '25', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('221', '25', 'offer_id', 'select_dropdown', 'Offer Id', '0', '1', '1', '1', '1', '1', null, '12');
INSERT INTO `data_rows` VALUES ('222', '25', 'room_id', 'select_dropdown', 'Room Id', '0', '1', '1', '1', '1', '1', null, '10');
INSERT INTO `data_rows` VALUES ('223', '25', 'accomodation_id', 'select_dropdown', 'Accomodation Id', '0', '1', '1', '1', '1', '1', null, '14');
INSERT INTO `data_rows` VALUES ('224', '25', 'curency_id', 'select_dropdown', 'Curency Id', '0', '1', '1', '1', '1', '1', null, '3');
INSERT INTO `data_rows` VALUES ('225', '25', 'price', 'text', 'Price', '0', '1', '1', '1', '1', '1', null, '4');
INSERT INTO `data_rows` VALUES ('226', '25', 'verified', 'text', 'Verified', '0', '1', '1', '1', '1', '1', null, '5');
INSERT INTO `data_rows` VALUES ('227', '25', 'created_at', 'text', 'Created At', '0', '0', '0', '0', '0', '0', null, '6');
INSERT INTO `data_rows` VALUES ('228', '25', 'updated_at', 'text', 'Updated At', '0', '0', '0', '0', '0', '0', null, '7');
INSERT INTO `data_rows` VALUES ('229', '25', 'deleted_at', 'text', 'Deleted At', '0', '0', '0', '0', '0', '0', null, '8');
INSERT INTO `data_rows` VALUES ('230', '25', 'agency_id', 'select_dropdown', 'Agency Id', '0', '1', '1', '1', '1', '1', null, '16');
INSERT INTO `data_rows` VALUES ('231', '25', 'package_belongsto_room_relationship', 'relationship', 'rooms', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Room\",\"table\":\"rooms\",\"type\":\"belongsTo\",\"column\":\"room_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '9');
INSERT INTO `data_rows` VALUES ('232', '25', 'package_belongsto_offer_relationship', 'relationship', 'offers', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Offer\",\"table\":\"offers\",\"type\":\"belongsTo\",\"column\":\"offer_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '11');
INSERT INTO `data_rows` VALUES ('233', '25', 'package_belongsto_accommodation_relationship', 'relationship', 'accommodations', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Accommodation\",\"table\":\"accommodations\",\"type\":\"belongsTo\",\"column\":\"accomodation_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '13');
INSERT INTO `data_rows` VALUES ('234', '25', 'package_belongsto_currency_relationship', 'relationship', 'currencies', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Currency\",\"table\":\"currencies\",\"type\":\"belongsTo\",\"column\":\"curency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '2');
INSERT INTO `data_rows` VALUES ('235', '25', 'package_belongsto_agency_relationship', 'relationship', 'agencies', '0', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Agency\",\"table\":\"agencies\",\"type\":\"belongsTo\",\"column\":\"agency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"accommodations\",\"pivot\":\"0\",\"taggable\":\"0\"}', '15');

-- ----------------------------
-- Table structure for data_types
-- ----------------------------
DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of data_types
-- ----------------------------
INSERT INTO `data_types` VALUES ('1', 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', '1', '0', null, '2018-09-01 10:39:05', '2018-09-01 10:39:05');
INSERT INTO `data_types` VALUES ('2', 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', null, '', '', '1', '0', null, '2018-09-01 10:39:05', '2018-09-01 10:39:05');
INSERT INTO `data_types` VALUES ('3', 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', null, '', '', '1', '0', null, '2018-09-01 10:39:05', '2018-09-01 10:39:05');
INSERT INTO `data_types` VALUES ('4', 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', null, '', '', '1', '0', null, '2018-09-01 10:39:16', '2018-09-01 10:39:16');
INSERT INTO `data_types` VALUES ('5', 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', '1', '0', null, '2018-09-01 10:39:18', '2018-09-01 10:39:18');
INSERT INTO `data_types` VALUES ('6', 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', null, '', '', '1', '0', null, '2018-09-01 10:39:20', '2018-09-01 10:39:20');
INSERT INTO `data_types` VALUES ('7', 'accommodations', 'accommodations', 'Accommodation', 'Accommodations', null, 'App\\Accommodation', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-01 14:28:10', '2018-09-01 14:28:10');
INSERT INTO `data_types` VALUES ('8', 'agencies', 'agencies', 'Agency', 'Agencies', null, 'App\\Agency', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-01 14:31:43', '2018-09-01 14:31:43');
INSERT INTO `data_types` VALUES ('9', 'applications', 'applications', 'Application', 'Applications', null, 'App\\Application', null, 'ApplicationController', null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-01 14:32:21', '2018-09-15 00:59:41');
INSERT INTO `data_types` VALUES ('10', 'currencies', 'currencies', 'Currency', 'Currencies', null, 'App\\Currency', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-01 14:38:16', '2018-09-01 14:38:16');
INSERT INTO `data_types` VALUES ('11', 'destinations', 'destinations', 'Destination', 'Destinations', null, 'App\\Destination', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-01 14:38:39', '2018-09-01 14:38:39');
INSERT INTO `data_types` VALUES ('12', 'facilities', 'facilities', 'Facility', 'Facilities', null, 'App\\Facility', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-01 14:39:07', '2018-09-01 14:39:07');
INSERT INTO `data_types` VALUES ('15', 'statuses', 'statuses', 'Status', 'Statuses', null, 'App\\Status', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-09 07:44:45', '2018-09-09 07:44:45');
INSERT INTO `data_types` VALUES ('16', 'programs', 'programs', 'Program', 'Programs', null, 'App\\Program', null, 'ProgramController', null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-09 07:55:01', '2018-09-18 20:20:33');
INSERT INTO `data_types` VALUES ('17', 'hotels', 'hotels', 'Hotel', 'Hotels', null, 'App\\Hotel', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-09 16:16:13', '2018-09-09 16:16:13');
INSERT INTO `data_types` VALUES ('19', 'countries', 'countries', 'Country', 'Countries', null, 'App\\Country', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-13 06:13:14', '2018-09-13 06:13:14');
INSERT INTO `data_types` VALUES ('20', 'keywords', 'keywords', 'Keyword', 'Keywords', null, 'App\\Keyword', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-13 06:17:46', '2018-09-13 06:17:46');
INSERT INTO `data_types` VALUES ('21', 'rooms', 'rooms', 'Room', 'Rooms', null, 'App\\Room', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-13 06:34:51', '2018-09-13 06:34:51');
INSERT INTO `data_types` VALUES ('22', 'section', 'section', 'Section', 'Sections', null, 'App\\Section', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-13 06:35:16', '2018-09-13 06:35:16');
INSERT INTO `data_types` VALUES ('23', 'cities', 'cities', 'City', 'Cities', null, 'App\\City', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-15 02:20:40', '2018-09-15 02:20:40');
INSERT INTO `data_types` VALUES ('24', 'offers', 'offers', 'Offer', 'Offers', null, 'App\\Offer', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-15 02:27:27', '2018-09-15 02:27:27');
INSERT INTO `data_types` VALUES ('25', 'packages', 'packages', 'Package', 'Packages', null, 'App\\Package', null, 'PackageController', null, '1', '0', '{\"order_column\":null,\"order_display_column\":null}', '2018-09-15 02:46:52', '2018-09-15 02:55:18');

-- ----------------------------
-- Table structure for destinations
-- ----------------------------
DROP TABLE IF EXISTS `destinations`;
CREATE TABLE `destinations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of destinations
-- ----------------------------
INSERT INTO `destinations` VALUES ('1', 'gouna', '2018-09-13 06:30:55', '2018-09-13 06:30:55', null, null);
INSERT INTO `destinations` VALUES ('2', 'shal hasheash', '2018-09-13 06:31:32', '2018-09-13 06:31:32', null, null);

-- ----------------------------
-- Table structure for destination_keywords
-- ----------------------------
DROP TABLE IF EXISTS `destination_keywords`;
CREATE TABLE `destination_keywords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) DEFAULT NULL,
  `keyword_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of destination_keywords
-- ----------------------------
INSERT INTO `destination_keywords` VALUES ('1', '1', '1', null, null, null);
INSERT INTO `destination_keywords` VALUES ('2', '2', '1', null, null, null);

-- ----------------------------
-- Table structure for destination_offer
-- ----------------------------
DROP TABLE IF EXISTS `destination_offer`;
CREATE TABLE `destination_offer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) DEFAULT NULL,
  `offer_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of destination_offer
-- ----------------------------

-- ----------------------------
-- Table structure for facilities
-- ----------------------------
DROP TABLE IF EXISTS `facilities`;
CREATE TABLE `facilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of facilities
-- ----------------------------
INSERT INTO `facilities` VALUES ('1', 'test', '2018-09-09 08:04:51', '2018-09-09 08:04:51', null);

-- ----------------------------
-- Table structure for facility_offers
-- ----------------------------
DROP TABLE IF EXISTS `facility_offers`;
CREATE TABLE `facility_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `facility_id` int(11) DEFAULT NULL,
  `offer_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of facility_offers
-- ----------------------------

-- ----------------------------
-- Table structure for hotels
-- ----------------------------
DROP TABLE IF EXISTS `hotels`;
CREATE TABLE `hotels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `star` int(11) DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coordinates` geometry DEFAULT NULL,
  `img` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `facebook` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of hotels
-- ----------------------------
INSERT INTO `hotels` VALUES ('1', 'hotel ', null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for keywords
-- ----------------------------
DROP TABLE IF EXISTS `keywords`;
CREATE TABLE `keywords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of keywords
-- ----------------------------
INSERT INTO `keywords` VALUES ('1', 'Redsea', '2018-09-13 06:31:52', '2018-09-13 06:31:52', null);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'admin', '2018-09-01 10:39:07', '2018-09-01 10:39:07');

-- ----------------------------
-- Table structure for menu_items
-- ----------------------------
DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of menu_items
-- ----------------------------
INSERT INTO `menu_items` VALUES ('1', '1', 'Dashboard', '', '_self', 'voyager-boat', null, null, '1', '2018-09-01 10:39:07', '2018-09-01 10:39:07', 'voyager.dashboard', null);
INSERT INTO `menu_items` VALUES ('2', '1', 'Media', '', '_self', 'voyager-images', null, null, '5', '2018-09-01 10:39:07', '2018-09-01 10:39:07', 'voyager.media.index', null);
INSERT INTO `menu_items` VALUES ('3', '1', 'Users', '', '_self', 'voyager-person', null, null, '3', '2018-09-01 10:39:07', '2018-09-01 10:39:07', 'voyager.users.index', null);
INSERT INTO `menu_items` VALUES ('4', '1', 'Roles', '', '_self', 'voyager-lock', null, null, '2', '2018-09-01 10:39:07', '2018-09-01 10:39:07', 'voyager.roles.index', null);
INSERT INTO `menu_items` VALUES ('5', '1', 'Tools', '', '_self', 'voyager-tools', null, null, '9', '2018-09-01 10:39:07', '2018-09-01 10:39:07', null, null);
INSERT INTO `menu_items` VALUES ('6', '1', 'Menu Builder', '', '_self', 'voyager-list', null, '5', '10', '2018-09-01 10:39:07', '2018-09-01 10:39:07', 'voyager.menus.index', null);
INSERT INTO `menu_items` VALUES ('7', '1', 'Database', '', '_self', 'voyager-data', null, '5', '11', '2018-09-01 10:39:07', '2018-09-01 10:39:07', 'voyager.database.index', null);
INSERT INTO `menu_items` VALUES ('8', '1', 'Compass', '', '_self', 'voyager-compass', null, '5', '12', '2018-09-01 10:39:07', '2018-09-01 10:39:07', 'voyager.compass.index', null);
INSERT INTO `menu_items` VALUES ('9', '1', 'BREAD', '', '_self', 'voyager-bread', null, '5', '13', '2018-09-01 10:39:07', '2018-09-01 10:39:07', 'voyager.bread.index', null);
INSERT INTO `menu_items` VALUES ('10', '1', 'Settings', '', '_self', 'voyager-settings', null, null, '14', '2018-09-01 10:39:08', '2018-09-01 10:39:08', 'voyager.settings.index', null);
INSERT INTO `menu_items` VALUES ('11', '1', 'Categories', '', '_self', 'voyager-categories', null, null, '8', '2018-09-01 10:39:17', '2018-09-01 10:39:17', 'voyager.categories.index', null);
INSERT INTO `menu_items` VALUES ('12', '1', 'Posts', '', '_self', 'voyager-news', null, null, '6', '2018-09-01 10:39:19', '2018-09-01 10:39:19', 'voyager.posts.index', null);
INSERT INTO `menu_items` VALUES ('13', '1', 'Pages', '', '_self', 'voyager-file-text', null, null, '7', '2018-09-01 10:39:21', '2018-09-01 10:39:21', 'voyager.pages.index', null);
INSERT INTO `menu_items` VALUES ('14', '1', 'Hooks', '', '_self', 'voyager-hook', null, '5', '13', '2018-09-01 10:39:24', '2018-09-01 10:39:24', 'voyager.hooks', null);
INSERT INTO `menu_items` VALUES ('15', '1', 'Accommodations', '', '_self', 'voyager-home', '#000000', null, '15', '2018-09-01 14:28:10', '2018-09-15 01:16:33', 'voyager.accommodations.index', 'null');
INSERT INTO `menu_items` VALUES ('16', '1', 'Agencies', '', '_self', 'voyager-list', '#000000', null, '16', '2018-09-01 14:31:43', '2018-09-15 01:17:27', 'voyager.agencies.index', 'null');
INSERT INTO `menu_items` VALUES ('17', '1', 'Applications', '', '_self', 'voyager-list', '#000000', null, '17', '2018-09-01 14:32:21', '2018-09-15 01:17:38', 'voyager.applications.index', 'null');
INSERT INTO `menu_items` VALUES ('18', '1', 'Currencies', '', '_self', 'voyager-dollar', '#000000', null, '18', '2018-09-01 14:38:16', '2018-09-15 01:17:54', 'voyager.currencies.index', 'null');
INSERT INTO `menu_items` VALUES ('19', '1', 'Destinations', '', '_self', 'voyager-home', '#000000', null, '19', '2018-09-01 14:38:40', '2018-09-15 01:18:20', 'voyager.destinations.index', 'null');
INSERT INTO `menu_items` VALUES ('20', '1', 'Facilities', '', '_self', 'voyager-list', '#000000', null, '20', '2018-09-01 14:39:07', '2018-09-15 01:18:41', 'voyager.facilities.index', 'null');
INSERT INTO `menu_items` VALUES ('21', '1', 'Offers', '', '_self', 'voyager-list', '#000000', null, '21', '2018-09-01 14:40:21', '2018-09-15 01:18:57', 'voyager.offers.index', 'null');
INSERT INTO `menu_items` VALUES ('22', '1', 'Packages', '', '_self', 'voyager-list', '#000000', null, '22', '2018-09-09 07:44:22', '2018-09-15 01:19:15', 'voyager.packages.index', 'null');
INSERT INTO `menu_items` VALUES ('23', '1', 'Statuses', '', '_self', 'voyager-list', '#000000', null, '23', '2018-09-09 07:44:46', '2018-09-15 01:19:38', 'voyager.statuses.index', 'null');
INSERT INTO `menu_items` VALUES ('24', '1', 'Programs', '', '_self', 'voyager-list', '#000000', null, '24', '2018-09-09 07:55:02', '2018-09-15 01:19:58', 'voyager.programs.index', 'null');
INSERT INTO `menu_items` VALUES ('25', '1', 'Hotels', '', '_self', 'voyager-home', '#000000', null, '25', '2018-09-09 16:16:14', '2018-09-15 01:20:15', 'voyager.hotels.index', 'null');
INSERT INTO `menu_items` VALUES ('26', '1', 'Cities', '', '_self', 'voyager-home', '#000000', null, '26', '2018-09-13 06:09:19', '2018-09-15 01:16:44', 'voyager.cities.index', 'null');
INSERT INTO `menu_items` VALUES ('27', '1', 'Countries', '', '_self', null, null, null, '27', '2018-09-13 06:13:15', '2018-09-13 06:13:15', 'voyager.countries.index', null);
INSERT INTO `menu_items` VALUES ('28', '1', 'Keywords', '', '_self', 'voyager-list', '#000000', null, '28', '2018-09-13 06:17:46', '2018-09-15 01:20:29', 'voyager.keywords.index', 'null');
INSERT INTO `menu_items` VALUES ('29', '1', 'Rooms', '', '_self', 'voyager-home', '#000000', null, '29', '2018-09-13 06:34:51', '2018-09-15 01:20:46', 'voyager.rooms.index', 'null');
INSERT INTO `menu_items` VALUES ('30', '1', 'Sections', '', '_self', null, null, null, '30', '2018-09-13 06:35:17', '2018-09-13 06:35:17', 'voyager.section.index', null);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2016_06_01_000001_create_oauth_auth_codes_table', '1');
INSERT INTO `migrations` VALUES ('4', '2016_06_01_000002_create_oauth_access_tokens_table', '1');
INSERT INTO `migrations` VALUES ('5', '2016_06_01_000003_create_oauth_refresh_tokens_table', '1');
INSERT INTO `migrations` VALUES ('6', '2016_06_01_000004_create_oauth_clients_table', '1');
INSERT INTO `migrations` VALUES ('7', '2016_06_01_000005_create_oauth_personal_access_clients_table', '1');
INSERT INTO `migrations` VALUES ('8', '2016_01_01_000000_add_voyager_user_fields', '2');
INSERT INTO `migrations` VALUES ('9', '2016_01_01_000000_create_data_types_table', '2');
INSERT INTO `migrations` VALUES ('10', '2016_05_19_173453_create_menu_table', '2');
INSERT INTO `migrations` VALUES ('11', '2016_10_21_190000_create_roles_table', '2');
INSERT INTO `migrations` VALUES ('12', '2016_10_21_190000_create_settings_table', '2');
INSERT INTO `migrations` VALUES ('13', '2016_11_30_135954_create_permission_table', '2');
INSERT INTO `migrations` VALUES ('14', '2016_11_30_141208_create_permission_role_table', '2');
INSERT INTO `migrations` VALUES ('15', '2016_12_26_201236_data_types__add__server_side', '2');
INSERT INTO `migrations` VALUES ('16', '2017_01_13_000000_add_route_to_menu_items_table', '2');
INSERT INTO `migrations` VALUES ('17', '2017_01_14_005015_create_translations_table', '2');
INSERT INTO `migrations` VALUES ('18', '2017_01_15_000000_make_table_name_nullable_in_permissions_table', '2');
INSERT INTO `migrations` VALUES ('19', '2017_03_06_000000_add_controller_to_data_types_table', '2');
INSERT INTO `migrations` VALUES ('20', '2017_04_21_000000_add_order_to_data_rows_table', '2');
INSERT INTO `migrations` VALUES ('21', '2017_07_05_210000_add_policyname_to_data_types_table', '2');
INSERT INTO `migrations` VALUES ('22', '2017_08_05_000000_add_group_to_settings_table', '2');
INSERT INTO `migrations` VALUES ('23', '2017_11_26_013050_add_user_role_relationship', '2');
INSERT INTO `migrations` VALUES ('24', '2017_11_26_015000_create_user_roles_table', '2');
INSERT INTO `migrations` VALUES ('25', '2018_03_11_000000_add_user_settings', '2');
INSERT INTO `migrations` VALUES ('26', '2018_03_14_000000_add_details_to_data_types_table', '2');
INSERT INTO `migrations` VALUES ('27', '2018_03_16_000000_make_settings_value_nullable', '2');
INSERT INTO `migrations` VALUES ('28', '2016_01_01_000000_create_pages_table', '3');
INSERT INTO `migrations` VALUES ('29', '2016_01_01_000000_create_posts_table', '3');
INSERT INTO `migrations` VALUES ('30', '2016_02_15_204651_create_categories_table', '3');
INSERT INTO `migrations` VALUES ('31', '2017_04_11_000000_alter_post_nullable_fields_table', '3');

-- ----------------------------
-- Table structure for oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_auth_codes
-- ----------------------------

-- ----------------------------
-- Table structure for oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_clients
-- ----------------------------
INSERT INTO `oauth_clients` VALUES ('1', null, 'Laravel Personal Access Client', 'UduBT0uyupUsKfmRcBR8LPb09sp1i0HnPD1cGVif', 'http://localhost', '1', '0', '0', '2018-09-01 09:54:21', '2018-09-01 09:54:21');
INSERT INTO `oauth_clients` VALUES ('2', null, 'Laravel Password Grant Client', 'lIgSDG1UvyK2ZV4gm3OVCjhUZ4z3z4Rle5Rvznbs', 'http://localhost', '0', '1', '0', '2018-09-01 09:54:21', '2018-09-01 09:54:21');

-- ----------------------------
-- Table structure for oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_personal_access_clients
-- ----------------------------
INSERT INTO `oauth_personal_access_clients` VALUES ('1', '1', '2018-09-01 09:54:21', '2018-09-01 09:54:21');

-- ----------------------------
-- Table structure for oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of oauth_refresh_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for offers
-- ----------------------------
DROP TABLE IF EXISTS `offers`;
CREATE TABLE `offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci,
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `img` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `verified` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `multi_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of offers
-- ----------------------------
INSERT INTO `offers` VALUES ('1', 'test', null, null, null, null, null, null, null, '1', null, null, null, null, null);

-- ----------------------------
-- Table structure for packages
-- ----------------------------
DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `offer_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `accomodation_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `verified` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of packages
-- ----------------------------
INSERT INTO `packages` VALUES ('1', '1', '1', null, '1', null, null, '2018-09-14 23:30:55', '2018-09-14 23:30:55', null, '1');
INSERT INTO `packages` VALUES ('2', '1', '1', null, '1', null, null, '2018-09-14 23:34:44', '2018-09-14 23:34:44', null, '1');
INSERT INTO `packages` VALUES ('3', '1', '1', null, '1', null, null, '2018-09-14 23:44:26', '2018-09-14 23:44:26', null, '1');
INSERT INTO `packages` VALUES ('4', '1', '1', null, '1', '20', null, '2018-09-15 02:34:10', '2018-09-15 02:34:10', null, '1');
INSERT INTO `packages` VALUES ('5', '1', '1', null, '1', null, null, '2018-09-15 02:42:23', '2018-09-15 02:42:23', null, '1');
INSERT INTO `packages` VALUES ('6', '1', '1', null, '1', '500', null, '2018-09-15 02:44:46', '2018-09-15 02:44:46', null, '2');
INSERT INTO `packages` VALUES ('7', '1', '1', null, '1', '5666', null, null, null, null, '1');
INSERT INTO `packages` VALUES ('8', '1', '1', null, '1', '1000', null, null, null, null, '1');
INSERT INTO `packages` VALUES ('9', '1', '1', null, '1', '1000', null, null, null, null, '2');
INSERT INTO `packages` VALUES ('10', '1', '1', null, '1', '69696', null, '2018-09-15 03:20:39', '2018-09-15 03:20:39', null, '1');
INSERT INTO `packages` VALUES ('11', '1', '1', null, '1', '123', null, '2018-09-15 03:21:07', '2018-09-18 19:42:47', '2018-09-18 19:42:47', '1');
INSERT INTO `packages` VALUES ('12', '1', '1', null, '1', '123', null, '2018-09-15 03:21:08', '2018-09-18 19:32:05', '2018-09-18 19:32:05', '1');

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES ('1', '0', 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2018-09-01 10:39:21', '2018-09-01 10:39:21');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'browse_admin', null, '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('2', 'browse_bread', null, '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('3', 'browse_database', null, '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('4', 'browse_media', null, '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('5', 'browse_compass', null, '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('6', 'browse_menus', 'menus', '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('7', 'read_menus', 'menus', '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('8', 'edit_menus', 'menus', '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('9', 'add_menus', 'menus', '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('10', 'delete_menus', 'menus', '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('11', 'browse_roles', 'roles', '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `permissions` VALUES ('12', 'read_roles', 'roles', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('13', 'edit_roles', 'roles', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('14', 'add_roles', 'roles', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('15', 'delete_roles', 'roles', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('16', 'browse_users', 'users', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('17', 'read_users', 'users', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('18', 'edit_users', 'users', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('19', 'add_users', 'users', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('20', 'delete_users', 'users', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('21', 'browse_settings', 'settings', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('22', 'read_settings', 'settings', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('23', 'edit_settings', 'settings', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('24', 'add_settings', 'settings', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('25', 'delete_settings', 'settings', '2018-09-01 10:39:09', '2018-09-01 10:39:09');
INSERT INTO `permissions` VALUES ('26', 'browse_categories', 'categories', '2018-09-01 10:39:17', '2018-09-01 10:39:17');
INSERT INTO `permissions` VALUES ('27', 'read_categories', 'categories', '2018-09-01 10:39:18', '2018-09-01 10:39:18');
INSERT INTO `permissions` VALUES ('28', 'edit_categories', 'categories', '2018-09-01 10:39:18', '2018-09-01 10:39:18');
INSERT INTO `permissions` VALUES ('29', 'add_categories', 'categories', '2018-09-01 10:39:18', '2018-09-01 10:39:18');
INSERT INTO `permissions` VALUES ('30', 'delete_categories', 'categories', '2018-09-01 10:39:18', '2018-09-01 10:39:18');
INSERT INTO `permissions` VALUES ('31', 'browse_posts', 'posts', '2018-09-01 10:39:20', '2018-09-01 10:39:20');
INSERT INTO `permissions` VALUES ('32', 'read_posts', 'posts', '2018-09-01 10:39:20', '2018-09-01 10:39:20');
INSERT INTO `permissions` VALUES ('33', 'edit_posts', 'posts', '2018-09-01 10:39:20', '2018-09-01 10:39:20');
INSERT INTO `permissions` VALUES ('34', 'add_posts', 'posts', '2018-09-01 10:39:20', '2018-09-01 10:39:20');
INSERT INTO `permissions` VALUES ('35', 'delete_posts', 'posts', '2018-09-01 10:39:20', '2018-09-01 10:39:20');
INSERT INTO `permissions` VALUES ('36', 'browse_pages', 'pages', '2018-09-01 10:39:21', '2018-09-01 10:39:21');
INSERT INTO `permissions` VALUES ('37', 'read_pages', 'pages', '2018-09-01 10:39:21', '2018-09-01 10:39:21');
INSERT INTO `permissions` VALUES ('38', 'edit_pages', 'pages', '2018-09-01 10:39:21', '2018-09-01 10:39:21');
INSERT INTO `permissions` VALUES ('39', 'add_pages', 'pages', '2018-09-01 10:39:21', '2018-09-01 10:39:21');
INSERT INTO `permissions` VALUES ('40', 'delete_pages', 'pages', '2018-09-01 10:39:21', '2018-09-01 10:39:21');
INSERT INTO `permissions` VALUES ('41', 'browse_hooks', null, '2018-09-01 10:39:24', '2018-09-01 10:39:24');
INSERT INTO `permissions` VALUES ('42', 'browse_accommodations', 'accommodations', '2018-09-01 14:28:10', '2018-09-01 14:28:10');
INSERT INTO `permissions` VALUES ('43', 'read_accommodations', 'accommodations', '2018-09-01 14:28:10', '2018-09-01 14:28:10');
INSERT INTO `permissions` VALUES ('44', 'edit_accommodations', 'accommodations', '2018-09-01 14:28:10', '2018-09-01 14:28:10');
INSERT INTO `permissions` VALUES ('45', 'add_accommodations', 'accommodations', '2018-09-01 14:28:10', '2018-09-01 14:28:10');
INSERT INTO `permissions` VALUES ('46', 'delete_accommodations', 'accommodations', '2018-09-01 14:28:10', '2018-09-01 14:28:10');
INSERT INTO `permissions` VALUES ('47', 'browse_agencies', 'agencies', '2018-09-01 14:31:43', '2018-09-01 14:31:43');
INSERT INTO `permissions` VALUES ('48', 'read_agencies', 'agencies', '2018-09-01 14:31:43', '2018-09-01 14:31:43');
INSERT INTO `permissions` VALUES ('49', 'edit_agencies', 'agencies', '2018-09-01 14:31:43', '2018-09-01 14:31:43');
INSERT INTO `permissions` VALUES ('50', 'add_agencies', 'agencies', '2018-09-01 14:31:43', '2018-09-01 14:31:43');
INSERT INTO `permissions` VALUES ('51', 'delete_agencies', 'agencies', '2018-09-01 14:31:43', '2018-09-01 14:31:43');
INSERT INTO `permissions` VALUES ('52', 'browse_applications', 'applications', '2018-09-01 14:32:21', '2018-09-01 14:32:21');
INSERT INTO `permissions` VALUES ('53', 'read_applications', 'applications', '2018-09-01 14:32:21', '2018-09-01 14:32:21');
INSERT INTO `permissions` VALUES ('54', 'edit_applications', 'applications', '2018-09-01 14:32:21', '2018-09-01 14:32:21');
INSERT INTO `permissions` VALUES ('55', 'add_applications', 'applications', '2018-09-01 14:32:21', '2018-09-01 14:32:21');
INSERT INTO `permissions` VALUES ('56', 'delete_applications', 'applications', '2018-09-01 14:32:21', '2018-09-01 14:32:21');
INSERT INTO `permissions` VALUES ('57', 'browse_currencies', 'currencies', '2018-09-01 14:38:16', '2018-09-01 14:38:16');
INSERT INTO `permissions` VALUES ('58', 'read_currencies', 'currencies', '2018-09-01 14:38:16', '2018-09-01 14:38:16');
INSERT INTO `permissions` VALUES ('59', 'edit_currencies', 'currencies', '2018-09-01 14:38:16', '2018-09-01 14:38:16');
INSERT INTO `permissions` VALUES ('60', 'add_currencies', 'currencies', '2018-09-01 14:38:16', '2018-09-01 14:38:16');
INSERT INTO `permissions` VALUES ('61', 'delete_currencies', 'currencies', '2018-09-01 14:38:16', '2018-09-01 14:38:16');
INSERT INTO `permissions` VALUES ('62', 'browse_destinations', 'destinations', '2018-09-01 14:38:40', '2018-09-01 14:38:40');
INSERT INTO `permissions` VALUES ('63', 'read_destinations', 'destinations', '2018-09-01 14:38:40', '2018-09-01 14:38:40');
INSERT INTO `permissions` VALUES ('64', 'edit_destinations', 'destinations', '2018-09-01 14:38:40', '2018-09-01 14:38:40');
INSERT INTO `permissions` VALUES ('65', 'add_destinations', 'destinations', '2018-09-01 14:38:40', '2018-09-01 14:38:40');
INSERT INTO `permissions` VALUES ('66', 'delete_destinations', 'destinations', '2018-09-01 14:38:40', '2018-09-01 14:38:40');
INSERT INTO `permissions` VALUES ('67', 'browse_facilities', 'facilities', '2018-09-01 14:39:07', '2018-09-01 14:39:07');
INSERT INTO `permissions` VALUES ('68', 'read_facilities', 'facilities', '2018-09-01 14:39:07', '2018-09-01 14:39:07');
INSERT INTO `permissions` VALUES ('69', 'edit_facilities', 'facilities', '2018-09-01 14:39:07', '2018-09-01 14:39:07');
INSERT INTO `permissions` VALUES ('70', 'add_facilities', 'facilities', '2018-09-01 14:39:07', '2018-09-01 14:39:07');
INSERT INTO `permissions` VALUES ('71', 'delete_facilities', 'facilities', '2018-09-01 14:39:07', '2018-09-01 14:39:07');
INSERT INTO `permissions` VALUES ('82', 'browse_statuses', 'statuses', '2018-09-09 07:44:46', '2018-09-09 07:44:46');
INSERT INTO `permissions` VALUES ('83', 'read_statuses', 'statuses', '2018-09-09 07:44:46', '2018-09-09 07:44:46');
INSERT INTO `permissions` VALUES ('84', 'edit_statuses', 'statuses', '2018-09-09 07:44:46', '2018-09-09 07:44:46');
INSERT INTO `permissions` VALUES ('85', 'add_statuses', 'statuses', '2018-09-09 07:44:46', '2018-09-09 07:44:46');
INSERT INTO `permissions` VALUES ('86', 'delete_statuses', 'statuses', '2018-09-09 07:44:46', '2018-09-09 07:44:46');
INSERT INTO `permissions` VALUES ('87', 'browse_programs', 'programs', '2018-09-09 07:55:02', '2018-09-09 07:55:02');
INSERT INTO `permissions` VALUES ('88', 'read_programs', 'programs', '2018-09-09 07:55:02', '2018-09-09 07:55:02');
INSERT INTO `permissions` VALUES ('89', 'edit_programs', 'programs', '2018-09-09 07:55:02', '2018-09-09 07:55:02');
INSERT INTO `permissions` VALUES ('90', 'add_programs', 'programs', '2018-09-09 07:55:02', '2018-09-09 07:55:02');
INSERT INTO `permissions` VALUES ('91', 'delete_programs', 'programs', '2018-09-09 07:55:02', '2018-09-09 07:55:02');
INSERT INTO `permissions` VALUES ('92', 'browse_hotels', 'hotels', '2018-09-09 16:16:14', '2018-09-09 16:16:14');
INSERT INTO `permissions` VALUES ('93', 'read_hotels', 'hotels', '2018-09-09 16:16:14', '2018-09-09 16:16:14');
INSERT INTO `permissions` VALUES ('94', 'edit_hotels', 'hotels', '2018-09-09 16:16:14', '2018-09-09 16:16:14');
INSERT INTO `permissions` VALUES ('95', 'add_hotels', 'hotels', '2018-09-09 16:16:14', '2018-09-09 16:16:14');
INSERT INTO `permissions` VALUES ('96', 'delete_hotels', 'hotels', '2018-09-09 16:16:14', '2018-09-09 16:16:14');
INSERT INTO `permissions` VALUES ('102', 'browse_countries', 'countries', '2018-09-13 06:13:15', '2018-09-13 06:13:15');
INSERT INTO `permissions` VALUES ('103', 'read_countries', 'countries', '2018-09-13 06:13:15', '2018-09-13 06:13:15');
INSERT INTO `permissions` VALUES ('104', 'edit_countries', 'countries', '2018-09-13 06:13:15', '2018-09-13 06:13:15');
INSERT INTO `permissions` VALUES ('105', 'add_countries', 'countries', '2018-09-13 06:13:15', '2018-09-13 06:13:15');
INSERT INTO `permissions` VALUES ('106', 'delete_countries', 'countries', '2018-09-13 06:13:15', '2018-09-13 06:13:15');
INSERT INTO `permissions` VALUES ('107', 'browse_keywords', 'keywords', '2018-09-13 06:17:46', '2018-09-13 06:17:46');
INSERT INTO `permissions` VALUES ('108', 'read_keywords', 'keywords', '2018-09-13 06:17:46', '2018-09-13 06:17:46');
INSERT INTO `permissions` VALUES ('109', 'edit_keywords', 'keywords', '2018-09-13 06:17:46', '2018-09-13 06:17:46');
INSERT INTO `permissions` VALUES ('110', 'add_keywords', 'keywords', '2018-09-13 06:17:46', '2018-09-13 06:17:46');
INSERT INTO `permissions` VALUES ('111', 'delete_keywords', 'keywords', '2018-09-13 06:17:46', '2018-09-13 06:17:46');
INSERT INTO `permissions` VALUES ('112', 'browse_rooms', 'rooms', '2018-09-13 06:34:51', '2018-09-13 06:34:51');
INSERT INTO `permissions` VALUES ('113', 'read_rooms', 'rooms', '2018-09-13 06:34:51', '2018-09-13 06:34:51');
INSERT INTO `permissions` VALUES ('114', 'edit_rooms', 'rooms', '2018-09-13 06:34:51', '2018-09-13 06:34:51');
INSERT INTO `permissions` VALUES ('115', 'add_rooms', 'rooms', '2018-09-13 06:34:51', '2018-09-13 06:34:51');
INSERT INTO `permissions` VALUES ('116', 'delete_rooms', 'rooms', '2018-09-13 06:34:51', '2018-09-13 06:34:51');
INSERT INTO `permissions` VALUES ('117', 'browse_section', 'section', '2018-09-13 06:35:16', '2018-09-13 06:35:16');
INSERT INTO `permissions` VALUES ('118', 'read_section', 'section', '2018-09-13 06:35:16', '2018-09-13 06:35:16');
INSERT INTO `permissions` VALUES ('119', 'edit_section', 'section', '2018-09-13 06:35:16', '2018-09-13 06:35:16');
INSERT INTO `permissions` VALUES ('120', 'add_section', 'section', '2018-09-13 06:35:16', '2018-09-13 06:35:16');
INSERT INTO `permissions` VALUES ('121', 'delete_section', 'section', '2018-09-13 06:35:16', '2018-09-13 06:35:16');
INSERT INTO `permissions` VALUES ('122', 'browse_cities', 'cities', '2018-09-15 02:20:40', '2018-09-15 02:20:40');
INSERT INTO `permissions` VALUES ('123', 'read_cities', 'cities', '2018-09-15 02:20:40', '2018-09-15 02:20:40');
INSERT INTO `permissions` VALUES ('124', 'edit_cities', 'cities', '2018-09-15 02:20:40', '2018-09-15 02:20:40');
INSERT INTO `permissions` VALUES ('125', 'add_cities', 'cities', '2018-09-15 02:20:40', '2018-09-15 02:20:40');
INSERT INTO `permissions` VALUES ('126', 'delete_cities', 'cities', '2018-09-15 02:20:40', '2018-09-15 02:20:40');
INSERT INTO `permissions` VALUES ('127', 'browse_offers', 'offers', '2018-09-15 02:27:28', '2018-09-15 02:27:28');
INSERT INTO `permissions` VALUES ('128', 'read_offers', 'offers', '2018-09-15 02:27:28', '2018-09-15 02:27:28');
INSERT INTO `permissions` VALUES ('129', 'edit_offers', 'offers', '2018-09-15 02:27:28', '2018-09-15 02:27:28');
INSERT INTO `permissions` VALUES ('130', 'add_offers', 'offers', '2018-09-15 02:27:28', '2018-09-15 02:27:28');
INSERT INTO `permissions` VALUES ('131', 'delete_offers', 'offers', '2018-09-15 02:27:28', '2018-09-15 02:27:28');
INSERT INTO `permissions` VALUES ('132', 'browse_packages', 'packages', '2018-09-15 02:46:52', '2018-09-15 02:46:52');
INSERT INTO `permissions` VALUES ('133', 'read_packages', 'packages', '2018-09-15 02:46:52', '2018-09-15 02:46:52');
INSERT INTO `permissions` VALUES ('134', 'edit_packages', 'packages', '2018-09-15 02:46:52', '2018-09-15 02:46:52');
INSERT INTO `permissions` VALUES ('135', 'add_packages', 'packages', '2018-09-15 02:46:52', '2018-09-15 02:46:52');
INSERT INTO `permissions` VALUES ('136', 'delete_packages', 'packages', '2018-09-15 02:46:52', '2018-09-15 02:46:52');

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES ('1', '1');
INSERT INTO `permission_role` VALUES ('1', '3');
INSERT INTO `permission_role` VALUES ('1', '4');
INSERT INTO `permission_role` VALUES ('2', '1');
INSERT INTO `permission_role` VALUES ('3', '1');
INSERT INTO `permission_role` VALUES ('4', '1');
INSERT INTO `permission_role` VALUES ('5', '1');
INSERT INTO `permission_role` VALUES ('6', '1');
INSERT INTO `permission_role` VALUES ('7', '1');
INSERT INTO `permission_role` VALUES ('8', '1');
INSERT INTO `permission_role` VALUES ('9', '1');
INSERT INTO `permission_role` VALUES ('10', '1');
INSERT INTO `permission_role` VALUES ('11', '1');
INSERT INTO `permission_role` VALUES ('12', '1');
INSERT INTO `permission_role` VALUES ('13', '1');
INSERT INTO `permission_role` VALUES ('14', '1');
INSERT INTO `permission_role` VALUES ('15', '1');
INSERT INTO `permission_role` VALUES ('16', '1');
INSERT INTO `permission_role` VALUES ('16', '4');
INSERT INTO `permission_role` VALUES ('17', '1');
INSERT INTO `permission_role` VALUES ('17', '4');
INSERT INTO `permission_role` VALUES ('18', '1');
INSERT INTO `permission_role` VALUES ('18', '4');
INSERT INTO `permission_role` VALUES ('19', '1');
INSERT INTO `permission_role` VALUES ('19', '4');
INSERT INTO `permission_role` VALUES ('20', '1');
INSERT INTO `permission_role` VALUES ('20', '4');
INSERT INTO `permission_role` VALUES ('21', '1');
INSERT INTO `permission_role` VALUES ('22', '1');
INSERT INTO `permission_role` VALUES ('23', '1');
INSERT INTO `permission_role` VALUES ('24', '1');
INSERT INTO `permission_role` VALUES ('25', '1');
INSERT INTO `permission_role` VALUES ('26', '1');
INSERT INTO `permission_role` VALUES ('27', '1');
INSERT INTO `permission_role` VALUES ('28', '1');
INSERT INTO `permission_role` VALUES ('29', '1');
INSERT INTO `permission_role` VALUES ('30', '1');
INSERT INTO `permission_role` VALUES ('31', '1');
INSERT INTO `permission_role` VALUES ('32', '1');
INSERT INTO `permission_role` VALUES ('33', '1');
INSERT INTO `permission_role` VALUES ('34', '1');
INSERT INTO `permission_role` VALUES ('35', '1');
INSERT INTO `permission_role` VALUES ('36', '1');
INSERT INTO `permission_role` VALUES ('37', '1');
INSERT INTO `permission_role` VALUES ('38', '1');
INSERT INTO `permission_role` VALUES ('39', '1');
INSERT INTO `permission_role` VALUES ('40', '1');
INSERT INTO `permission_role` VALUES ('42', '1');
INSERT INTO `permission_role` VALUES ('42', '4');
INSERT INTO `permission_role` VALUES ('43', '1');
INSERT INTO `permission_role` VALUES ('43', '4');
INSERT INTO `permission_role` VALUES ('44', '1');
INSERT INTO `permission_role` VALUES ('44', '4');
INSERT INTO `permission_role` VALUES ('45', '1');
INSERT INTO `permission_role` VALUES ('45', '4');
INSERT INTO `permission_role` VALUES ('46', '1');
INSERT INTO `permission_role` VALUES ('46', '4');
INSERT INTO `permission_role` VALUES ('47', '1');
INSERT INTO `permission_role` VALUES ('47', '4');
INSERT INTO `permission_role` VALUES ('48', '1');
INSERT INTO `permission_role` VALUES ('48', '4');
INSERT INTO `permission_role` VALUES ('49', '1');
INSERT INTO `permission_role` VALUES ('49', '4');
INSERT INTO `permission_role` VALUES ('50', '1');
INSERT INTO `permission_role` VALUES ('50', '4');
INSERT INTO `permission_role` VALUES ('51', '1');
INSERT INTO `permission_role` VALUES ('51', '4');
INSERT INTO `permission_role` VALUES ('52', '1');
INSERT INTO `permission_role` VALUES ('52', '3');
INSERT INTO `permission_role` VALUES ('52', '4');
INSERT INTO `permission_role` VALUES ('53', '1');
INSERT INTO `permission_role` VALUES ('53', '4');
INSERT INTO `permission_role` VALUES ('54', '1');
INSERT INTO `permission_role` VALUES ('54', '4');
INSERT INTO `permission_role` VALUES ('55', '1');
INSERT INTO `permission_role` VALUES ('55', '4');
INSERT INTO `permission_role` VALUES ('56', '1');
INSERT INTO `permission_role` VALUES ('56', '4');
INSERT INTO `permission_role` VALUES ('57', '1');
INSERT INTO `permission_role` VALUES ('57', '4');
INSERT INTO `permission_role` VALUES ('58', '1');
INSERT INTO `permission_role` VALUES ('58', '4');
INSERT INTO `permission_role` VALUES ('59', '1');
INSERT INTO `permission_role` VALUES ('59', '4');
INSERT INTO `permission_role` VALUES ('60', '1');
INSERT INTO `permission_role` VALUES ('60', '4');
INSERT INTO `permission_role` VALUES ('61', '1');
INSERT INTO `permission_role` VALUES ('61', '4');
INSERT INTO `permission_role` VALUES ('62', '1');
INSERT INTO `permission_role` VALUES ('62', '4');
INSERT INTO `permission_role` VALUES ('63', '1');
INSERT INTO `permission_role` VALUES ('63', '4');
INSERT INTO `permission_role` VALUES ('64', '1');
INSERT INTO `permission_role` VALUES ('64', '4');
INSERT INTO `permission_role` VALUES ('65', '1');
INSERT INTO `permission_role` VALUES ('65', '4');
INSERT INTO `permission_role` VALUES ('66', '1');
INSERT INTO `permission_role` VALUES ('66', '4');
INSERT INTO `permission_role` VALUES ('67', '1');
INSERT INTO `permission_role` VALUES ('67', '4');
INSERT INTO `permission_role` VALUES ('68', '1');
INSERT INTO `permission_role` VALUES ('68', '4');
INSERT INTO `permission_role` VALUES ('69', '1');
INSERT INTO `permission_role` VALUES ('69', '4');
INSERT INTO `permission_role` VALUES ('70', '1');
INSERT INTO `permission_role` VALUES ('70', '4');
INSERT INTO `permission_role` VALUES ('71', '1');
INSERT INTO `permission_role` VALUES ('71', '4');
INSERT INTO `permission_role` VALUES ('82', '1');
INSERT INTO `permission_role` VALUES ('82', '4');
INSERT INTO `permission_role` VALUES ('83', '1');
INSERT INTO `permission_role` VALUES ('83', '4');
INSERT INTO `permission_role` VALUES ('84', '1');
INSERT INTO `permission_role` VALUES ('84', '4');
INSERT INTO `permission_role` VALUES ('85', '1');
INSERT INTO `permission_role` VALUES ('85', '4');
INSERT INTO `permission_role` VALUES ('86', '1');
INSERT INTO `permission_role` VALUES ('86', '4');
INSERT INTO `permission_role` VALUES ('87', '1');
INSERT INTO `permission_role` VALUES ('87', '3');
INSERT INTO `permission_role` VALUES ('87', '4');
INSERT INTO `permission_role` VALUES ('88', '1');
INSERT INTO `permission_role` VALUES ('88', '3');
INSERT INTO `permission_role` VALUES ('88', '4');
INSERT INTO `permission_role` VALUES ('89', '1');
INSERT INTO `permission_role` VALUES ('89', '3');
INSERT INTO `permission_role` VALUES ('89', '4');
INSERT INTO `permission_role` VALUES ('90', '1');
INSERT INTO `permission_role` VALUES ('90', '3');
INSERT INTO `permission_role` VALUES ('90', '4');
INSERT INTO `permission_role` VALUES ('91', '1');
INSERT INTO `permission_role` VALUES ('91', '3');
INSERT INTO `permission_role` VALUES ('91', '4');
INSERT INTO `permission_role` VALUES ('92', '1');
INSERT INTO `permission_role` VALUES ('92', '4');
INSERT INTO `permission_role` VALUES ('93', '1');
INSERT INTO `permission_role` VALUES ('93', '4');
INSERT INTO `permission_role` VALUES ('94', '1');
INSERT INTO `permission_role` VALUES ('94', '4');
INSERT INTO `permission_role` VALUES ('95', '1');
INSERT INTO `permission_role` VALUES ('95', '4');
INSERT INTO `permission_role` VALUES ('96', '1');
INSERT INTO `permission_role` VALUES ('96', '4');
INSERT INTO `permission_role` VALUES ('102', '1');
INSERT INTO `permission_role` VALUES ('102', '4');
INSERT INTO `permission_role` VALUES ('103', '1');
INSERT INTO `permission_role` VALUES ('103', '4');
INSERT INTO `permission_role` VALUES ('104', '1');
INSERT INTO `permission_role` VALUES ('104', '4');
INSERT INTO `permission_role` VALUES ('105', '1');
INSERT INTO `permission_role` VALUES ('105', '4');
INSERT INTO `permission_role` VALUES ('106', '1');
INSERT INTO `permission_role` VALUES ('106', '4');
INSERT INTO `permission_role` VALUES ('107', '1');
INSERT INTO `permission_role` VALUES ('107', '4');
INSERT INTO `permission_role` VALUES ('108', '1');
INSERT INTO `permission_role` VALUES ('108', '4');
INSERT INTO `permission_role` VALUES ('109', '1');
INSERT INTO `permission_role` VALUES ('109', '4');
INSERT INTO `permission_role` VALUES ('110', '1');
INSERT INTO `permission_role` VALUES ('110', '4');
INSERT INTO `permission_role` VALUES ('111', '1');
INSERT INTO `permission_role` VALUES ('111', '4');
INSERT INTO `permission_role` VALUES ('112', '1');
INSERT INTO `permission_role` VALUES ('112', '4');
INSERT INTO `permission_role` VALUES ('113', '1');
INSERT INTO `permission_role` VALUES ('113', '4');
INSERT INTO `permission_role` VALUES ('114', '1');
INSERT INTO `permission_role` VALUES ('114', '4');
INSERT INTO `permission_role` VALUES ('115', '1');
INSERT INTO `permission_role` VALUES ('115', '4');
INSERT INTO `permission_role` VALUES ('116', '1');
INSERT INTO `permission_role` VALUES ('116', '4');
INSERT INTO `permission_role` VALUES ('117', '1');
INSERT INTO `permission_role` VALUES ('117', '4');
INSERT INTO `permission_role` VALUES ('118', '1');
INSERT INTO `permission_role` VALUES ('118', '4');
INSERT INTO `permission_role` VALUES ('119', '1');
INSERT INTO `permission_role` VALUES ('119', '4');
INSERT INTO `permission_role` VALUES ('120', '1');
INSERT INTO `permission_role` VALUES ('120', '4');
INSERT INTO `permission_role` VALUES ('121', '1');
INSERT INTO `permission_role` VALUES ('121', '4');
INSERT INTO `permission_role` VALUES ('122', '1');
INSERT INTO `permission_role` VALUES ('123', '1');
INSERT INTO `permission_role` VALUES ('124', '1');
INSERT INTO `permission_role` VALUES ('125', '1');
INSERT INTO `permission_role` VALUES ('126', '1');
INSERT INTO `permission_role` VALUES ('127', '1');
INSERT INTO `permission_role` VALUES ('127', '3');
INSERT INTO `permission_role` VALUES ('128', '1');
INSERT INTO `permission_role` VALUES ('128', '3');
INSERT INTO `permission_role` VALUES ('129', '1');
INSERT INTO `permission_role` VALUES ('129', '3');
INSERT INTO `permission_role` VALUES ('130', '1');
INSERT INTO `permission_role` VALUES ('130', '3');
INSERT INTO `permission_role` VALUES ('131', '1');
INSERT INTO `permission_role` VALUES ('131', '3');
INSERT INTO `permission_role` VALUES ('132', '1');
INSERT INTO `permission_role` VALUES ('132', '3');
INSERT INTO `permission_role` VALUES ('133', '1');
INSERT INTO `permission_role` VALUES ('133', '3');
INSERT INTO `permission_role` VALUES ('134', '1');
INSERT INTO `permission_role` VALUES ('134', '3');
INSERT INTO `permission_role` VALUES ('135', '1');
INSERT INTO `permission_role` VALUES ('135', '3');
INSERT INTO `permission_role` VALUES ('136', '1');
INSERT INTO `permission_role` VALUES ('136', '3');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('1', '0', null, 'Lorem Ipsum Post', null, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', '0', '2018-09-01 10:39:20', '2018-09-01 10:39:20');
INSERT INTO `posts` VALUES ('2', '0', null, 'My Sample Post', null, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', '0', '2018-09-01 10:39:20', '2018-09-01 10:39:20');
INSERT INTO `posts` VALUES ('3', '0', null, 'Latest Post', null, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', '0', '2018-09-01 10:39:20', '2018-09-01 10:39:20');
INSERT INTO `posts` VALUES ('4', '0', null, 'Yarr Post', null, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', '0', '2018-09-01 10:39:20', '2018-09-01 10:39:20');

-- ----------------------------
-- Table structure for programs
-- ----------------------------
DROP TABLE IF EXISTS `programs`;
CREATE TABLE `programs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci,
  `hotel_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `offer_id` int(11) DEFAULT NULL,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of programs
-- ----------------------------
INSERT INTO `programs` VALUES ('1', 'test P', 'desc', '1', '23', '1', null, null, '2018-09-15 01:43:47', '2018-09-15 01:43:47', null, '2');

-- ----------------------------
-- Table structure for reviews
-- ----------------------------
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rate` int(11) DEFAULT NULL,
  `review_text` longtext COLLATE utf8mb4_unicode_ci,
  `offer_id` int(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of reviews
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', 'Administrator', '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `roles` VALUES ('2', 'user', 'Normal User', '2018-09-01 10:39:08', '2018-09-01 10:39:08');
INSERT INTO `roles` VALUES ('3', 'agency', 'Agency User', '2018-09-09 16:29:45', '2018-09-09 16:29:45');
INSERT INTO `roles` VALUES ('4', 'SuperAdmin', 'Admin', '2018-09-14 21:24:15', '2018-09-14 21:24:15');

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of rooms
-- ----------------------------
INSERT INTO `rooms` VALUES ('1', 'room1', null, null, null);

-- ----------------------------
-- Table structure for section
-- ----------------------------
DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of section
-- ----------------------------

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', 'site.title', 'Site Title', 'Site Title', '', 'text', '1', 'Site');
INSERT INTO `settings` VALUES ('2', 'site.description', 'Site Description', 'Site Description', '', 'text', '2', 'Site');
INSERT INTO `settings` VALUES ('3', 'site.logo', 'Site Logo', '', '', 'image', '3', 'Site');
INSERT INTO `settings` VALUES ('4', 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', '4', 'Site');
INSERT INTO `settings` VALUES ('5', 'admin.bg_image', 'Admin Background Image', '', '', 'image', '5', 'Admin');
INSERT INTO `settings` VALUES ('6', 'admin.title', 'Admin Title', 'Voyager', '', 'text', '1', 'Admin');
INSERT INTO `settings` VALUES ('7', 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', '2', 'Admin');
INSERT INTO `settings` VALUES ('8', 'admin.loader', 'Admin Loader', '', '', 'image', '3', 'Admin');
INSERT INTO `settings` VALUES ('9', 'admin.icon_image', 'Admin Icon Image', '', '', 'image', '4', 'Admin');
INSERT INTO `settings` VALUES ('10', 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', '1', 'Admin');

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of status
-- ----------------------------

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of statuses
-- ----------------------------

-- ----------------------------
-- Table structure for translations
-- ----------------------------
DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of translations
-- ----------------------------
INSERT INTO `translations` VALUES ('1', 'data_types', 'display_name_singular', '5', 'pt', 'Post', '2018-09-01 10:39:21', '2018-09-01 10:39:21');
INSERT INTO `translations` VALUES ('2', 'data_types', 'display_name_singular', '6', 'pt', 'Página', '2018-09-01 10:39:21', '2018-09-01 10:39:21');
INSERT INTO `translations` VALUES ('3', 'data_types', 'display_name_singular', '1', 'pt', 'Utilizador', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('4', 'data_types', 'display_name_singular', '4', 'pt', 'Categoria', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('5', 'data_types', 'display_name_singular', '2', 'pt', 'Menu', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('6', 'data_types', 'display_name_singular', '3', 'pt', 'Função', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('7', 'data_types', 'display_name_plural', '5', 'pt', 'Posts', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('8', 'data_types', 'display_name_plural', '6', 'pt', 'Páginas', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('9', 'data_types', 'display_name_plural', '1', 'pt', 'Utilizadores', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('10', 'data_types', 'display_name_plural', '4', 'pt', 'Categorias', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('11', 'data_types', 'display_name_plural', '2', 'pt', 'Menus', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('12', 'data_types', 'display_name_plural', '3', 'pt', 'Funções', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('13', 'categories', 'slug', '1', 'pt', 'categoria-1', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('14', 'categories', 'name', '1', 'pt', 'Categoria 1', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('15', 'categories', 'slug', '2', 'pt', 'categoria-2', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('16', 'categories', 'name', '2', 'pt', 'Categoria 2', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('17', 'pages', 'title', '1', 'pt', 'Olá Mundo', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('18', 'pages', 'slug', '1', 'pt', 'ola-mundo', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('19', 'pages', 'body', '1', 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-09-01 10:39:22', '2018-09-01 10:39:22');
INSERT INTO `translations` VALUES ('20', 'menu_items', 'title', '1', 'pt', 'Painel de Controle', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('21', 'menu_items', 'title', '2', 'pt', 'Media', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('22', 'menu_items', 'title', '12', 'pt', 'Publicações', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('23', 'menu_items', 'title', '3', 'pt', 'Utilizadores', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('24', 'menu_items', 'title', '11', 'pt', 'Categorias', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('25', 'menu_items', 'title', '13', 'pt', 'Páginas', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('26', 'menu_items', 'title', '4', 'pt', 'Funções', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('27', 'menu_items', 'title', '5', 'pt', 'Ferramentas', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('28', 'menu_items', 'title', '6', 'pt', 'Menus', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('29', 'menu_items', 'title', '7', 'pt', 'Base de dados', '2018-09-01 10:39:23', '2018-09-01 10:39:23');
INSERT INTO `translations` VALUES ('30', 'menu_items', 'title', '10', 'pt', 'Configurações', '2018-09-01 10:39:23', '2018-09-01 10:39:23');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'Admin', 'admin@admin.com', 'users/default.png', '$2y$10$lTGWXNvtB9t5l00Xr9v8d.l8/Bv3cFUtx0O8KZscy0TSOaEQNziG2', 'ACG74utBUfjSkmKZUjdRBdPwHUTL4pAi3Qf3FRS4g4tJJUr5TDljuL5HpfaX', null, '2018-09-01 10:39:18', '2018-09-01 10:39:18');
INSERT INTO `users` VALUES ('2', '4', 'tripiata', 'admin@tripiata.com', 'users/default.png', '$2y$10$yzFGAS.vEMZXPEow5kOYLu.wg2EQIfW.f8sDKzUXtmEQO5cBXwgRa', 'bVXhLoGTznCmQMHBlVZWfBwd9Orpj78SC9mn59sat1KobsUWQWSbCKnJcsME', '{\"locale\":\"en\"}', '2018-09-14 21:31:19', '2018-09-14 21:31:19');
INSERT INTO `users` VALUES ('3', '3', 'test', 'test@agency.com', 'users/default.png', '$2y$10$yzFGAS.vEMZXPEow5kOYLu.wg2EQIfW.f8sDKzUXtmEQO5cBXwgRa', null, '{\"locale\":\"en\"}', '2018-09-14 23:52:05', '2018-09-14 23:55:20');

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
